package pa_model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;



public class RecordProperties {
	
	private Map properties; 
	
	public RecordProperties(Map properties){
	
		if(properties == null){
				this.properties = new HashMap();
				} else {
					this.properties = new HashMap(properties);
				}
	}

	public Object getProperty(String propertyName){
		
			return properties.get(propertyName);
	} 
	
	public Map getProperties(){
			return properties;
	}

	public boolean isMatch(RecordProperties otherProperties){
		for (Iterator i = otherProperties.getProperties().keySet().iterator();
				i.hasNext();){
			String propertyName = (String)i.next();
			
			if (!properties.get(propertyName).equals(otherProperties.getProperty(propertyName))){return false;}
		}
		return true;
	}
	
}
