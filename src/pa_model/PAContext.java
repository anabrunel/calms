package pa_model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import cern.jet.random.Uniform;
import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.ISchedulableAction;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.ui.RSApplication;

public class PAContext extends DefaultContext<Object> implements ContextBuilder<Object>{
	//////////////////////////VARIABLES DECLARATION START//////////////////////////
		
	public static Grid<Object> grid;
	
	public static ISchedule schedule;
	public static ISchedulableAction nextClear, nextEnd, psaAnnual;
	public static Parameters p;
	public static int[] myRandomSeeds;
	public static Random myGenerator; 
	public static int deadCounter;
	//public static Util.Clock clock;
	public static Util.Clock clock = new Util.Clock();
	//clock end time check for annual recording
	public static double clockYearsAtEnd;
	
	// for adding visualisation
	public static final String GRID_ID = "grid";
	public static int gridSizeX;
	public static int gridSizeY;
	
	//population records 
	public static List selectedPopulation;
	public static int totalPopulation; // holds the number of records that match the selection criteria 
	public static int cohortSize; // holds the cohort size that is user defined from parameters
	public static boolean enoughCohort = true;
	
	// Covid variables
	public static double infectiousAgentCount;
	public static double agentCount;
	public static double propInfectiousAgents;
	public static int unvaccinatedCount;
	public static double proportionHospital;
	public static int covidIntervention;
	public static double initialSusceptible;
	public static double initialExposed;
	public static int introAgeMinCovidIntervention;
	public static int introAgeMaxCovidIntervention;
	public static int vaccineStratified;
	
	//Declare run parameters
	public static int replications; // Number of replications
	public static int repCounter;
	public static double warmup; // Warm-up time for each replication
	public static double endTime; // Length of each replication
	
	//Output variables
	public static boolean individualRecordOn = false;
	
	//Declare data lists
	public static Data peopleData = new Data();
	public static Data replicationsData = new Data();
	
	//Declare array lists for risk coefficients
	public static Util.My4DArray<String> qRiskCoef = new Util.My4DArray<String>();	
	public static Util.My4DArray<String> dRiskCoef = new Util.My4DArray<String>();
	public static Util.My4DArray<String> qCovidCoef = new Util.My4DArray<String>();
	
	//Declare array lists for probabilities
	public static Util.My2DArray<Double> proportionStrokeCHD = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> probDepression = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> probMSI = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrMortality = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrPAOnCHD = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrPAOnStroke = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrPAOnDiabetes = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrPAOnDepression = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> rrPAOnMSI = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> fatalityCHD = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> fatalityStroke = new Util.My2DArray<Double>();

	//Declare array lists for covid data
	public static Util.My2DArray<Double> covidCharacteristics = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> covidInterventions = new Util.My2DArray<Double>();
	public static Util.My2DArray<Double> covidCosts = new Util.My2DArray<Double>();
	
	
	//Life tables --- reads annual mortality probability by age --- they are converted into the model for the time unit     
	public static Double[] mortality3monthsMaleHealthyNonCVD = new Double[101];
	public static Double[] mortality3monthsMaleHealthyCVD = new Double[101];
	public static Double[] mortality3monthsFemaleHealthyNonCVD = new Double[101];
	public static Double[] mortality3monthsFemaleHealthyCVD = new Double[101];
	
	//Declare array lists for statistics
	public static ArrayList<Stats> psaStatsList = new ArrayList<Stats>();
	public static ArrayList<Stats> runStatsList = new ArrayList<Stats>();
	
	//Declare array lists for annual statistics
	public static ArrayList<AnnualPSAOutput> psaAnnualStatsList = new ArrayList<AnnualPSAOutput>();
	
	//public static ArrayList<ArrayList<Stats>> runAnnualStatsList = new ArrayList<ArrayList<Stats>>();
	
	//public static Util.My2DArray<Stats> runStats = new Util.My2DArray<Stats>();
	//////////////////////////VARIABLES DECLARATION END//////////////////////////
	
	
	
	
	@Override
	public Context<Object> build(Context<Object> context) {
		
		schedule = RunEnvironment.getInstance().getCurrentSchedule();
		p = RunEnvironment.getInstance().getParameters();
		//initialise replication counter *will be increased within initialise() method --> initialisation before each replication
		repCounter = 0;
		//clock = new Util.Clock();
		clock.resetClock();
		
		//clear data for starting a new run of the simulation (a different reset method for iterations)    
		peopleData.clearData();
		replicationsData.clearData();
		qRiskCoef.clear();	
		dRiskCoef.clear();	
		proportionStrokeCHD.clear();
		probDepression.clear();
		probMSI.clear();
		rrMortality.clear();
		rrPAOnCHD.clear();
		rrPAOnStroke.clear();
		rrPAOnDiabetes.clear();
		rrPAOnDepression.clear();
		rrPAOnMSI.clear();
		fatalityCHD.clear();
		fatalityStroke.clear();
		psaAnnualStatsList.clear();

		covidCharacteristics.clear();
		covidInterventions.clear();
		covidCosts.clear();
		qCovidCoef.clear();
	
		//initialise variables from parameters
		replications = (Integer)p.getValue("replications");
		warmup = (Double)p.getValue("warmup");
		endTime = ((double) p.getValue("endTime")) * Util.CONVERT_YEARS_CALMS; 
		cohortSize = (Integer)p.getValue("cohortSize");
		
		//INITIALISE COVID PARAMS
		covidIntervention = (Integer)p.getValue("covidIntervention");
		initialSusceptible = (Double)p.getValue("initialSusceptible");
	    initialExposed = (Double)p.getValue("initialExposed");
	    introAgeMaxCovidIntervention = (Integer)p.getValue("maxCAIntroAge");
	    introAgeMinCovidIntervention = (Integer)p.getValue("minCAIntroAge");
	    vaccineStratified = (Integer)p.getValue("vaccineStratified");
	    
		unvaccinatedCount = cohortSize;
		
			////////

		    Util.readPeopleDataFiles(DataFiles.PEOPLE_DATA_FILE, peopleData);
		  //replications, risk and probability data is read after checking for enough records in the selected population
		  			
			//TODO reset variables 
			
			selectedPopulation = Util.selectPopulation(peopleData);
		
			
			if (enoughCohort){
				Util.readReplicationsDataFiles(DataFiles.EVENTS_DATA_FILE.toString(), DataFiles.DATA_DIRECTORY.toString());
				Util.readReplicationsDataFiles(DataFiles.INTERVENTIONS_DATA_FILE.toString(), DataFiles.DATA_DIRECTORY.toString());
				
				Util.readRiskCoef(DataFiles.QRISK_COEF_DATA_FILE);
				Util.readRiskCoef(DataFiles.DRISK_COEF_DATA_FILE);
				Util.read2DArrayData(DataFiles.PROPORTION_STROKE_CHD,proportionStrokeCHD);
				Util.read2DArrayData(DataFiles.DEPRESSION_PROBABILITY,probDepression);
				Util.read2DArrayData(DataFiles.MSI_PROBABILITY,probMSI);
				Util.read2DArrayData(DataFiles.MORTALITY_RR, rrMortality);
				Util.read2DArrayData(DataFiles.RR_PA_ON_CHD, rrPAOnCHD);
				Util.read2DArrayData(DataFiles.RR_PA_ON_STROKE, rrPAOnStroke);
				Util.read2DArrayData(DataFiles.RR_PA_ON_DIABETES, rrPAOnDiabetes);
				Util.read2DArrayData(DataFiles.RR_PA_ON_DEPRESSION, rrPAOnDepression);
				Util.read2DArrayData(DataFiles.RR_PA_ON_MSI, rrPAOnMSI);
				Util.readMortality3monthsProbability(DataFiles.MORTALITY_DATA);
				Util.read2DArrayData(DataFiles.FATALITY_CHD, fatalityCHD);
				Util.read2DArrayData(DataFiles.FATALITY_STROKE, fatalityStroke);				
				// COVID FILES
				Util.read2DArrayData(DataFiles.Covid_Data, covidCharacteristics);
				Util.read2DArrayData(DataFiles.Covid_Intervention_Data, covidInterventions);
    			Util.read2DArrayData(DataFiles.Cost_Data, covidCosts);
    			Util.readRiskCoef(DataFiles.QCovid_Coef_Data_File);
		
				Util.psaErrorCheck();
			}
	
			
				//the grid size depends on user specified cohort size 
				gridSizeX = (int) Math.ceil(Math.sqrt(cohortSize));
				gridSizeY = (int) Math.round(Math.sqrt(cohortSize));
		 		
				// Create a grid on which agents are located at
					
				grid = GridFactoryFinder.createGridFactory(null)
						.createGrid(GRID_ID, context,
								new GridBuilderParameters<Object>(
								new repast.simphony.space.grid.StrictBorders(),
								new SimpleGridAdder<Object>(),
										// Each cell in the grid is multi-occupancy?
										false,
										gridSizeX, gridSizeY));
		
				context.add(clock);
				
				//COVID INITIALISATIONS
				
				// count total infectious agents
		        for (Object obj : context) {
		                if(obj instanceof Agent){
		                        if (((Agent) obj).infected == 1) 
		                        {
		                        	infectiousAgentCount ++; }
			         }
		         }
		        // count total living agents
		        for (Object obj : context) {
		            if(obj instanceof Agent){
		                    if (((Agent) obj).isDead == false) 
		                    {
		                    	agentCount ++; }
		           }
		        }
		        
				
				//Create the random seeds sequence for this trial
				myRandomSeeds = Util.createRandomSeed(replications);
				
				//1. Call the initialise() method
				schedule.schedule(ScheduleParameters.createOneTime(0), this, "initialise");
				
				//CALMS: CALL COVID METHODS EACH TIME STEP
				schedule.schedule(ScheduleParameters.createRepeating(1, 1), 
		                ()-> run());
				
				
		return context;
	}

	//initialise before each replication

	public static void initialise(){
		clock.resetClock();
		clockYearsAtEnd = 1000;
		deadCounter = 0;
		
		//resetStats(repCounter);
		p.setValue("randomSeed", myRandomSeeds[repCounter]);
		
		//Random number generator (0.0-1.0) for this replication (different for each replication)
		myGenerator = new Random((int) p.getValue("randomSeed")); 
		
		
		if(repCounter==0){
			createStatsLists();
			populateGrid();
		}
		else{
			initialisePSAStats();
			nnewCohort();
		}
		initialiseAnnualPSAStats();
		repCounter++;
		Agent.repReadData = repCounter-1;
		
		///////Do not need warmup 
		//nextClear = schedule.schedule(ScheduleParameters.createOneTime(schedule.getTickCount()+warmup, ScheduleParameters.LAST_PRIORITY), this, "clearStats");
		//TODO reset(); DONE
				
		///////Only when users set end time
		///////*** if the run ends when all agents die
		///////The end() method is called from Agent class  
	
		PAContext pc = new PAContext();
		nextEnd = schedule.schedule(ScheduleParameters.createOneTime(schedule.getTickCount()+endTime, ScheduleParameters.LAST_PRIORITY), pc, "end");
	}
	
	public static void createStatsLists(){
		//Stats lists for psa
		Stats psaLifeYears = new Stats();
		Stats psaEvents = new Stats();
		Stats psaCVDEvents = new Stats();
		Stats psaMSIEvents = new Stats();
		Stats psaDprsEpisodes = new Stats();
		Stats psaStrokeEvents = new Stats();
		Stats psaCHDEvents = new Stats();
		Stats psaT2DEvents = new Stats();
		Stats psaYearsActive = new Stats();
		Stats psaEventCost = new Stats();
		Stats psaEventCostDiscounted = new Stats();
		// 

		Stats psaTimesInfected = new Stats();
		Stats psaTimesHospitalised = new Stats();
		Stats psaTimesICU = new Stats();
		Stats psaDieOfCovid = new Stats();
		Stats psaTimesLongCovid = new Stats();
		Stats psaTotalHospitalCost = new Stats();
		Stats psaTotalICUCost = new Stats();
		Stats psaTotalLongCovidCost = new Stats();
		Stats psaTotalLockdownCost = new Stats();
		Stats psaTotalVaccinationCost = new Stats();
		Stats psaTotalHealthCostCovid = new Stats();
		Stats psaTotalInterventionCostCovid = new Stats();
		Stats psaTotalCostCovid = new Stats();
		Stats psaTimesVaccinated = new Stats();
		//
		
		psaStatsList.add(psaLifeYears);
		psaStatsList.add(psaEvents);
		psaStatsList.add(psaCVDEvents);
		psaStatsList.add(psaMSIEvents);
		psaStatsList.add(psaDprsEpisodes);
		psaStatsList.add(psaStrokeEvents);
		psaStatsList.add(psaCHDEvents);
		psaStatsList.add(psaT2DEvents);
		psaStatsList.add(psaYearsActive);
		psaStatsList.add(psaEventCost);
		psaStatsList.add(psaEventCostDiscounted);	
		// 
		psaStatsList.add(psaTimesInfected);
		psaStatsList.add(psaTimesHospitalised);
		psaStatsList.add(psaTimesICU);
		psaStatsList.add(psaDieOfCovid);
		psaStatsList.add(psaTimesLongCovid);
		psaStatsList.add(psaTotalHospitalCost);
		psaStatsList.add(psaTotalICUCost);
		psaStatsList.add(psaTotalLongCovidCost);
		psaStatsList.add(psaTotalLockdownCost);
		psaStatsList.add(psaTotalVaccinationCost);
		psaStatsList.add(psaTotalHealthCostCovid);
		psaStatsList.add(psaTotalInterventionCostCovid);
		psaStatsList.add(psaTotalCostCovid);
		psaStatsList.add(psaTimesVaccinated);
		//
		
		//Stats lists for run
				Stats runLifeYears = new Stats();
				Stats runEvents = new Stats();
				Stats runCVDEvents = new Stats();
				Stats runMSIEvents = new Stats();
				Stats runDprsEpisodes = new Stats();
				Stats runStrokeEvents = new Stats();
				Stats runCHDEvents = new Stats();
				Stats runT2DEvents = new Stats();
				Stats runYearsActive = new Stats();
				Stats runEventCost = new Stats();
				Stats runEventCostDiscounted = new Stats();
				// 
				Stats runTimesInfected = new Stats();
				Stats runTimesHospitalised = new Stats();
				Stats runTimesICU = new Stats();
				Stats runDieOfCovid = new Stats();
				Stats runTimesLongCovid = new Stats();
				Stats runTotalHospitalCost = new Stats();
				Stats runTotalICUCost = new Stats();
				Stats runTotalLongCovidCost = new Stats();
				Stats runTotalLockdownCost = new Stats();
				Stats runTotalVaccinationCost = new Stats();
				Stats runTotalHealthCostCovid = new Stats();
				Stats runTotalInterventionCostCovid = new Stats();
				Stats runTotalCostCovid = new Stats();
				Stats runTimesVaccinated = new Stats();
				//		
								
				runStatsList.add(runLifeYears);
				runStatsList.add(runEvents);
				runStatsList.add(runCVDEvents);
				runStatsList.add(runMSIEvents);
				runStatsList.add(runDprsEpisodes);
				runStatsList.add(runStrokeEvents);
				runStatsList.add(runCHDEvents);
				runStatsList.add(runT2DEvents);
				runStatsList.add(runYearsActive);
				runStatsList.add(runEventCost);
				runStatsList.add(runEventCostDiscounted);
				
				//
				runStatsList.add(runTimesInfected);
				runStatsList.add(runTimesHospitalised);
				runStatsList.add(runTimesICU);
				runStatsList.add(runDieOfCovid);
				runStatsList.add(runTimesLongCovid);
				runStatsList.add(runTotalHospitalCost);
				runStatsList.add(runTotalICUCost);
				runStatsList.add(runTotalLongCovidCost);
				runStatsList.add(runTotalLockdownCost);
				runStatsList.add(runTotalVaccinationCost);
				runStatsList.add(runTotalHealthCostCovid);
				runStatsList.add(runTotalInterventionCostCovid);
				runStatsList.add(runTotalCostCovid);
				runStatsList.add(runTimesVaccinated);
				//
		
	}
	
	
	
	public static void initialisePSAStats(){
		for (Stats s: psaStatsList){
			s.initialise();
		}
	} 
	
	public static void initialiseAnnualPSAStats(){
		for (AnnualPSAOutput apo: psaAnnualStatsList){
			apo.initialiseAnnualPSAStats();
		}
	}
	
	
	public static void recordRunStats(){
		for (int i = 0; i < runStatsList.size(); i++){
			runStatsList.get(i).recordDT(psaStatsList.get(i).getAverage());
		}
	}
	
	public static void recordPSAStats(){
		Context context = RunState.getInstance().getMasterContext();
		for (Object obj : context) {
			if(obj instanceof Agent){
				int idx = 0;
				while(idx < psaStatsList.size()){
					psaStatsList.get(idx++).recordDT(((Agent) obj).getLifeYears());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalNoOfEvents());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalNoOfCVD());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getNoOfMSI());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getNoOfDprsEpisodes());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getNoOfStroke());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getNoOfCHD());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getNoOfT2D());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getYearsActive());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalEventCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalEventCostDiscounted());
					 
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTimesInfected());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTimesHospitalised());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTimesICU());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getDiedOfCovid());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTimesLongCovid());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalHospitalCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalICUCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalLongCovidCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotallockdownCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalVaccineCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalHealthCostCovid());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalInterventionCostCovid());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTotalCovidCost());
					psaStatsList.get(idx++).recordDT(((Agent) obj).getTimesVaccinated());

				}
			}
						
		}
		
	}
	
	public static void populateGrid(){
		Context context = RunState.getInstance().getMasterContext();
			
			int coh = 0;
			if(!(totalPopulation == 0)){
				while (coh<cohortSize){
				
					if (coh>=cohortSize){
					break;
					}
					
					int i = (int) Math.ceil(myGenerator.nextDouble()*(totalPopulation-1));
					Record r = (Record) selectedPopulation.get(i);
					Agent a = new Agent(r.getInModelID());
					Util.initialiseAgentsFromData(a, r); 
					context.add(a);
					
				
				coh++;
				}
			
				for (Object obj : context) {
					if (obj instanceof Agent){
						for(int y = 0; y < gridSizeY; y++){
							for (int x = 0; x< gridSizeX; x++){
								grid.moveTo(obj, x, y);
							}
						}
					}			
				}
			}
	}
	
	public static void nnewCohort(){
		
		Context context = RunState.getInstance().getMasterContext();
		
		for (Object obj : context) {
			if (obj instanceof Agent){
				
				Util.resetAgent((Agent) obj);
				
				int i = (int) Math.ceil(myGenerator.nextDouble()*(totalPopulation-1));
				Record r = (Record) selectedPopulation.get(i);
				
				Util.initialiseAgentsFromData((Agent) obj, r);
				((Agent) obj).setInModelID(r.getInModelID());
				
			}	
		}
	}
	
	// Covid methods

    public static void run() {
    	totalInfectious();   // calculate total number of infectious agents (used to calculate infection prob)
    	totalAgents();    
    	calculateProportionInfectious();
    	totalUnvaccinated();    // calculates total number of vaccinated agents - used to calculate the number who will be vaccinated per day
    	calculateProportionHospital();  // calculates proportion of agents in hospital - used to calculate hospital threshold for periodic lockdown scenario. 
    }
    
	public static void totalInfectious() {
		Context context = RunState.getInstance().getMasterContext();
    	infectiousAgentCount = 0; // reset to 0 first so we get a daily count rather than a cumulative count
        for (Object obj : context) {
                if(obj instanceof Agent){
                        if (((Agent) obj).infected == 1) 
                        {
                        	infectiousAgentCount ++;
                        	}
                }
        }
   }
	
	public static void totalAgents() {
		Context context = RunState.getInstance().getMasterContext();
		agentCount = 0;
        for (Object obj : context) {
                if(obj instanceof Agent){
                        if (((Agent) obj).isDead == false) 
                        {
                        	agentCount ++;
                        	}
	              }
           }
   }
	
	public static void calculateProportionInfectious() {
		propInfectiousAgents = (infectiousAgentCount / agentCount); 
	}
	
	public static void totalUnvaccinated() {
		Context context = RunState.getInstance().getMasterContext();
		unvaccinatedCount = 0;
        for (Object obj : context) {
                if(obj instanceof Agent){
                        if (((Agent) obj).vaccinated == 0) 
                        {
                        	unvaccinatedCount ++;
                        	}
	            }
           }
	}
	
	public static void calculateProportionHospital() {
		Context context = RunState.getInstance().getMasterContext();
		int hospitalCount = 0;
        for (Object obj : context) {
                if(obj instanceof Agent){
                        if (((Agent) obj).getHospitalised() == 1) 
                        {
                        	hospitalCount ++;
                        	}
	            }
           }
        proportionHospital = hospitalCount / agentCount; 
        		
	}
	
	
	/*
	 * TODO WARM UP NOT IMPLEMENTED
	// Clear statistics after warmup period
	public void clearStats() {
		for (Stat s:sList)
			s.initialise();
		arrivals = 0;
		departures = 0;
	}
	*/
	//end of each replication
	public static void end(){
		
		clockYearsAtEnd = clock.getClockYears();
		
		if (individualRecordOn){
			Context context = RunState.getInstance().getMasterContext();
			for (Object obj : context) {
				if (obj instanceof Agent){
					((Agent) obj).setAgeAtSimEnd(((Agent) obj).getAge());
					((Agent) obj).setPAStatusAtSimEnd(((Agent) obj).getPAStatus());
					((Agent) obj).setBMIAtEnd(((Agent) obj).getBMI());
					((Agent) obj).setHDLRatioAtEnd(((Agent) obj).getHDLRatio());
					((Agent) obj).setSBPAtEnd(((Agent) obj).getSBloodPressure());
					
					Util.writeOutIndividual((Agent) obj);
					
				}	
			}
		}
		
		recordPSAStats();
		recordRunStats();
		
		// Note: due to the scheduling in initialising Stats, the AnnualPSA output does not include the final year of the simulation (all outputs are set to 0).
	    if(!(clock.getClockYears()%1==0)){Util.recordAnnualPSAOutput();}
		
		Util.writeOut(psaStatsList, Util.currentOutDirectory.getPath()+OutputFiles.PSA.toString().trim()+".csv");//+Util.dateAppend+".csv");
		Util.writeOutAnnual(psaAnnualStatsList, Util.currentOutDirectory.getPath()+OutputFiles.ANNUAL_PSA.toString().trim()+".csv");//+Util.dateAppend+".csv");
		if (repCounter < replications){
			schedule.removeAction(nextEnd);
			PAContext pa = new PAContext(); 
			schedule.schedule(ScheduleParameters.createOneTime(Util.getCurrentTime()+1), pa, "initialise");
		}
		else {
			Util.writeOut(runStatsList, Util.currentOutDirectory.getPath()+OutputFiles.STRATEGY.toString().trim()+".csv");//+Util.dateAppend+".csv");
			RunEnvironment.getInstance().endRun();
		}
	}
}
