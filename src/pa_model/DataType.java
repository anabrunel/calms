package pa_model;

public enum DataType {
	
	PEOPLE, EVENT;
	
	public String toString(){
		switch(this){
		case PEOPLE: return "People";
		case EVENT: return "Event";
		
		default: return "No Data Type";
		}
	}

}
