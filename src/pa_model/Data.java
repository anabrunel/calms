package pa_model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Data {
	
	private List data;
	
	
	public Data(){
		data = new ArrayList();
	}
	
	
	public void clearData(){
	data.clear();
	}
	
	public void addRecord(int inModelID, String type, RecordProperties prop){
		Record record = new Record(inModelID, type, prop);
		data.add(record);
	}
	
	
	public void addRecord(int id, String type, String name, ReplicationData rep){
		Record record = new Record(id, type, name, rep);
		data.add(record);
	}
	
	public List search (RecordProperties searchProp){
		List matchingRecords = new ArrayList();
		
		for (Iterator i = data.iterator();i.hasNext();){
			Record record = (Record)i.next();
				if(record.getProp().isMatch(searchProp)){
					// select range 
					double d1 = Double.valueOf(record.getProp().getProperty(PeopleProperties.AGE.toString()).toString().trim());
					double d2 = Double.valueOf(PAContext.p.getValueAsString("minAge").trim());	
					double d3 = Double.valueOf(PAContext.p.getValueAsString("maxAge").trim());	
				
					if((d1>=d2) && (d1<=d3)){
						
						double b1 = Double.valueOf(record.getProp().getProperty(PeopleProperties.BMI.toString()).toString().trim());
						double b2 = Double.valueOf(PAContext.p.getValueAsString("minBMI").trim());	
						double b3 = Double.valueOf(PAContext.p.getValueAsString("maxBMI").trim());
						
						if((b1>=b2) && (b1<=b3)){
							
							double p1 = Double.valueOf(record.getProp().getProperty(PeopleProperties.PA_STATUS.toString()).toString().trim());
							double p2 = Double.valueOf(PAContext.p.getValueAsString("minPAStatus").trim());	
							double p3 = Double.valueOf(PAContext.p.getValueAsString("maxPAStatus").trim());
								
							if((p1>=p2) && (p1<=p3)){
							matchingRecords.add(record);
							}
							
						}
							
					}	
				}
		}
		
		return matchingRecords;
	}
	
	public ArrayList getDataList(){
		return (ArrayList) data;
	}
	
	public void getAllData(){
		
		for (Iterator i = data.iterator();i.hasNext();){
			Record repRec = (Record) i.next();
			
		}	
	}
	
	public void getReplicationData(String type, String name){
		
		for (Iterator i = data.iterator();i.hasNext();){
			Record repRec = (Record) i.next();
		}	
	}

}