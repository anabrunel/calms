package pa_model;

import java.awt.Color;

import repast.simphony.random.RandomHelper;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;



public class AgentStyle extends DefaultStyleOGL2D {
  
	
	
	   @Override 

		       public Color getColor(Object o) 
		       { 
				   
				    double r = RandomHelper.nextDoubleFromTo(0.0, Util.PA_MODERATELY_THRESHOLD-1);
				    double r1 = RandomHelper.nextDoubleFromTo(Util.PA_MODERATELY_THRESHOLD, Util.PA_VERY_THRESHOLD-1);
				    
				    if(o instanceof Agent) { 
		                Agent a = (Agent) o;
		                
		                
		                if(a.getIsDead()){return Color.BLACK;}
		                
		                else if (a.getSeverity()==3){
		             	   return Color.RED;
		                }
		                else if (a.getSeverity()==2){
		             	   return Color.ORANGE;
		                }
		                else if (a.getSeverity()==1){
			             	   return Color.green;
			                }
		                		                                       
		                else { 
		                        return Color.LIGHT_GRAY; 
		                }
		                
		                
		        }
		    
               
              return null;
       } 
	  

	  
	   
	   
}
