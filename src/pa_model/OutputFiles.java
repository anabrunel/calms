package pa_model;

import java.text.SimpleDateFormat;
import java.util.Date;



public enum OutputFiles {
	
	INDIVIDUAL, PSA, STRATEGY,/* ANNUAL_INDIVIDUAL,*/ ANNUAL_PSA, /*ANNUAL_STRATEGY,*/ /*OUT_DIRECTORY*/;
	
	
	//String dateAppend = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
	
	public String toString(){
		switch(this){
		
		case INDIVIDUAL: return "/OutIndividual";
		case PSA: return "/OutCohortPSA";
		case STRATEGY: return "/OutCohortStrategy";
		case ANNUAL_PSA: return "/AnnualPSA";

		default: return "No Output File";
		}
	}
	

}
