package pa_model;

public class Stats {
	
	private double total, totalSq, count, lastTime, currentTime;
	
	public Stats(){
		initialise();
	}
	
	public void initialise(){
		
		total = 0;
		totalSq = 0;
		count = 0;
		lastTime = Util.getCurrentTime();
	}

	// Record continuous-time statistic
		public void recordCT(double value) {
			currentTime = Util.getCurrentTime(); 
			total += value*(currentTime-lastTime);
			count += currentTime-lastTime;
			lastTime = currentTime;
		}
		
		// Record discrete-time statistic
		public void recordDT(double value) {
			count++;
			total += value;
			totalSq += value*value;
			
		}
		
		// Return average
		public double getAverage() {
			if (count > 0)
				return total/count;
			else
				return 0;
		}
		
		//Return total
		public double getTotal() {
			return total;
		}
		
		//  Welford's algorithm
		public double getVariance(){
			if (count > 0)
				return (totalSq - count*Math.pow(this.getAverage(), 2))/count;
				
			else
				return 0;
			
		}
		
		// Return standard deviation (relevant only for discrete time statistics) Welford's algorithm
		public double getStDev() {
			if (count > 0)
				return Math.sqrt(this.getVariance());
			else
				return 0;
		}
	
		
	
}
