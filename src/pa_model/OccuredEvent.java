package pa_model;

import repast.simphony.random.RandomHelper;

public class OccuredEvent {
	
	private double startTime, ageAtStart, paAtStart;
	
	private String eventName;
	private int acuteDuration;
	private double acuteCost, acuteUtilMult, acutePA, postAcuteCost, postAcuteUtilMult,
	postAcutePAInactiveMmean, postAcutePAInactiveMsd, postAcutePAModActiveMmean, postAcutePAModActiveMsd, postAcutePAVeryActiveMmean, postAcutePAVeryActiveMsd,
	postAcutePAInactiveFmean, postAcutePAInactiveFsd, postAcutePAModActiveFmean, postAcutePAModActiveFsd, postAcutePAVeryActiveFmean, postAcutePAVeryActiveFsd;
	
	
	public OccuredEvent(Record rec, Agent a){
		
		startTime = Util.getCurrentTime();
		ageAtStart = a.getAge();
		paAtStart = a.getPAStatus();
		eventName = rec.getEventName(a.repReadData);
		acuteDuration = rec.getEventAcuteDurationInMonths(a.repReadData);
		acuteCost = rec.getEventAcuteCostPerMonth(a.repReadData); 
		acuteUtilMult = rec.getEventAcuteUtilityMultiplier(a.repReadData);
		acutePA = rec.getEventAcutePA(a.repReadData);
		postAcuteCost = rec.getEventPostAcuteCostPerMonth(a.repReadData);
		postAcuteUtilMult = rec.getEventPostAcuteUtilityMultiplier(a.repReadData);
		postAcutePAInactiveMmean = rec.getEventPostAcutePAInactiveMmean(a.repReadData);
		postAcutePAInactiveMsd = rec.getEventPostAcutePAInactiveMsd(a.repReadData);
		postAcutePAModActiveMmean = rec.getEventPostAcutePAModeratelyActiveMmean(a.repReadData);
		postAcutePAModActiveMsd = rec.getEventPostAcutePAModeratelyActiveMsd(a.repReadData);
		postAcutePAVeryActiveMmean = rec.getEventPostAcutePAVeryActiveMmean(a.repReadData);
		postAcutePAVeryActiveMsd = rec.getEventPostAcutePAVeryActiveMsd(a.repReadData);
		postAcutePAInactiveFmean = rec.getEventPostAcutePAInactiveFmean(a.repReadData);
		postAcutePAInactiveFsd = rec.getEventPostAcutePAInactiveFsd(a.repReadData);
		postAcutePAModActiveFmean = rec.getEventPostAcutePAModeratelyActiveFmean(a.repReadData);
		postAcutePAModActiveFsd = rec.getEventPostAcutePAModeratelyActiveFsd(a.repReadData);
		postAcutePAVeryActiveFmean = rec.getEventPostAcutePAVeryActiveFmean(a.repReadData);
		postAcutePAVeryActiveFsd = rec.getEventPostAcutePAVeryActiveFsd(a.repReadData);
	}

	
	public double getStartTime(){
		return this.startTime;
	}
	
	public double getAgeATStart(){
		return this.ageAtStart;
	}
	
	public double getPAATStart(){
		return this.paAtStart;
	}
	
	public String getEventName(){
		return this.eventName;
	}
	
	public double getEventAcuteDuration(){
		return this.acuteDuration;
	}
	
	public double getEventAcuteCost(){
		return this.acuteCost;
	}
	
	public double getEventAcuteUtilityMultiplier(){
		return this.acuteUtilMult;
	}
	
	public double getEventAcutePA(){
		return this.acutePA;
	}
	
	public double getEventPostAcuteCost(){
		return this.postAcuteCost;
	}
	
	public double getEventPostAcuteUtilityMultiplier(){
		return this.postAcuteUtilMult;
	}
	
	public double getEventPostAcutePAInactiveMmean(){
		return this.postAcutePAInactiveMmean;
	}
	
	public double getEventPostAcutePAInactiveMsd(){
		return this.postAcutePAInactiveMsd;
	}
	
	public double getEventPostAcutePAModeratelyActiveMmean(){
		return this.postAcutePAModActiveMmean;
	}
	
	public double getEventPostAcutePAModeratelyActiveMsd(){
		return this.postAcutePAModActiveMsd;
	}
	
	public double getEventPostAcutePAVeryActiveMmean(){
		return this.postAcutePAVeryActiveMmean;
	}
	
	public double getEventPostAcutePAVeryActiveMsd(){
		return this.postAcutePAVeryActiveMsd;
	}
	
	public double getEventPostAcutePAInactiveFmean(){
		return this.postAcutePAInactiveFmean;
	}
	
	public double getEventPostAcutePAInactiveFsd(){
		return this.postAcutePAInactiveFsd;
	}
	
	public double getEventPostAcutePAModeratelyActiveFmean(){
		return this.postAcutePAModActiveFmean;
	}
	
	public double getEventPostAcutePAModeratelyActiveFsd(){
		return this.postAcutePAModActiveFsd;
	}
	
	public double getEventPostAcutePAVeryActiveFmean(){
		return this.postAcutePAVeryActiveFmean;
	}
	
	public double getEventPostAcutePAVeryActiveFsd(){
		return this.postAcutePAVeryActiveFsd;
	}
	
	public void setPostPAStatus (Agent a){
		double pa;
		double mean;
		double sd;
		double paChange;
		/*
		System.out.println("IN set post acute pa status");
		System.out.println("pa = "+a.getPAStatus());
		System.out.println("pa previous = "+a.getPAPreviousPeriod());
		*/
		if(a.getGender().equalsIgnoreCase("M")){
			if(a.getPAStatus() < Util.PA_MODERATELY_THRESHOLD){
				mean = this.getEventPostAcutePAInactiveMmean();
				sd = this.getEventPostAcutePAInactiveMsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange; 
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
			else if(a.getPAStatus() >= Util.PA_MODERATELY_THRESHOLD && a.getPAStatus() < Util.PA_VERY_THRESHOLD){
				mean = this.getEventPostAcutePAModeratelyActiveMmean();
				sd = this.getEventPostAcutePAModeratelyActiveMsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange;
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
			else {
				mean = this.getEventPostAcutePAVeryActiveMmean();
				sd = this.getEventPostAcutePAVeryActiveMsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange;
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
		}
		else{
			if(a.getPAStatus() < Util.PA_MODERATELY_THRESHOLD){
				mean = this.getEventPostAcutePAInactiveFmean();
				sd = this.getEventPostAcutePAInactiveFsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange;
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
			else if(a.getPAStatus() >= Util.PA_MODERATELY_THRESHOLD && a.getPAStatus() < Util.PA_VERY_THRESHOLD){
				mean = this.getEventPostAcutePAModeratelyActiveFmean();
				sd = this.getEventPostAcutePAModeratelyActiveFsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange;
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
			else {
				mean = this.getEventPostAcutePAVeryActiveFmean();
				sd = this.getEventPostAcutePAVeryActiveFsd();
				paChange = RandomHelper.createNormal(mean, sd).nextDouble();
				pa = this.getPAATStart() + paChange;
				if(pa<0){pa=0;}
				a.setPAStatus(pa);
			}
		}
		/*
		System.out.println("IN set post acute pa status END");
		System.out.println("pa = "+a.getPAStatus());
		System.out.println("pa previous = "+a.getPAPreviousPeriod());
		System.out.println("DATA");
		System.out.println(mean);
		System.out.println(sd);
		System.out.println(paChange);
		System.out.println("DATA");
		*/
	}
	
	
}