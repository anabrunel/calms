package pa_model;

public enum DataFiles {
	
	PEOPLE_DATA_FILE, INTERVENTIONS_DATA_FILE, EVENTS_DATA_FILE, DATA_DIRECTORY, QRISK_COEF_DATA_FILE, DRISK_COEF_DATA_FILE, PROPORTION_STROKE_CHD, DEPRESSION_PROBABILITY, MSI_PROBABILITY, MORTALITY_DATA, MORTALITY_RR,
	RR_PA_ON_DIABETES, RR_PA_ON_STROKE, RR_PA_ON_CHD, RR_PA_ON_DEPRESSION, RR_PA_ON_MSI, FATALITY_CHD, FATALITY_STROKE,

	Covid_Intervention_Data, Cost_Data, Covid_Data, QCovid_Coef_Data_File;
	
	
	public String toString(){
		switch(this){
		case PEOPLE_DATA_FILE: return "data/PeopleInputData.csv";
		case EVENTS_DATA_FILE: return "EventsInputData";
		case DATA_DIRECTORY: return "data/";
		case QRISK_COEF_DATA_FILE: return "data/QRiskCoef.csv";
		case DRISK_COEF_DATA_FILE: return "data/DRiskCoef.csv";
		case PROPORTION_STROKE_CHD: return "data/ProportionStrokeCHD.csv";
		case DEPRESSION_PROBABILITY: return "data/DepressionProbability.csv";
		case MSI_PROBABILITY: return "data/MSIProbability.csv";
		case MORTALITY_DATA: return "data/MortalityData.csv";
		case MORTALITY_RR: return "data/MortalityRR.csv";
		case RR_PA_ON_DIABETES: return "data/rrPAonDiabetes.csv";
		case RR_PA_ON_STROKE: return "data/rrPAonStroke.csv";
		case RR_PA_ON_CHD: return "data/rrPAonCHD.csv";
		case RR_PA_ON_DEPRESSION: return "data/rrPAonDepression.csv";
		case RR_PA_ON_MSI: return "data/rrPAonMSI.csv";
		case FATALITY_CHD: return "data/fatalityCHD.csv";
		case FATALITY_STROKE: return "data/fatalityStroke.csv";
		
		case Cost_Data: return "data/Costs.csv";
		case Covid_Data: return "data/CovidData.csv";
		case Covid_Intervention_Data: return "data/InterventionDataCovid.csv";
		case QCovid_Coef_Data_File: return "data/QCovidCoef.csv";
		
		default: return "No Data File";
		}
	}

}
