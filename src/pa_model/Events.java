package pa_model;

public enum Events {
	
	CHD, STROKE, T2D, MSI, DEPRESSION;
	
	public String toString(){
		switch(this){
		case CHD: return "CHD";
		case STROKE: return "Stroke";
		case T2D: return "T2D";
		case MSI: return "MSI";
		case DEPRESSION: return "Depression";
		default: return " ";
		}
	}

}

