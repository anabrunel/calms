package pa_model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JOptionPane;

import cern.jet.random.Uniform;
import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.ui.RSApplication;
import repast.simphony.random.RandomHelper;

public class Util {
	public static final int DATA_FILES_HEADER_LINES = 2; // header lines to be ignored when reading input data files
	///////Multipliers constant in order to easily change simulation timestep 

	public static final double CONVERT_YEARS = 0.25; //multiplier to convert 3 month time step (from PALMS) to years 
	public static final double CONVERT_MONTHS = 3; //multiplier to convert 3 month time step (from PALMS) to months
	public static final double CONVERT_DAYS = 90; //multiplier to convert 3 month time step (from PALMS) time to days
	
	public static final double CONVERT_YEARS_CALMS = 360; // CALMS assumes 360 days in a year for simulation purposes (30 days pm)
	
	//For PSA error check
	private static boolean psaError = false;
	private static String psaErrorMessage = "Not enough PSA data in: ";
	
	//Discount rates
	public static final double DISCOUNT_ANNUAL_RATE_COST = 0.035;
	
	public static final double DISCOUNT_DAILY_RATE_COST = 1/Math.pow(1+DISCOUNT_ANNUAL_RATE_COST, 1/365);

	//Daily Increment
	public static final double DAILY_INCREMENT = 0.02;
	
	//TODO threshold for considering a person active 
	public static final double PA_ACTIVE_THRESHOLD = (double) PAContext.p.getValue("activeThreshold"); // for counting active years
	
	// inactive if pa < 86 PA_MODERATELY_THRESHOLD
	public static final double PA_MODERATELY_THRESHOLD = 86; // min per week - moderately active if pa >=86 & pa < 426 (PA_VERY_THRESHOLD) 
	public static final double PA_VERY_THRESHOLD = 426; // min per week - very active if pa >=426
	
	public static File currentOutDirectory; 
	public static String dateAppend;
	
		public Util(){
			
			}
		//Simulation clock - advances every day (repast tick); resets at start of each replication  
		// conversion of timesteps was necessary from PALMS (3 months) to CALMS (1 day) - risk algorithms, age and deaths are still
		// calculated every 3 months . 
		public static class Clock{
			
			private double clockYears, clock3Months, clockMonths, clockDays;
			
			public Clock(){
				resetClock();
			}
			
			@ScheduledMethod(start=0, interval=1, priority=ScheduleParameters.FIRST_PRIORITY)
			public void advanceClock(){
				
				this.setClockDays(this.getClockDays()+1); // added for CALMS
				this.setClock3Months(this.getClock3Months()+1); 
				this.setClockYears(this.getClockDays()); // changed for CALMS
				this.setClockMonths(this.getClock3Months()); 
				Util u = new Util();
				// changed for CALMS
				if((this.getClockDays()%360==0) && (this.getClockYears()<PAContext.clockYearsAtEnd)){
					
					PAContext.psaAnnual = PAContext.schedule.schedule(ScheduleParameters.createOneTime(Util.getCurrentTime(), ScheduleParameters.LAST_PRIORITY+1), u, "recordAnnualPSAOutput");
				}
				
			}
			
			public void resetClock(){
				this.clockYears = 0;
				this.clock3Months = 0;
				this.clockMonths = 0;
				// added for CALMS
				this.clockDays = 0;
			}
			
			public double getClockYears(){
				return this.clockYears;
			}
			//Changed for CALMS
			public void setClockYears(double clockDays){
			this.clockYears = clockDays/360;
		}
			public double getClock3Months(){
				return this.clock3Months;
			}
			public void setClock3Months(double clock3Months){
				this.clock3Months = clock3Months;
			}
			public double getClockMonths(){
				return this.clockMonths;
			}
			public void setClockMonths(double clock3Months){
				this.clockMonths = 3*clock3Months;
			}
			// added for CALMS
			public double getClockDays(){
				return this.clockDays;
			}
			public void setClockDays(double clockDays){
				this.clockDays = clockDays;
			}
			
		}
		
		//// EXAMPLE create random seeds sequence for replications 
		public static int[] createRandomSeed(int replications){
			int[] myRandomSeed = new int[replications];  
			Random myRandom = new Random(0);
			int i;
			for (i = 0; i<replications; i++){
				myRandomSeed[i] = myRandom.nextInt();
				
				if(myRandomSeed[i] < 0){
				
					myRandomSeed[i] = (-1)*myRandomSeed[i];
					}
			}
			return myRandomSeed;
		}

		public static double getCurrentTime(){
			return PAContext.schedule.getTickCount();
		}
		public static double getYears(Double time){
			double years = CONVERT_YEARS*time;
			return years;
		}
		public static double getMonths(Double time){
			double months = CONVERT_MONTHS*time;
			return months;
		}
		
		//Formatting to two decimal points --- for exporting to text. the calculations happen before
		public static String formatNumber(double myDouble){
			DecimalFormat formatter = new DecimalFormat("###.##");
			String out = formatter.format(myDouble);
			return out;
		}
		
		//////return an array with all filenames of the same data files in a directory
		
		public static String[] getSameTypeDataFiles(String fileName, String directory){
			int numOfDataFiles = getNumOfFilesInData(fileName);
			String filenames[] = new String[numOfDataFiles];
		
			  for (int i = 0; i < numOfDataFiles; i++) {
			      filenames[i] = directory+fileName+(i+1)+".csv";
			 
			    }
			  return filenames; 	
		}
		
		
		//////return the number of files in a directory
		
		public static int getNumOfFilesInData(String fileName){
			int numOfDataFiles = 0; 
			
			File folder = new File(DataFiles.DATA_DIRECTORY.toString());
			File[] listOfFiles = folder.listFiles();
			
			
			  for (int i = 0; i < listOfFiles.length; i++) {
			      if (listOfFiles[i].isFile() & listOfFiles[i].getName().contains(fileName)) {
			    	 
			        numOfDataFiles++;
			      } 
			    }
			  return numOfDataFiles; 	
		}
		
		
		public static void psaErrorCheck(){
			
			if (psaError){
				//JOptionPane.showMessageDialog(null, psaErrorMessage+"The simulation will terminate.", "", JOptionPane.ERROR_MESSAGE);
				JOptionPane.showMessageDialog(null, "<html><body><p style='width: 200px;'>"+psaErrorMessage+"</p>"+"<p style='font: 14pt;'>"+"<b>"+"The simulation will terminate."+"</b>"+"</p></body></html>", 
						"ERROR", JOptionPane.ERROR_MESSAGE);
				
				RSApplication.getRSApplicationInstance().getGui().getFrame().dispose();
			
			}
			else {
				individualResultsDialogue();
				try {
					createOutFiles();
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
			/*
			JOptionPane.showMessageDialog(
				    this, 
				    "<html><body><p style='width: 200px;'>"+exp.getMessage()+"</p></body></html>", 
				    "Error", 
				    JOptionPane.ERROR_MESSAGE);
			
			*/
			
			
		}
		
		///// READ FROM DATA FILES TO HASHMAP AND CREATE RECORDS 

		public static void readPeopleDataFiles(DataFiles filename, Data ddata){
			int id = 0;
			Map peoplePropMap = new HashMap();
			
			if (filename == DataFiles.PEOPLE_DATA_FILE){
				String fn = filename.toString();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HasMap
						String line = null;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
							try {
								id++;
								int idx = 0;
								
								peoplePropMap.put(PeopleProperties.ID.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.AGE.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.GENDER.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.PA_STATUS.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HAD_CVD.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HAD_T2D.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.BMI.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HDL_RATIO.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.S_BLOOD_PRESSURE.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.D_BLOOD_PRESSURE.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.SMOKING.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.ETHNICITY.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.FAMILY_CVD_HISTORY.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.DEPRIVATION_TOWN.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.BP_TREATMENT.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.RHEUMATOID_ARTHRITIS.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.CHRONIC_KIDNEY_DISEASE.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.ATRIAL_FIBRILLATION.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HAD_T1D.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.CORT.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.FAMILY_T2D_HISTORY.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.EDUCATION.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.SAMPLE_WEIGHT.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.UTILITY_IMPEQ5D.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.DEPRIVATION_IMD.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HAD_DEPRESSION.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.HAD_MSI.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.SEC.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.LLI.toString().trim(), data[idx++].trim());
								peoplePropMap.put(PeopleProperties.COMORBIDITY.toString().trim(), data[idx++].trim());
								
								RecordProperties peopleRecProp = new RecordProperties(peoplePropMap);
								ddata.addRecord(id, DataType.PEOPLE.toString(), peopleRecProp);
								
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
			}
			else{
				throw new IllegalArgumentException(
						"Error with the data files");
			}
		
		}
		
		///start read
		//parameters e.g.
		//filename DataFiles.INTERVENTIONS_DATA_FILE.toString()
		//directory DataFiles.DATA_DIRECTORY.toString()
		
		public static void readReplicationsDataFiles(String filename, String directory){
			
			
			
			String[] myFiles = Util.getSameTypeDataFiles(filename, directory);
			
			
			//Map<String, Map<String, ArrayList<String>>> replicationsData = new HashMap<String, Map<String, ArrayList<String>>>();
			//Map<String, ArrayList<String>> arrayMap = new HashMap<String, ArrayList<String>>();
			
			if(filename == DataFiles.EVENTS_DATA_FILE.toString()){
				int id =0;
				
				for(int j=0; j < myFiles.length; j++){
					
					id++;
					Map<String, Map<String, ArrayList<String>>> replicationsData = new HashMap<String, Map<String, ArrayList<String>>>();
					Map<String, ArrayList<String>> arrayMap = new HashMap<String, ArrayList<String>>();

					ArrayList<String> name = new ArrayList();
					ArrayList<String> acuteDuration = new ArrayList();
					ArrayList<String> acuteCost = new ArrayList();
					ArrayList<String> acuteUtility = new ArrayList();
					ArrayList<String> acutePA = new ArrayList();
					ArrayList<String> postAcuteCost = new ArrayList();
					ArrayList<String> postAcuteUtility = new ArrayList();
					ArrayList<String> postAcutePAInactiveMmean = new ArrayList();
					ArrayList<String> postAcutePAInactiveMsd = new ArrayList();
					ArrayList<String> postAcutePAModeratelyActiveMmean = new ArrayList();
					ArrayList<String> postAcutePAModeratelyActiveMsd = new ArrayList();
					ArrayList<String> postAcutePAVeryActiveMmean = new ArrayList();
					ArrayList<String> postAcutePAVeryActiveMsd = new ArrayList();
					ArrayList<String> postAcutePAInactiveFmean = new ArrayList();
					ArrayList<String> postAcutePAInactiveFsd = new ArrayList();
					ArrayList<String> postAcutePAModeratelyActiveFmean = new ArrayList();
					ArrayList<String> postAcutePAModeratelyActiveFsd = new ArrayList();
					ArrayList<String> postAcutePAVeryActiveFmean = new ArrayList();
					ArrayList<String> postAcutePAVeryActiveFsd = new ArrayList();
													
					String fn = myFiles[j];
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HasMap
						String line = null;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
							try {
								
								int idx = 0;
								//increase the read index before write because in csv 1st column is the PSA number
								name.add(data[++idx].trim());
								acuteDuration.add(data[++idx].trim());
								acuteCost.add(data[++idx].trim());
								acuteUtility.add(data[++idx].trim());
								acutePA.add(data[++idx].trim());
								postAcuteCost.add(data[++idx].trim());
								postAcuteUtility.add(data[++idx].trim());
								postAcutePAInactiveMmean.add(data[++idx].trim());
								postAcutePAInactiveMsd.add(data[++idx].trim());
								postAcutePAModeratelyActiveMmean.add(data[++idx].trim());
								postAcutePAModeratelyActiveMsd.add(data[++idx].trim());
								postAcutePAVeryActiveMmean.add(data[++idx].trim());
								postAcutePAVeryActiveMsd.add(data[++idx].trim());
								postAcutePAInactiveFmean.add(data[++idx].trim());
								postAcutePAInactiveFsd.add(data[++idx].trim());
								postAcutePAModeratelyActiveFmean.add(data[++idx].trim());
								postAcutePAModeratelyActiveFsd.add(data[++idx].trim());
								postAcutePAVeryActiveFmean.add(data[++idx].trim());
								postAcutePAVeryActiveFsd.add(data[++idx].trim());
																
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
					
					///put
					//replicationsData.
					arrayMap.put(EventsProperties.NAME.toString().trim(), name);
					arrayMap.put(EventsProperties.ACUTE_DURATION_MONTHS.toString().trim(), acuteDuration);
					arrayMap.put(EventsProperties.ACUTE_COST_MONTH.toString().trim(), acuteCost);
					arrayMap.put(EventsProperties.ACUTE_UTILITY_MULTIPLIER.toString().trim(), acuteUtility);
					arrayMap.put(EventsProperties.ACUTE_PA.toString().trim(), acutePA);
					arrayMap.put(EventsProperties.POST_ACUTE_COST_MONTH.toString().trim(), postAcuteCost);
					arrayMap.put(EventsProperties.POST_ACUTE_UTILITY_MULTIPLIER.toString().trim(), postAcuteUtility);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_INACTIVE_M_MEAN.toString().trim(), postAcutePAInactiveMmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_INACTIVE_M_SD.toString().trim(), postAcutePAInactiveMsd);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_M_MEAN.toString().trim(), postAcutePAModeratelyActiveMmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_M_SD.toString().trim(), postAcutePAModeratelyActiveMsd);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_M_MEAN.toString().trim(), postAcutePAVeryActiveMmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_M_SD.toString().trim(), postAcutePAVeryActiveMsd);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_INACTIVE_F_MEAN.toString().trim(), postAcutePAInactiveFmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_INACTIVE_F_SD.toString().trim(), postAcutePAInactiveFsd);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_F_MEAN.toString().trim(), postAcutePAModeratelyActiveFmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_F_SD.toString().trim(), postAcutePAModeratelyActiveFsd);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_F_MEAN.toString().trim(), postAcutePAVeryActiveFmean);
					arrayMap.put(EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_F_SD.toString().trim(), postAcutePAVeryActiveFsd);
										
					if(name.size() < PAContext.replications){
						psaError = true;
						//psaErrorMessage = psaErrorMessage + name.get(0)+"; ";
						psaErrorMessage = psaErrorMessage + fn+"; ";
					}
					
					replicationsData.put(DataType.EVENT.toString(), arrayMap);
				
					//PAContext.events = new ReplicationData(replicationsData);
					//PAContext.replicationsList.addRecord(name.get(0), PAContext.events);
					PAContext.replicationsData.addRecord(id, DataType.EVENT.toString(), name.get(0), new ReplicationData(replicationsData));
				
				}
				
			}
		}

		
		public static void readRiskCoef(DataFiles filename){
			
			if (filename == DataFiles.QRISK_COEF_DATA_FILE){
				String fn = filename.toString().trim();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HasMap
						String line = null;
						int replIdx = 0;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
							try {
								int dataIdx = 0;
								int varIdx = 0;
								int matrixIdx = 0;
								int riskIdx = 0;
								
								while (dataIdx<1){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<8){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<18){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<23){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<30){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<37){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<42){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<52){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<57){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<67){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								matrixIdx=0;
								++riskIdx;
								while (dataIdx<(67+1)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+8)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+18)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+23)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+30)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+37)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+42)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+52)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+57)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(67+67)){
									PAContext.qRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								///////////
								replIdx++;
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
					
					if(PAContext.qRiskCoef.get(0).get(0).get(0).size() < PAContext.replications){
						psaError = true;
						psaErrorMessage = psaErrorMessage + filename+"; ";
					}
					
			}
			
			else if (filename == DataFiles.DRISK_COEF_DATA_FILE){
				String fn = filename.toString().trim();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HasMap
						String line = null;
						int replIdx = 0;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
							try {
								int dataIdx = 0;
								int varIdx = 0;
								int matrixIdx = 0;
								int riskIdx = 0;
								
								while (dataIdx<1){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<6){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<16){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<21){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<26){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<30){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<33){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<36){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								
								varIdx=0;
								matrixIdx=0;
								++riskIdx;
								
								while (dataIdx<(36+1)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+6)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+16)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+21)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+26)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+30)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+33)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(36+36)){
									PAContext.dRiskCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								///////////
								replIdx++;
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
					if(PAContext.qRiskCoef.get(0).get(0).get(0).size() < PAContext.replications){
						psaError = true;
						psaErrorMessage = psaErrorMessage + filename+"; ";
					}
			}
			
			// QCOVID MATRIX
			else if (filename == DataFiles.QCovid_Coef_Data_File){
				String fn = filename.toString().trim();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HasMap
						String line = null;
						int replIdx = 0;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
							try {
								int dataIdx = 0;
								int varIdx = 0;
								int matrixIdx = 0;
								int riskIdx = 0;
								
								while (dataIdx<1){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<8){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<18){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<32){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<33){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								
								varIdx=0;
								matrixIdx=0;
								++riskIdx;
								
								while (dataIdx<(33+1)){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(33+8)){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(33+18)){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(33+32)){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								varIdx=0;
								++matrixIdx;
								while (dataIdx<(33+33)){
									PAContext.qCovidCoef.addToReplicationArray(riskIdx, matrixIdx, varIdx++, replIdx, data[++dataIdx]);
								}
								///////////
								replIdx++;
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
					if(PAContext.qCovidCoef.get(0).get(0).get(0).size() < PAContext.replications){
						psaError = true;
						psaErrorMessage = psaErrorMessage + filename+"; ";
					}
			}
			
			else{
				throw new IllegalArgumentException(
						"Error with the data files");
			}
			
		}
		
		
		
		public static void read2DArrayData(DataFiles filename, Util.My2DArray<Double> my2D){
			
		
				String fn = filename.toString().trim();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
						}
						// Read lines, parse data and add to HashMap
						String line = null;
						int replIdx = 0;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
						//	System.out.println(data.length);
							try {
								int dataIdx = 0;
								int ageIdx = 0;
							while (dataIdx<data.length-1){
								my2D.addToInnerArray(ageIdx++, replIdx, Double.valueOf(data[++dataIdx]));
															
							}
								///////////
								replIdx++;
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
					
					if(my2D.get(0).size() < PAContext.replications){
						psaError = true;
						psaErrorMessage = psaErrorMessage + filename+"; ";
					}
							
		}
		
		
		
		///Read mortality annual probability from life tables and then read the relative risks of conditions
		//the 3-months mortality probability for healthy or for any condition is calculated in the model
	
		public static void readMortality3monthsProbability(DataFiles filename){
			
		
				String fn = filename.toString().trim();
					BufferedReader br = null;
					try {
						br = new BufferedReader(new FileReader(fn));
						// Skip header
						for (int i = 0; i < DATA_FILES_HEADER_LINES; ++i) {
							br.readLine();
							
						}
						// Read lines, parse data and add to HashMap
						String line = null;
						int lineCount =0;
						while ((line = br.readLine()) != null) {
							// Split the line around commas
							final String[] data = line.split(",");
							// Check if current line seems all right
							/*if (data.length != 16) { // *** length of data 
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename.name(), line));
							}*/
						//	System.out.println(data.length);
							try {
								//System.out.println(lineCount);
							if(lineCount ==0){	
								int dataIdx = 0;
								while (dataIdx<data.length-1){
									PAContext.mortality3monthsMaleHealthyNonCVD[dataIdx] = Double.valueOf(data[++dataIdx]);
								}
							}
							else if(lineCount ==1){	
								int dataIdx = 0;
								while (dataIdx<data.length-1){
									PAContext.mortality3monthsMaleHealthyCVD[dataIdx] = Double.valueOf(data[++dataIdx]);
								}
							}
							else if(lineCount ==2){	
								int dataIdx = 0;
								while (dataIdx<data.length-1){
									PAContext.mortality3monthsFemaleHealthyNonCVD[dataIdx] = Double.valueOf(data[++dataIdx]);
								}
							}
							else if(lineCount ==3){	
								int dataIdx = 0;
								while (dataIdx<data.length-1){
									PAContext.mortality3monthsFemaleHealthyCVD[dataIdx] = Double.valueOf(data[++dataIdx]);
								}
							}
							
								///////////
							lineCount++;	
							} catch (final NumberFormatException e) {
								e.printStackTrace();
								throw new IllegalArgumentException(String.format(
										"File %s contains a malformed input line: %s",
										filename, line), e);
							}
						}
					} catch (final FileNotFoundException e) {
						e.printStackTrace();
					} catch (final IOException e) {
						e.printStackTrace();
					} finally {
						if (br != null) {
							try {
								br.close();
							} catch (final IOException e) {
								e.printStackTrace();
							}
						}
					}
		
		}
		
		
		
	/////END READ
	
		// range selection variables (eg age, bmi, pa are in Data class) 
		public static List selectPopulation(Data d){
			Map properties = new HashMap();
			
			if((PAContext.p.getValueAsString("gender").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.GENDER.toString().trim(), "M");
			}
			else if((PAContext.p.getValueAsString("gender").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.GENDER.toString().trim(), "F");
			}
	
			if((PAContext.p.getValueAsString("cvd").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.HAD_CVD.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("cvd").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.HAD_CVD.toString().trim(), "1");
			}
						
			if((PAContext.p.getValueAsString("t2d").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.HAD_T2D.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("t2d").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.HAD_T2D.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("t1d").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.HAD_T1D.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("t1d").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.HAD_T1D.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("depression").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.HAD_DEPRESSION.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("depression").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.HAD_DEPRESSION.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("msi").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.HAD_MSI.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("msi").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.HAD_MSI.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("bpTreatment").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.BP_TREATMENT.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("bpTreatment").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.BP_TREATMENT.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("kidneyDisease").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.CHRONIC_KIDNEY_DISEASE.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("kidneyDisease").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.CHRONIC_KIDNEY_DISEASE.toString().trim(), "1");
			}
			
			if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("1"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "0");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("2"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "1");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("3"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "2");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("4"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "3.1");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("5"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "3.2");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("6"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "4");
			}
			else if((PAContext.p.getValueAsString("sec").trim().equalsIgnoreCase("7"))){
				properties.put(PeopleProperties.SEC.toString().trim(), "5");
			}
			
			
			RecordProperties selectedProp = new RecordProperties(properties);
			

			List matchingRecords = d.search(selectedProp);
			/*
			System.out.print("matchingRecords.size() ");
			System.out.println(matchingRecords.size());
			*/
			PAContext.totalPopulation = matchingRecords.size();
			if(matchingRecords.size()==0){
		//		JOptionPane.showMessageDialog(null, "There is no record that matches your criteria. The simulation will terminate.");
				PAContext.enoughCohort = false;
				RSApplication.getRSApplicationInstance().getGui().getFrame().dispose();
				//RunEnvironment.getInstance().endRun();
			}
		//	else{JOptionPane.showMessageDialog(null, "Numbers of records matching your criteria: "+matchingRecords.size());}
			
			return matchingRecords;
			
		}
		
		
		public static void initialiseAgentsFromData(Agent a, Record record){
			a.setID(Integer.parseInt(record.getProp().getProperty(PeopleProperties.ID.toString()).toString()));
			a.setAge(Double.parseDouble(record.getProp().getProperty(PeopleProperties.AGE.toString()).toString()));
			a.setGender(record.getProp().getProperty(PeopleProperties.GENDER.toString()).toString());
			a.setPAStatus(Double.parseDouble(record.getProp().getProperty(PeopleProperties.PA_STATUS.toString()).toString()));
			a.setHadCVD(Integer.parseInt(record.getProp().getProperty(PeopleProperties.HAD_CVD.toString()).toString()));
			a.setHadT2D(Integer.parseInt(record.getProp().getProperty(PeopleProperties.HAD_T2D.toString()).toString()));
			a.setBMI(Double.parseDouble(record.getProp().getProperty(PeopleProperties.BMI.toString()).toString()));
			a.setHDLRatio(Double.parseDouble(record.getProp().getProperty(PeopleProperties.HDL_RATIO.toString()).toString()));
			a.setSBloodPressure(Double.parseDouble(record.getProp().getProperty(PeopleProperties.S_BLOOD_PRESSURE.toString()).toString()));
			a.setDBloodPressure(Double.parseDouble(record.getProp().getProperty(PeopleProperties.D_BLOOD_PRESSURE.toString()).toString()));
			a.setSmoking(Integer.parseInt(record.getProp().getProperty(PeopleProperties.SMOKING.toString()).toString()));
			a.setEthnicity(record.getProp().getProperty(PeopleProperties.ETHNICITY.toString()).toString());
			a.setFamilyCVDHistory(Integer.parseInt(record.getProp().getProperty(PeopleProperties.FAMILY_CVD_HISTORY.toString()).toString()));
			a.setDeprivationTown(Double.parseDouble(record.getProp().getProperty(PeopleProperties.DEPRIVATION_TOWN.toString()).toString()));
			a.setBpTreatment(Integer.parseInt(record.getProp().getProperty(PeopleProperties.BP_TREATMENT.toString()).toString()));
			a.setRheumatoidArthritis(Integer.parseInt(record.getProp().getProperty(PeopleProperties.RHEUMATOID_ARTHRITIS.toString()).toString()));
			a.setCHronicKidneyDisease(Integer.parseInt(record.getProp().getProperty(PeopleProperties.CHRONIC_KIDNEY_DISEASE.toString()).toString()));
			a.setAtrialFibrillation(Integer.parseInt(record.getProp().getProperty(PeopleProperties.ATRIAL_FIBRILLATION.toString()).toString()));
			a.setHadT1D(Integer.parseInt(record.getProp().getProperty(PeopleProperties.HAD_T1D.toString()).toString()));
			a.setCort(Integer.parseInt(record.getProp().getProperty(PeopleProperties.CORT.toString()).toString()));
			a.setFamilyT2DHistory(Integer.parseInt(record.getProp().getProperty(PeopleProperties.FAMILY_T2D_HISTORY.toString()).toString()));
			a.setEducation(Integer.parseInt(record.getProp().getProperty(PeopleProperties.EDUCATION.toString()).toString()));
			a.setSampleWeight(Double.parseDouble(record.getProp().getProperty(PeopleProperties.SAMPLE_WEIGHT.toString()).toString()));
			a.setUtilityIMPEQ5D(Double.parseDouble(record.getProp().getProperty(PeopleProperties.UTILITY_IMPEQ5D.toString()).toString()));
			a.setDeprivationIMD(Double.parseDouble(record.getProp().getProperty(PeopleProperties.DEPRIVATION_IMD.toString()).toString()));
			a.setHadDepression(Integer.parseInt(record.getProp().getProperty(PeopleProperties.HAD_DEPRESSION.toString()).toString()));
			a.setHadMSI(Integer.parseInt(record.getProp().getProperty(PeopleProperties.HAD_MSI.toString()).toString()));
			a.setSocioEconomicClass(Double.parseDouble(record.getProp().getProperty(PeopleProperties.SEC.toString()).toString()));
			a.setLongLastingIllness(Integer.parseInt(record.getProp().getProperty(PeopleProperties.LLI.toString()).toString()));
			a.setComorbidity(Integer.parseInt(record.getProp().getProperty(PeopleProperties.COMORBIDITY.toString()).toString()));
			
			a.setAgeAtEntry(a.getAge());
			a.setPAStatusAtEntry(a.getPAStatus());
			a.setBMIAtEntry(a.getBMI());
			a.setHDLRatioAtEntry(a.getHDLRatio());
			a.setSBPAtEntry(a.getSBloodPressure());
			a.setPAPreviousPeriod(a.getPAStatus());
			
					
			////TODO 
			for (Iterator i = PAContext.replicationsData.getDataList().iterator();i.hasNext();){ 
				Record rec = (Record) i.next();
				if(rec.getType().equalsIgnoreCase(DataType.EVENT.toString())){
	
				}
				
			}
			////TODO 
			
			
		}
		
		public static void resetAgent(Agent a){
			
			a.setIsDead(false);
			a.setCauseOfDeath(null);	
			a.setInAcutePeriod(false);
				
			a.setLifeYears(0.0);
			a.setYearsActive(0.0);
			a.setTotalEventCost(0.0);
			a.setTotalEventCostDiscounted(0.0);
			
			a.setTotalNoOfEvents(0);
			a.setTotalNoOfCVD(0);
			a.setNoOfMSI(0);
			a.setNoOfDprsEpisodes(0);
			a.setNoOfStroke(0);
			a.setNoOfCHD(0);
			a.setNoOfT2D(0);
					
						
			a.clearCHDEventsList();
			a.clearStrokeEventsList();
			a.clearT2DEventsList();
			a.clearMSIEventsList();
			a.clearDepressionEventsList();
			
            a.setInfected(0);
    		
            if (PAContext.initialSusceptible < Math.random()) {
    			a.setSusceptible(0);
    		}
    		else 
    			{a.setSusceptible(1); }
    		
    		/// set initial number of exposed agents
    		if (PAContext.initialExposed < Math.random()) {
    			a.setExposed(0);
    		}
    		else 
    			{a.setExposed(1);}
    		
            a.setRecovered(0);
            a.setLongTermEffects(0);
            a.setTimesLongCovid(0);
            a.setDaysSinceExposure(0);
            a.setDaysSinceInfected(0);
            a.setTimeSinceLastInfection(0);
            a.setCRisk(0);
            a.setSeverity(0);
            a.setHospitalised(0);
            a.setTimesHospitalised(0);
            a.setMyHospitalCost(0.0);
            a.setICU(0);
            a.setTimesICU(0);
            a.setMyICUCost(0.0);
            a.setMyLockdownCost(0.0);
            a.setTimesInfected(0);
            a.setVaccinated(0);
            a.setVaccineRiskAdjust(0.0);
            a.setVaccineUptakeDecision(0);
            a.setMyVaccineCost(0.0);
            a.setTimesVaccinated(0);
            a.setDaysOfLongTermEffects(0);
            a.setMyLongCovidCost(0.0); 
            a.setTicks(1);
            a.setNContactsTickCounter(0);
            a.setPeriodicLockdownTimestep(0);
            a.setThreeMonthTimeStepAdjust(0);
            a.setTotalHospitalCost(0);
            a.setTotalICUCost(0);
            a.setTotalLongCovidCost(0);
            a.setTotalVaccineCost(0);
            a.setTotalLockdownCost(0);
            a.setDiedOfCovid(0);
            a.setCountDead(0);
            a.setCovidDeathDiabetes(0);
            a.setCovidDeathCVD(0);
            a.setCovidDeathHypertension(0);
            a.setCovidDeathMale(0);
            a.setCovidDeathObese(0);
            a.seticuMorbidlyobese(0);
            a.setIcuObese(0);
            a.setIcuOverweight(0);
            a.setIcuNormalWeight(0);
            a.setBeginLockdown(0);
            a.setClinicallyVulnerable(0);
            //
		}
		
		
		public static void individualResultsDialogue(){
			// // turn off individual output option for batch runs
/*			Object[] options = {"Yes", "No"};
			
			int dialogue = JOptionPane.showOptionDialog(null, "Do you want to write results for each individual?", "Individual results", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

			if (dialogue == JOptionPane.YES_OPTION){
				PAContext.individualRecordOn = true; 
			} */
			
		}  
		
		public static void createOutFiles() throws IOException{
			dateAppend = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			
			currentOutDirectory = new File("./output");///"+dateAppend); in order for the outputs to be generated in batch mode I had to remove the date (this is a function of the update)
			currentOutDirectory.mkdir();
			
			/////////////INDIVIDUAL TRACK
			String outfile = currentOutDirectory.getPath() + OutputFiles.INDIVIDUAL.toString().trim()+"TRACK.csv";
			File f2 = new File(outfile);
			if(!f2.exists()){
				try{
					f2.createNewFile();
					FileWriter fw = new FileWriter(outfile, true);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write("Simulation Time"+","+"ID in model"+","+"ID in DB"+","+"Age"+","+"PA Status"+","+"PA previous period"+","+"Is in acute period?"+","+"Event name Stroke"
					+","+"Number of Stroke Events"+","+"Stroke start time"+","+"Age at stroke start"+","+"PA at stroke start"+","+"Stroke acute duration (months)"
					+","+"Stroke acute PA status"+","+"Event name CHD"+","+"Number of CHD Events"+","+"CHD start time"+","+"Age at CHD start"+","+"PA at CHD start"
					+","+"CHD acute duration (months)"+","+"CHD acute PA status"+","+"Event name Depression"+","+"Number of Depression Events"+","+"Depression start time"
					+","+"Age at Depression start"+","+"PA at depression start"+","+"depression acute duration (months)"+","+"depression acute PA status"
					+","+"Event name MSI"+","+"Number of MSI Events"+","+"MSi start time"+","+"Age at MSI start"+","+"PA at MSI start"+","+"MSI acute duration (months)"
					+","+"MSI acute PA status"

					+","+"Susceptible"+","+"Exposed"+","+"Infected"+","+"Recovered"+","+"Infection prob"
					+","+"Times infected"+","+"Severity"+","+"Hospitalised"+","+"ICU"+","+"Hospital cost"+","+"ICU cost"
					+","+"Risk probability"+","+"LoSHosp"+","+"LosICU\r\n"
					//
					);
					
					bw.close();
									
				}catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			/////////////////INDIVIDUAL TRACK
			
			for (OutputFiles o : OutputFiles.values() ){
				String out = currentOutDirectory.getPath().trim() + o.toString().trim()+".csv"; //+dateAppend+".csv";
				
				if(!PAContext.individualRecordOn && out.contains("Individual")){
				}
				else{
					//if(!out.contains("Annual")){File f1 = new File(out);
					
					File f1 = new File(out);
					if(!f1.exists()) {
					    try {
							f1.createNewFile();
							if(f1.getName().contains("Individual")){
							 	if(f1.getName().contains("Annual")){

									}
								else{
									FileWriter fw = new FileWriter(out, true);
									BufferedWriter bw = new BufferedWriter(fw);
									bw.write("PSA"+","+"ID in model"+","+"ID in DB"+","+"Age at entry"+","+"Age at sim end"+","+"Age at death"+","+"Cause of death"+","+"Life years"
									+","+"PA status at entry"+","+"PA status at sim end"+","+"PA status at death"+","+"BMI at entry"+","+"BMI at end"
									+","+"HDL Ratio at entry"+","+"HDL Ratio at end"+","+"SBP at entry"+","+"SBP at end"
									+","+"Years active"+","+"Total No of events"+","+"Total No of CVD events"+","+"Total No of MSI events"
									+","+"Total No of Depression episodes"+","+"No of Strokes"+","+"No of CHD"+","+"Has T2D"+","+"Had T2D"+","+"Had T1D"+","+"Had CVD"
									+","+"Had MSI"+","+"Had Depression"+","+"SocioEconomicClass"+","+"Total event cost"+","+"Total event cost discounted"
									
									+","+"Times infected"+","+"Times hospitalised"+","+"Total hospital cost"+","+"Times ICU"+","+"Total ICU cost"
									+","+"Total long covid"+","+"Total long covid cost"+","+"Died of Covid"+","+
									"Times vaccinated\r\n");
									bw.close();
									//
									}
							 	 
								}
							// annualPSA file (COHORT)
							else if(f1.getName().contains("PSA")){
							 	if(f1.getName().contains("Annual")){
									FileWriter fw = new FileWriter(out, true);
									BufferedWriter bw = new BufferedWriter(fw);
									bw.write("SimYear"+","+"PSA"+","+"Cohort size"
									+","+"Total life years"+","+"Avg life years"+","+"SD life years"
									+","+"Total PA status"+","+"Avg PA status"+","+"SD PA status"
									+","+"Total BMI"+","+"Avg BMI status"+","+"SD BMI"
									+","+"Total HDL ratio"+","+"Avg HDL ratio"+","+"SD HDL ratio"
									+","+"Total sbp"+","+"Avg sbp"+","+"SD sbp"
									+","+"Total events"+","+"Avg events"+","+"SD events"
									+","+"Total CVD events"+","+"Avg CVD events"+","+"SD CVD events"
									+","+"Total MSI events"+","+"Avg MSI events"+","+"SD MSI events"
									+","+"Total Depression episodes"+","+"Avg Depression episodes"+","+"SD Depression episodes"
									+","+"Total strokes"+","+"Avg strokes"+","+"SD strokes"+","+"Total CHD"+","+"Avg CHD"+","+"SD CHD"
									+","+"Total T2D"+","+"Avg T2D"+","+"SD T2D"
									+","+"Total years active"+","+"Avg years active"+","+"SD years active"
									+","+"Total event cost"+","+"Avg event cost"+","+"SD event cost"

									+","+"Total times infected"+","+"Avg times infected"+","+"SD times infected"
									+","+"Total times hospitalised"+","+"Avg times hospitalised"+","+"SD times hospitalised"
									+","+"Total times ICU"+","+"Avg times ICU"+","+"SD times ICU"
									+","+"Total died of covid"+","+"Avg died of covid"+","+"SD died of covid"
									+","+"Total times long covid"+","+"Avg long covid"+","+"SD long covid"
									+","+"Total hospital cost"+","+"Avg hospital cost"+","+"SD hospital cost"
									+","+"Total ICU cost"+","+"Avg ICU cost"+","+"SD ICU cost"
									+","+"Total long covid cost"+","+"Avg long covid cost"+","+"SD long covid cost"
									+","+"Total lockdown cost"+","+"Avg lockdown cost"+","+"SD lockdown cost"
									+","+"Total vaccination cost"+","+"Avg vaccination cost"+","+"SD vaccination cost"
									+","+"Total healthcare cost (covid)"+","+"Avg healthcare cost (covid)"+","+"SD healthcare cost (covid)"
									+","+"Total intervention cost (covid)"+","+"Avg intervention cost (covid)"+","+"SD intervention cost (covid)"
									+","+"Total covid cost"+","+"Avg covid cost"+","+"SD covid cost"
									+","+"Total times vaccinated"+","+"Avg vaccinated"+","+"SD vaccinated\r\n");
									
									bw.close();
									}
							 	//out cohort PSA
								else{
									FileWriter fw = new FileWriter(out, true);
									BufferedWriter bw = new BufferedWriter(fw);
									bw.write("PSA"+","+"Cohort size"+","+"Avg life years"+","+"SD life years"+","+"Avg events"+","+"SD events"+","+"Avg CVD events"
									+","+"SD CVD events"+","+"Avg MSI events"+","+"SD MSI events"+","+"Avg Depression episodes"+","+"SD Depression episodes"
									+","+"Avg strokes"+","+"SD strokes"+","+"Avg CHD"+","+"SD CHD"+","+"Avg T2D"+","+"SD T2D"+","+"Avg years active"
									+","+"SD years active"+","+"Avg event cost"
									+","+"SD event cost"+","+"Avg event cost discounted"+","+"SD event cost discounted"

									+","+"Avg times infected"+","+"SD times infected"
									+","+"Avg times hospitalised"+","+"SD times hospitalised"
									+","+"Avg times ICU"+","+"SD times ICU"
									+","+"Avg died of covid"+","+"SD died of covid"
									+","+"Avg times long covid"+","+"SD times long covid"
									+","+"Avg total hospital cost"+","+"SD total hospital cost"
									+","+"Avg total ICU cost"+","+"SD total ICU cost"
									+","+"Avg long covid cost"+","+"SD long covid cost"
									+","+"Avg total lockdown cost"+","+"SD total lockdown cost"
									+","+"Avg total vaccination cost"+","+"SD total vaccination cost"
									+","+"Avg total health cost (covid)"+","+"SD total health cost (covid)"
									+","+"Avg total intervention cost (covid)"+","+"SD total intervention cost (covid)"
									+","+"Avg total cost (covid)"+","+"SD total cost (covid)"
									+","+"Avg times vaccinated"+","+"SD times vaccinated\r\n");
								    //

									bw.close();
									}
									
								}
							else if(f1.getName().contains("Strategy")){
								if(f1.getName().contains("Annual")){
									}
								//OUT COHORT STRATEGY// 
								else{
									FileWriter fw = new FileWriter(out, true);
									BufferedWriter bw = new BufferedWriter(fw);
									bw.write("No of PSA"+","+"Cohort size"+","+"Avg life years"+","+"SD life years"+","+"Avg events"+","+"SD events"+","+"Avg CVD events"
											+","+"SD CVD events"+","+"Avg MSI events"+","+"SD MSI events"+","+"Avg Depression episodes"+","+"SD Depression episodes"
											+","+"Avg strokes"+","+"SD strokes"+","+"Avg CHD"+","+"SD CHD"
											+","+"Avg T2D"+","+"SD T2D"+","+"Avg years active"+","+"SD years active"+","+
											"Avg event cost"+","+"SD event cost"+","+"Avg event cost discounted"+","+"SD event cost discounted"
											
											+","+"Avg times infected"+","+"SD times infected"
											+","+"Avg times hospitalised"+","+"SD times hospitalised"
											+","+"Avg times ICU"+","+"SD times ICU"
											+","+"Avg died of covid"+","+"SD died of covid"
											+","+"Avg times long covid"+","+"SD times long covid"
											+","+"Avg total hospital cost"+","+"SD total hospital cost"
											+","+"Avg total ICU cost"+","+"SD total ICU cost"
											+","+"Avg long covid cost"+","+"SD long covid cost"
											+","+"Avg total lockdown cost"+","+"SD total lockdown cost"
											+","+"Avg total vaccination cost"+","+"SD total vaccination cost"
											+","+"Avg total health cost (covid)"+","+"SD total health cost (covid)"
											+","+"Avg total intervention cost (covid)"+","+"SD total intervention cost (covid)"
											+","+"Avg total cost (covid)"+","+"SD total cost (covid)"
											+","+"Avg times vaccinated"+","+"SD times vaccinated\r\n");
											
									bw.close();
									}
									
								}
						} catch (IOException e) {
							// Auto-generated catch block
							e.printStackTrace();
						}
					}
				//}	
				}
			}
		} 
		
		public static void writeOutIndividual(Agent a){
			try{
			
				String outfile = currentOutDirectory.getPath() + OutputFiles.INDIVIDUAL.toString().trim()+".csv";//+dateAppend+".csv";
			//	String outfile = OutputFiles.INDIVIDUAL.toString().trim();
				FileWriter fw = new FileWriter(outfile, true);
				  BufferedWriter out = new BufferedWriter(fw);
				  out.write(PAContext.repCounter+","+a.getInModelID()+","+a.getID()+","
						  +formatNumber(a.getAgeAtEntry())+","
						  +formatNumber(a.getAgeAtSimEnd())+","
						  +formatNumber(a.getAgeAtDeath())+","
						  +a.getCauseOfDeath()+","
						  +formatNumber(a.getLifeYears())+","
						  +formatNumber(a.getPAStatusAtEntry())+","
						  +formatNumber(a.getPAStatusAtSimEnd())+","
						  +formatNumber(a.getPAStatusAtDeath())+","
						  +formatNumber(a.getBMIAtEntry())+","
						  +formatNumber(a.getBMIAtEnd())+","
						  +formatNumber(a.getHDLRatioAtEntry())+","
						  +formatNumber(a.getHDLRatioAtEnd())+","
						  +formatNumber(a.getSBPAtEntry())+","
						  +formatNumber(a.getSBPAtEnd())+","
						  +formatNumber(a.getYearsActive())+","
						  +a.getTotalNoOfEvents()+","
						  +a.getTotalNoOfCVD()+","
						  +a.getNoOfMSI()+","
						  +a.getNoOfDprsEpisodes()+","
						  +a.getNoOfStroke()+","
						  +a.getNoOfCHD()+","
						  +a.getNoOfT2D()+","
						  +a.getHadT2D()+","
						  +a.getHadT1D()+","
						  +a.getHadCVD()+","
						  +a.getHadMSI()+","
						  +a.getHadDepression()+","
						  +a.getSocioEconomicClass()+","
						  +formatNumber(a.getTotalEventCost())+","
						  +formatNumber(a.getTotalEventCostDiscounted())+","

						  +a.getTimesInfected()+","
						  +a.getTimesHospitalised()+","
						  +formatNumber(a.getTotalHospitalCost())+","
						  +a.getTimesICU()+","
					      +formatNumber(a.getTotalICUCost())+","
					      +a.getTimesLongCovid()+","
					      +formatNumber(a.getTotalLongCovidCost())+","
					      +a.getDiedOfCovid()+","
					      +a.getCovidDeathCVD()+","
					      +a.getCovidDeathDiabetes()+","
					      +a.getCovidDeathMale()+","
					      +a.getTimesVaccinated()+"\r\n");
					     //
				  
				  //Close the output stream
				  out.close();
				
			  }catch (Exception e){//Catch exception if any
				  System.err.println("Error: " + e.getMessage());
				  }
		}
		
		// write out stats for PSA and Strategy (both have the same format)
		//statsList
		
		public static void writeOut(ArrayList<Stats> statsList, String filename){
			try{
				
				FileWriter fw = new FileWriter(filename, true);
				  BufferedWriter out = new BufferedWriter(fw);
				  	  
				  out.write(PAContext.repCounter+","
						  +PAContext.cohortSize+",");
				  int idx = 0;  
					while (idx < statsList.size()){	
						out.write(
						formatNumber(statsList.get(idx).getAverage())+"," 
						  +formatNumber(statsList.get(idx++).getStDev())+",");
					}
					
					out.write("\r\n");
				  					
				  //Close the output stream
				  out.close();
				
			  }catch (Exception e){//Catch exception if any
				  System.err.println("Error: " + e.getMessage());
				  }
		}
		
		//write out annual PSA outputs
		public static void writeOutAnnual(ArrayList<AnnualPSAOutput> annualPSAOutput, String filename){
			try{
				
				FileWriter fw = new FileWriter(filename, true);
				  BufferedWriter out = new BufferedWriter(fw);
				  
				  int idx = 0;  
					while (idx < annualPSAOutput.size()){	
						out.write(
						formatNumber(annualPSAOutput.get(idx).getYear())+","+
						formatNumber(annualPSAOutput.get(idx).getPSA())+","+
						formatNumber(annualPSAOutput.get(idx).getCohortSize())+","+
						formatNumber(annualPSAOutput.get(idx).getLifeYears().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getLifeYears().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getLifeYears().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getPAStatus().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getPAStatus().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getPAStatus().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getBMI().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getBMI().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getBMI().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getHDL().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getHDL().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getHDL().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getSBP().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getSBP().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getSBP().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getCVDEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getCVDEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getCVDEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getMSIEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getMSIEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getMSIEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getDepressionEpisodes().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getDepressionEpisodes().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getDepressionEpisodes().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getStrokeEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getStrokeEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getStrokeEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getCHDEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getCHDEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getCHDEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getT2DEvents().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getT2DEvents().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getT2DEvents().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getYearsActive().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getYearsActive().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getYearsActive().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getEventCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getEventCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getEventCost().getStDev())+","+

						formatNumber(annualPSAOutput.get(idx).getTimesInfected().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesInfected().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesInfected().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesHospitalised().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesHospitalised().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesHospitalised().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesICU().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesICU().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesICU().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getDiedOfCovid().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getDiedOfCovid().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getDiedOfCovid().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesLongCovid().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesLongCovid().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesLongCovid().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHospitalCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHospitalCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHospitalCost().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalICUCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalICUCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalICUCost().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLongCovidCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLongCovidCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLongCovidCost().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLockdownCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLockdownCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalLockdownCost().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalVaccinationCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalVaccinationCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalVaccinationCost().getStDev())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHealthcareCostCovid().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHealthcareCostCovid().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalHealthcareCostCovid().getStDev())+","+	
						formatNumber(annualPSAOutput.get(idx).getTotalInterventionCostCovid().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalInterventionCostCovid().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalInterventionCostCovid().getStDev())+","+	
						formatNumber(annualPSAOutput.get(idx).getTotalCovidCost().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalCovidCost().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTotalCovidCost().getStDev())+","+						
						formatNumber(annualPSAOutput.get(idx).getTimesVaccinated().getTotal())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesVaccinated().getAverage())+","+
						formatNumber(annualPSAOutput.get(idx).getTimesVaccinated().getStDev())+"\r\n");
						//
						idx++;
					}
				  //Close the output stream
				  out.close();
				
			  }catch (Exception e){//Catch exception if any
				  System.err.println("Error: " + e.getMessage());
			  }
			annualPSAOutput.clear();
		}
		
		// For multidimensional (2D) arrays in keeping statistics
		public static class My2DArray<T> extends ArrayList<ArrayList<T>>{
				
			
			
			////Add to arrays
			public void addToOuterArray(int index){
				while (index >=this.size()){
					this.add(new ArrayList<T>());
				}
			}
						
			public void addToInnerArray(int index, int index1, T element){
				while (index >=this.size()){
					this.add(new ArrayList<T>());
				}
				ArrayList<T> inner1 = this.get(index);
				while (index1 >= inner1.size()) {
					inner1.add(null);
				}
				inner1.set(index1, element); 
			}
						
		}
		
		
		// For multidimensional (4D) arrays in risk coefficients
		public static class My4DArray<T> extends ArrayList<ArrayList<ArrayList<ArrayList<T>>>>{
		
			
			
			////Add to arrays
			public void addToRiskArray(int index){
				while (index >=this.size()){
					this.add(new ArrayList<ArrayList<ArrayList<T>>>());
				}
			}
			
						
			public void addToMatrixArray(int index, int index1){
				while (index >=this.size()){
					this.add(new ArrayList<ArrayList<ArrayList<T>>>());
				}
				ArrayList<ArrayList<ArrayList<T>>> inner1 = this.get(index);
			    while (index1 >= inner1.size()) {
			        inner1.add(new ArrayList<ArrayList<T>>());
			    }
			}
			
			public void addToVarArray(int index, int index1, int index2){
				while (index >=this.size()){
					this.add(new ArrayList<ArrayList<ArrayList<T>>>());
				}
				ArrayList<ArrayList<ArrayList<T>>> inner1 = this.get(index);
			    while (index1 >= inner1.size()) {
			        inner1.add(new ArrayList<ArrayList<T>>());
			    }
			    ArrayList<ArrayList<T>> inner2 = inner1.get(index1);
			    while (index2 >= inner2.size()) {
			        inner2.add(new ArrayList<T>());
			    }
			}
			
			public void addToReplicationArray(int index, int index1, int index2, int index3, T element){
				while (index >=this.size()){
					this.add(new ArrayList<ArrayList<ArrayList<T>>>());
				}
				ArrayList<ArrayList<ArrayList<T>>> inner1 = this.get(index);
			    while (index1 >= inner1.size()) {
			        inner1.add(new ArrayList<ArrayList<T>>());
			    }
			    ArrayList<ArrayList<T>> inner2 = inner1.get(index1);
			    while (index2 >= inner2.size()) {
			        inner2.add(new ArrayList<T>());
			    }
			    
			    ArrayList<T> inner3 = inner2.get(index2);
			    while(index3 >= inner3.size()){
			    	inner3.add(null);
			   }
			   inner3.set(index3, element); 
			}
			
			
			
			////////get elements for QRISK CVD
			public double getQSURV_M(int variable, int repReadData){
				//variable=0 only one value (baseline) in QSURV matrix
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=0; // 0 for array index for QSURV matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQSURV_F(int variable, int repReadData){
				//variable=0 only one value (baseline) in QSURV matrix
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=0; // 0 for array index for QSURV matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQXBAR_M(int variable, int repReadData){
				//variables: seven variables in QXBAR matrix
				/*indices
				 * 0: age1
				 * 1: age2
				 * 2: bmi1
				 * 3: bmi2
				 * 4: rati
				 * 5: sbp
				 * 6: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=1; // 1 for array index for QXBAR matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQXBAR_F(int variable, int repReadData){
				//variables: seven variables in QXBAR matrix
				/*indices
				 * 0: age1
				 * 1: age2
				 * 2: bmi1
				 * 3: bmi2
				 * 4: rati
				 * 5: sbp
				 * 6: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=1; // 1 for array index for QXBAR matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQBETAeth_M(int variable, int repReadData){
				//variables: ten variables in QBETAeth matrix
				/*indices
				 * 0: White
				 * 1: Not Recorded
				 * 2: Indian
				 * 3: Pakistani
				 * 4: Bangladeshi
				 * 5: Other Asian
				 * 6: Black Carribean
				 * 7: Black African
				 * 8: Chinese
				 * 9: Other
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=2; // 2 for array index for QBETAeth matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQBETAeth_F(int variable, int repReadData){
				//variables: ten variables in QBETAeth matrix
				/*indices
				 * 0: White
				 * 1: Not Recorded
				 * 2: Indian
				 * 3: Pakistani
				 * 4: Bangladeshi
				 * 5: Other Asian
				 * 6: Black Carribean
				 * 7: Black African
				 * 8: Chinese
				 * 9: Other
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=2; // 2 for array index for QBETAeth matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQBETAsmo_M(int variable, int repReadData){
				//variables: five variables in QBETAsmo matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=3; // 3 for array index for QBETAsmo matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQBETAsmo_F(int variable, int repReadData){
				//variables: five variables in QBETAsmo matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=3; // 3 for array index for QBETAsmo matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQBETAcv_M(int variable, int repReadData){
				//variables: seven variables in QBETAcv matrix
				/*indices
				 * 0: age1
				 * 1: age2
				 * 2: bmi1
				 * 3: bmi2
				 * 4: rati
				 * 5: sbp
				 * 6: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=4; // 4 for array index for QBETAcv matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQBETAcv_F(int variable, int repReadData){
				//variables: seven variables in QBETAcv matrix
				/*indices
				 * 0: age1
				 * 1: age2
				 * 2: bmi1
				 * 3: bmi2
				 * 4: rati
				 * 5: sbp
				 * 6: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=4; // 4 for array index for QBETAcv matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQBETAbv_M(int variable, int repReadData){
				//variables: seven variables in QBETAbv matrix
				/*indices
				 * 0: af
				 * 1: ra
				 * 2: ckd
				 * 3: aht
				 * 4: dm1
				 * 5: dm2
				 * 6: FHcvd
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=5; // 5 for array index for QBETAbv matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQBETAbv_F(int variable, int repReadData){
				//variables: seven variables in QBETAbv matrix
				/*indices
				 * 0: af
				 * 1: ra
				 * 2: ckd
				 * 3: aht
				 * 4: dm1
				 * 5: dm2
				 * 6: FHcvd
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=5; // 5 for array index for QBETAbv matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQAGE1SMO_M(int variable, int repReadData){
				//variables: five variables in QAGE1SMO matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=6; // 6 for array index for QAGE1SMO matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQAGE1SMO_F(int variable, int repReadData){
				//variables: five variables in QAGE1SMO matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=6; // 6 for array index for QAGE1SMO matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQAGE1INT_M(int variable, int repReadData){
				//variables: ten variables in QAGE1INT matrix
				/*indices
				 * 0: af
				 * 1: ckd
				 * 2: aht
				 * 3: dm1
				 * 4: dm2
				 * 5: bmi1
				 * 6: bmi2
				 * 7: FHcvd
				 * 8: sbp
				 * 9: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=7; // 7 for array index for QAGE1INT matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQAGE1INT_F(int variable, int repReadData){
				//variables: ten variables in QAGE1INT matrix
				/*indices
				 * 0: af
				 * 1: ckd
				 * 2: aht
				 * 3: dm1
				 * 4: dm2
				 * 5: bmi1
				 * 6: bmi2
				 * 7: FHcvd
				 * 8: sbp
				 * 9: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=7; // 7 for array index for QAGE1INT matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQAGE2SMO_M(int variable, int repReadData){
				//variables: five variables in QAGE2SMO matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=8; // 8 for array index for QAGE2SMO matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQAGE2SMO_F(int variable, int repReadData){
				//variables: five variables in QAGE2SMO matrix
				/*indices
				 * 0: non smoker
				 * 1: ex smoker
				 * 2: light smoker
				 * 3: moderate smoker
				 * 4: heavy smoker
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=8; // 8 for array index for QAGE2SMO matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			
			public double getQAGE2INT_M(int variable, int repReadData){
				//variables: ten variables in QAGE2INT matrix
				/*indices
				 * 0: af
				 * 1: ckd
				 * 2: aht
				 * 3: dm1
				 * 4: dm2
				 * 5: bmi1
				 * 6: bmi2
				 * 7: FHcvd
				 * 8: sbp
				 * 9: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=0; // 0 for array index for Male risk
				int matrix=9; // 9 for array index for QAGE9INT matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
			public double getQAGE2INT_F(int variable, int repReadData){
				//variables: ten variables in QAGE2INT matrix
				/*indices
				 * 0: af
				 * 1: ckd
				 * 2: aht
				 * 3: dm1
				 * 4: dm2
				 * 5: bmi1
				 * 6: bmi2
				 * 7: FHcvd
				 * 8: sbp
				 * 9: town
				*/
				//repReadData = replication -1 --> set in Agent class for all PSA readings
				int risk=1; // 1 for array index for Female risk
				int matrix=9; // 9 for array index for QAGE2INT matrix
				
				double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
				
				return myValue;
			}
		
			////////get elements for QRISK DIABETES
				public double getDSURV_M(int variable, int repReadData){
					//variable=0 only one value (baseline) in DSURV matrix
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=0; // 0 for array index for DSURV matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDSURV_F(int variable, int repReadData){
					//variable=0 only one value (baseline) in DSURV matrix
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=0; // 0 for array index for DSURV matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDXBAR_M(int variable, int repReadData){
					//variables: five variables in DXBAR matrix
					/*indices
					 * 0: age1
					 * 1: age2
					 * 2: bmi1
					 * 3: bmi2
					 * 4: town
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=1; // 1 for array index for DXBAR matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDXBAR_F(int variable, int repReadData){
					//variables: five variables in DXBAR matrix
					/*indices
					 * 0: age1
					 * 1: age2
					 * 2: bmi1
					 * 3: bmi2
					 * 4: town
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=1; // 1 for array index for DXBAR matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDBETAeth_M(int variable, int repReadData){
					//variables: ten variables in DBETAeth matrix
					/*indices
					 * 0: White
					 * 1: Not Recorded
					 * 2: Indian
					 * 3: Pakistani
					 * 4: Bangladeshi
					 * 5: Other Asian
					 * 6: Black Carribean
					 * 7: Black African
					 * 8: Chinese
					 * 9: Other
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=2; // 2 for array index for DBETAeth matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDBETAeth_F(int variable, int repReadData){
					//variables: ten variables in DBETAeth matrix
					/*indices
					 * 0: White
					 * 1: Not Recorded
					 * 2: Indian
					 * 3: Pakistani
					 * 4: Bangladeshi
					 * 5: Other Asian
					 * 6: Black Carribean
					 * 7: Black African
					 * 8: Chinese
					 * 9: Other
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=2; // 2 for array index for DBETAeth matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDBETAsmo_M(int variable, int repReadData){
					//variables: five variables in DBETAsmo matrix
					/*indices
					 * 0: non smoker
					 * 1: ex smoker
					 * 2: light smoker
					 * 3: moderate smoker
					 * 4: heavy smoker
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=3; // 3 for array index for DBETAsmo matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDBETAsmo_F(int variable, int repReadData){
					//variables: five variables in DBETAsmo matrix
					/*indices
					 * 0: non smoker
					 * 1: ex smoker
					 * 2: light smoker
					 * 3: moderate smoker
					 * 4: heavy smoker
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=3; // 3 for array index for DBETAsmo matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDBETAcv_M(int variable, int repReadData){
					//variables: five variables in DBETAcv matrix
					/*indices
					 * 0: age1
					 * 1: age2
					 * 2: bmi1
					 * 3: bmi2
					 * 4: town
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=4; // 4 for array index for DBETAcv matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDBETAcv_F(int variable, int repReadData){
					//variables: five variables in DBETAcv matrix
					/*indices
					 * 0: age1
					 * 1: age2
					 * 2: bmi1
					 * 3: bmi2
					 * 4: town
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=4; // 4 for array index for DBETAcv matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDBETAbv_M(int variable, int repReadData){
					//variables: four variables in DBETAbv matrix
					/*indices
					 * 0: cort
					 * 1: cvd
					 * 2: aht
					 * 3: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=5; // 5 for array index for DBETAbv matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDBETAbv_F(int variable, int repReadData){
					//variables: four variables in DBETAbv matrix
					/*indices
					 * 0: cort
					 * 1: cvd
					 * 2: aht
					 * 3: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=5; // 5 for array index for DBETAbv matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDAGE1INT_M(int variable, int repReadData){
					//variables: three variables in DAGE1INT matrix
					/*indices
					 * 0: bmi1
					 * 1: bmi2
					 * 2: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=6; // 6 for array index for DAGE1INT matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDAGE1INT_F(int variable, int repReadData){
					//variables: three variables in DAGE1INT matrix
					/*indices
					 * 0: bmi1
					 * 1: bmi2
					 * 2: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=6; // 6 for array index for DAGE1INT matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				
				public double getDAGE2INT_M(int variable, int repReadData){
					//variables: three variables in DAGE2INT matrix
					/*indices
					 * 0: bmi1
					 * 1: bmi2
					 * 2: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=0; // 0 for array index for Male risk
					int matrix=7; // 7 for array index for DAGE9INT matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
				public double getDAGE2INT_F(int variable, int repReadData){
					//variables: three variables in DAGE2INT matrix
					/*indices
					 * 0: bmi1
					 * 1: bmi2
					 * 2: FHdm2
					*/
					//repReadData = replication -1 --> set in Agent class for all PSA readings
					int risk=1; // 1 for array index for Female risk
					int matrix=7; // 7 for array index for DAGE2INT matrix
					
					double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
					
					return myValue;
				}
		
		////////get elements for QCOVID
		public double getCSURV_M(int variable, int repReadData){
			//variable=0 only one value (baseline) in DSURV matrix
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=0; // 0 for array index for Male risk
			int matrix=0; // 0 for array index for CSURV matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
			
		}
		public double getCSURV_F(int variable, int repReadData){
			//variable=0 only one value (baseline) in DSURV matrix
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=1; // 1 for array index for Female risk
			int matrix=0; // 0 for array index for CSURV matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
		//	System.out.println(myValue);
			return myValue;
		}
		
		public double getAGEBMIPOW_M(int variable, int repReadData){
			//variables: six variables in AGEBMIPOW matrix
			/*indices
			 * 0: age1_pow
			 * 1: age2_pow
			 * 2: bmi_pow
			 * 3: age1
			 * 4: age2
			 * 5: bmi1
			 * 6: bmi2
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=0; // 0 for array index for Male risk
			int matrix=1; // 1 for array index for DXBAR matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
		public double getAGEBMIPOW_F(int variable, int repReadData){
			//variables: six variables in AGEBMIPOW matrix
			/*indices
			 * 0: age1_pow
			 * 1: age2_pow
			 * 2: bmi_pow
			 * 3: age1
			 * 4. age2
			 * 5: bmi1
			 * 6: bmi2
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=1; // 1 for array index for Female risk
			int matrix=1; // 1 for array index for DXBAR matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
		
		public double getCAeth_M(int variable, int repReadData){
			//variables: ten variables in CAeth matrix
			/*indices
			 * 0: White
			 * 1: Not Recorded
			 * 2: Indian
			 * 3: Pakistani
			 * 4: Bangladeshi
			 * 5: Other Asian
			 * 6: Black Carribean
			 * 7: Black African
			 * 8: Chinese
			 * 9: Other
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=0; // 0 for array index for Male risk
			int matrix=2; // 2 for array index for CAeth matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
		public double getCAeth_F(int variable, int repReadData){
			//variables: ten variables in CAeth matrix
			/*indices
			 * 0: White
			 * 1: Not Recorded
			 * 2: Indian
			 * 3: Pakistani
			 * 4: Bangladeshi
			 * 5: Other Asian
			 * 6: Black Carribean
			 * 7: Black African
			 * 8: Chinese
			 * 9: Other
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=1; // 1 for array index for Female risk
			int matrix=2; // 2 for array index for CAeth matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			return myValue;
		}
		
		public double getCMults_M(int variable, int repReadData){
			//variables: fourteen variables in CMults matrix (multiplication factors for diseases)
			/*indices
			 * 0: RenalCat
			 * 1: age1
			 * 2: age2
			 * 3: bmi1
			 * 4: bmi2
			 * 5: town
			 * 6: AF
			 * 7: pulm_hyper
			 * 8: ra_sle
			 * 9: DT1
			 * 10: DT2
			 * 11: CVD
			 * 12: DT1_age
			 * 13: DT2_age
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=0; // 0 for array index for Male risk
			int matrix=3; // 3 for array index for CMults matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
		public double getCMults_F(int variable, int repReadData){
			//variables: five variables in CMults matrix
			/*indices
		     * 0: RenalCat
			 * 1: age1
			 * 2: age2
			 * 3: bmi1
			 * 4: bmi2
			 * 5: town
			 * 6: AF
			 * 7: pulm_hyper
			 * 8: ra_sle
			 * 9: DT1
			 * 10: DT2
			 * 11: CVD
			 * 12: DT1_age
			 * 13: DT2_age
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=1; // 1 for array index for Female risk
			int matrix=3; // 3 for array index for CMults matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
	//		System.out.println(myValue);
			return myValue;
		}
		
		public double getCalib_M(int variable, int repReadData){
			//variables: one variable in Calib matrix
			/*indices
			 * 0: calibration
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=0; // 0 for array index for Male risk
			int matrix=4; // 4 for array index for Calib matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
		public double getCalib_F(int variable, int repReadData){
			//variables: one variable in Calib matrix
			/*indices
			 * 0: calibration
			*/
			//repReadData = replication -1 --> set in Agent class for all PSA readings
			int risk=1; // 1 for array index for Female risk
			int matrix=4; // 4 for array index for Calib matrix
			
			double myValue = Double.valueOf(this.get(risk).get(matrix).get(variable).get(repReadData).toString().trim());
			
			return myValue;
		}
}
		
		
		public static double calculateQRisk(Agent a, int repReadData){
			double qRisk, sum;
			
			//variables for individual agent 
			int sex, eth, smo, af, ra, ckd, aht, dm1, dm2, fhcvd; 
			double age, bmi, rati, sbp, town;
			if (a.getGender().equalsIgnoreCase("M")){sex = 0;} else {sex = 1;}
			age = a.getAge();
			bmi = a.getBMI();
			rati = a.getHDLRatio();
			sbp = a.getSBloodPressure();
			town = a.getDeprivationTown();
			if(a.getEthnicity().equalsIgnoreCase("White")){eth = 0;}
			else if (a.getEthnicity().equalsIgnoreCase("NR")){eth = 1;}
			else if (a.getEthnicity().equalsIgnoreCase("Indian")){eth = 2;}
			else if (a.getEthnicity().equalsIgnoreCase("Pakistani")){eth = 3;}
			else if (a.getEthnicity().equalsIgnoreCase("Bangladeshi")){eth = 4;}
			else if (a.getEthnicity().equalsIgnoreCase("Other_Asian")){eth = 5;}
			else if (a.getEthnicity().equalsIgnoreCase("Black_Carribean")){eth = 6;}
			else if (a.getEthnicity().equalsIgnoreCase("Black_African")){eth = 7;}
			else if (a.getEthnicity().equalsIgnoreCase("Chinese")){eth = 8;}
			else {eth = 9;}
			smo = a.getSmoking(); //0: non, 1: ex, 2: light, 3: moderate, 4: heavy
			af = a.getAtrialFibrillation();
			ra = a.getRheumatoidArthritis();
			ckd = a.getChronicKidneyDisease();
			aht = a.getBpTreatment();
			dm1 = a.getHadT1D();
			if(a.hasT2D()){dm2 = 1;}
			else{dm2 =0;}
			
			fhcvd = a.getFamilyCVDHistory();
			
			//variables for QRisk calculation
			double dage, age1, age2, dbmi, bmi1, bmi2; 
						
			//fit
			if (bmi > 40){bmi = 40;} else if (bmi < 20) {bmi = 20;}
			if (sbp > 210) {sbp = 210;} else if (sbp < 70) {sbp = 70;}
				
			dage = age/10;
			// MALE: sex = 0; FEMALE: sex = 1
			if (sex==1) {age1 = Math.pow(dage, 0.5); age2 = dage;} else {age1 = Math.pow(dage, -1); age2 = Math.pow(dage, 2);}
						
			dbmi = bmi/10;
			bmi1 = Math.pow(dbmi, -2);
			bmi2 = bmi1*Math.log(dbmi);
						
			if (sex == 0) { //MALE
				//Centre
				
				age1 = age1 - PAContext.qRiskCoef.getQXBAR_M(0, repReadData);
				age2 = age2 - PAContext.qRiskCoef.getQXBAR_M(1, repReadData);
				
				bmi1 = bmi1 - PAContext.qRiskCoef.getQXBAR_M(2, repReadData);
				bmi2 = bmi2 - PAContext.qRiskCoef.getQXBAR_M(3, repReadData);
				rati = rati - PAContext.qRiskCoef.getQXBAR_M(4, repReadData);
				sbp = sbp - PAContext.qRiskCoef.getQXBAR_M(5, repReadData);
				town = town - PAContext.qRiskCoef.getQXBAR_M(6, repReadData);
				
				//Sums
				sum = 0;
				
				sum = PAContext.qRiskCoef.getQBETAeth_M(eth, repReadData) + PAContext.qRiskCoef.getQBETAsmo_M(smo, repReadData);
					
				sum = sum + (age1*PAContext.qRiskCoef.getQBETAcv_M(0, repReadData)) + (age2*PAContext.qRiskCoef.getQBETAcv_M(1, repReadData)) 
						+ (bmi1*PAContext.qRiskCoef.getQBETAcv_M(2, repReadData)) + (bmi2*PAContext.qRiskCoef.getQBETAcv_M(3, repReadData))
						+ (rati*PAContext.qRiskCoef.getQBETAcv_M(4, repReadData)) + (sbp*PAContext.qRiskCoef.getQBETAcv_M(5, repReadData))
						+ (town*PAContext.qRiskCoef.getQBETAcv_M(6, repReadData));
		
				sum = sum + (af*PAContext.qRiskCoef.getQBETAbv_M(0, repReadData)) + (ra*PAContext.qRiskCoef.getQBETAbv_M(1, repReadData))
						+ (ckd*PAContext.qRiskCoef.getQBETAbv_M(2, repReadData)) + (aht*PAContext.qRiskCoef.getQBETAbv_M(3, repReadData))
						+ (dm1*PAContext.qRiskCoef.getQBETAbv_M(4, repReadData)) + (dm2*PAContext.qRiskCoef.getQBETAbv_M(5, repReadData))
						+ (fhcvd*PAContext.qRiskCoef.getQBETAbv_M(6, repReadData));
				
				sum = sum + (age1*PAContext.qRiskCoef.getQAGE1SMO_M(smo, repReadData));
				
				sum = sum + (age1*af*PAContext.qRiskCoef.getQAGE1INT_M(0, repReadData)) + (age1*ckd*PAContext.qRiskCoef.getQAGE1INT_M(1, repReadData))
						+ (age1*aht*PAContext.qRiskCoef.getQAGE1INT_M(2, repReadData)) + (age1*dm1*PAContext.qRiskCoef.getQAGE1INT_M(3, repReadData))
						+ (age1*dm2*PAContext.qRiskCoef.getQAGE1INT_M(4, repReadData)) + (age1*bmi1*PAContext.qRiskCoef.getQAGE1INT_M(5, repReadData))
						+ (age1*bmi2*PAContext.qRiskCoef.getQAGE1INT_M(6, repReadData)) + (age1*fhcvd*PAContext.qRiskCoef.getQAGE1INT_M(7, repReadData))
						+ (age1*sbp*PAContext.qRiskCoef.getQAGE1INT_M(8, repReadData)) + (age1*town*PAContext.qRiskCoef.getQAGE1INT_M(9, repReadData));
				
				sum = sum + (age2*PAContext.qRiskCoef.getQAGE2SMO_M(smo, repReadData));
				
				sum = sum + (age2*af*PAContext.qRiskCoef.getQAGE2INT_M(0, repReadData)) + (age2*ckd*PAContext.qRiskCoef.getQAGE2INT_M(1, repReadData))
						+ (age2*aht*PAContext.qRiskCoef.getQAGE2INT_M(2, repReadData)) + (age2*dm1*PAContext.qRiskCoef.getQAGE2INT_M(3, repReadData))
						+ (age2*dm2*PAContext.qRiskCoef.getQAGE2INT_M(4, repReadData)) + (age2*bmi1*PAContext.qRiskCoef.getQAGE2INT_M(5, repReadData))
						+ (age2*bmi2*PAContext.qRiskCoef.getQAGE2INT_M(6, repReadData)) + (age2*fhcvd*PAContext.qRiskCoef.getQAGE2INT_M(7, repReadData))
						+ (age2*sbp*PAContext.qRiskCoef.getQAGE2INT_M(8, repReadData)) + (age2*town*PAContext.qRiskCoef.getQAGE2INT_M(9, repReadData));
											
				qRisk = 1 - Math.pow(PAContext.qRiskCoef.getQSURV_M(0, repReadData), Math.exp(sum)); 
			
			}
			else { //FEMALE
				//Centre
				age1 = age1 - PAContext.qRiskCoef.getQXBAR_F(0, repReadData);
				age2 = age2 - PAContext.qRiskCoef.getQXBAR_F(1, repReadData);
				
				bmi1 = bmi1 - PAContext.qRiskCoef.getQXBAR_F(2, repReadData);
				bmi2 = bmi2 - PAContext.qRiskCoef.getQXBAR_F(3, repReadData);
				rati = rati - PAContext.qRiskCoef.getQXBAR_F(4, repReadData);
				sbp = sbp - PAContext.qRiskCoef.getQXBAR_F(5, repReadData);
				town = town - PAContext.qRiskCoef.getQXBAR_F(6, repReadData);
				
				//Sums
				sum = 0;
				
				sum = PAContext.qRiskCoef.getQBETAeth_F(eth, repReadData) + PAContext.qRiskCoef.getQBETAsmo_F(smo, repReadData);
				
				sum = sum + (age1*PAContext.qRiskCoef.getQBETAcv_F(0, repReadData)) + (age2*PAContext.qRiskCoef.getQBETAcv_F(1, repReadData)) 
						+ (bmi1*PAContext.qRiskCoef.getQBETAcv_F(2, repReadData)) + (bmi2*PAContext.qRiskCoef.getQBETAcv_F(3, repReadData))
						+ (rati*PAContext.qRiskCoef.getQBETAcv_F(4, repReadData)) + (sbp*PAContext.qRiskCoef.getQBETAcv_F(5, repReadData))
						+ (town*PAContext.qRiskCoef.getQBETAcv_F(6, repReadData));
				
				sum = sum + (af*PAContext.qRiskCoef.getQBETAbv_F(0, repReadData)) + (ra*PAContext.qRiskCoef.getQBETAbv_F(1, repReadData))
						+ (ckd*PAContext.qRiskCoef.getQBETAbv_F(2, repReadData)) + (aht*PAContext.qRiskCoef.getQBETAbv_F(3, repReadData))
						+ (dm1*PAContext.qRiskCoef.getQBETAbv_F(4, repReadData)) + (dm2*PAContext.qRiskCoef.getQBETAbv_F(5, repReadData))
						+ (fhcvd*PAContext.qRiskCoef.getQBETAbv_F(6, repReadData));
				
				sum = sum + (age1*PAContext.qRiskCoef.getQAGE1SMO_F(smo, repReadData));
				
				sum = sum + (age1*af*PAContext.qRiskCoef.getQAGE1INT_F(0, repReadData)) + (age1*ckd*PAContext.qRiskCoef.getQAGE1INT_F(1, repReadData))
						+ (age1*aht*PAContext.qRiskCoef.getQAGE1INT_F(2, repReadData)) + (age1*dm1*PAContext.qRiskCoef.getQAGE1INT_F(3, repReadData))
						+ (age1*dm2*PAContext.qRiskCoef.getQAGE1INT_F(4, repReadData)) + (age1*bmi1*PAContext.qRiskCoef.getQAGE1INT_F(5, repReadData))
						+ (age1*bmi2*PAContext.qRiskCoef.getQAGE1INT_F(6, repReadData)) + (age1*fhcvd*PAContext.qRiskCoef.getQAGE1INT_F(7, repReadData))
						+ (age1*sbp*PAContext.qRiskCoef.getQAGE1INT_F(8, repReadData)) + (age1*town*PAContext.qRiskCoef.getQAGE1INT_F(9, repReadData));
				
				sum = sum + (age2*PAContext.qRiskCoef.getQAGE2SMO_F(smo, repReadData));
				
				sum = sum + (age2*af*PAContext.qRiskCoef.getQAGE2INT_F(0, repReadData)) + (age2*ckd*PAContext.qRiskCoef.getQAGE2INT_F(1, repReadData))
						+ (age2*aht*PAContext.qRiskCoef.getQAGE2INT_F(2, repReadData)) + (age2*dm1*PAContext.qRiskCoef.getQAGE2INT_F(3, repReadData))
						+ (age2*dm2*PAContext.qRiskCoef.getQAGE2INT_F(4, repReadData)) + (age2*bmi1*PAContext.qRiskCoef.getQAGE2INT_F(5, repReadData))
						+ (age2*bmi2*PAContext.qRiskCoef.getQAGE2INT_F(6, repReadData)) + (age2*fhcvd*PAContext.qRiskCoef.getQAGE2INT_F(7, repReadData))
						+ (age2*sbp*PAContext.qRiskCoef.getQAGE2INT_F(8, repReadData)) + (age2*town*PAContext.qRiskCoef.getQAGE2INT_F(9, repReadData));
				
				qRisk = 1 - Math.pow(PAContext.qRiskCoef.getQSURV_F(0, repReadData), Math.exp(sum)); 
			}
			
				
			return qRisk;
		} 
		
		public static double calculateDRisk(Agent a, int repReadData){
					
			double dRisk, sum;
			
			//variables for individual agent 
			int sex, eth, smo, cort, cvd, aht, fhdm2; 
			double age, bmi, town;
			
			if (a.getGender().equalsIgnoreCase("M")){sex = 0;} else {sex = 1;}
			age = a.getAge();
			bmi = a.getBMI();
			
			town = a.getDeprivationTown();
			if(a.getEthnicity().equalsIgnoreCase("White")){eth = 0;}
			else if (a.getEthnicity().equalsIgnoreCase("NR")){eth = 1;}
			else if (a.getEthnicity().equalsIgnoreCase("Indian")){eth = 2;}
			else if (a.getEthnicity().equalsIgnoreCase("Pakistani")){eth = 3;}
			else if (a.getEthnicity().equalsIgnoreCase("Bangladeshi")){eth = 4;}
			else if (a.getEthnicity().equalsIgnoreCase("Other_Asian")){eth = 5;}
			else if (a.getEthnicity().equalsIgnoreCase("Black_Carribean")){eth = 6;}
			else if (a.getEthnicity().equalsIgnoreCase("Black_African")){eth = 7;}
			else if (a.getEthnicity().equalsIgnoreCase("Chinese")){eth = 8;}
			else {eth = 9;}
			smo = a.getSmoking(); //0: non, 1: ex, 2: light, 3: moderate, 4: heavy
			aht = a.getBpTreatment();
			fhdm2 = a.getFamilyT2DHistory();
			cort = a.getCort();
			if (a.hasCVD()) {cvd = 1;} else {cvd = 0;}
			
			//variables for QRisk calculation
			double dage, age1, age2, dbmi, bmi1, bmi2; 
						
			//fit
			if (bmi > 40){bmi = 40;} else if (bmi < 20) {bmi = 20;}
				
			//TODO check with Catriona and Nana
			dage = age/10;
			if (sex == 1) {age1 = Math.pow(dage, 0.5); age2 = Math.pow(dage, 3);} else {age1 = Math.log(dage); age2 = Math.pow(dage, 3);}
						
			dbmi = bmi/10;
			if (sex == 1) {bmi1 = dbmi; bmi2 = Math.pow(dbmi, 3);} else {bmi1 = Math.pow(dbmi, 2); bmi2 = Math.pow(dbmi, 3);}
						
			if (sex == 0) { //MALE
				//Centre
				age1 = age1 - PAContext.dRiskCoef.getDXBAR_M(0, repReadData);
				age2 = age2 - PAContext.dRiskCoef.getDXBAR_M(1, repReadData);
				
				bmi1 = bmi1 - PAContext.dRiskCoef.getDXBAR_M(2, repReadData);
				bmi2 = bmi2 - PAContext.dRiskCoef.getDXBAR_M(3, repReadData);
				town = town - PAContext.dRiskCoef.getDXBAR_M(4, repReadData);
				
				//Sums
				sum = 0;
				
				sum = PAContext.dRiskCoef.getDBETAeth_M(eth, repReadData) + PAContext.dRiskCoef.getDBETAsmo_M(smo, repReadData);
				
				sum = sum + (age1*PAContext.dRiskCoef.getDBETAcv_M(0, repReadData)) + (age2*PAContext.dRiskCoef.getDBETAcv_M(1, repReadData)) 
						+ (bmi1*PAContext.dRiskCoef.getDBETAcv_M(2, repReadData)) + (bmi2*PAContext.dRiskCoef.getDBETAcv_M(3, repReadData))
						+ (town*PAContext.dRiskCoef.getDBETAcv_M(4, repReadData));
				
				sum = sum + (cort*PAContext.dRiskCoef.getDBETAbv_M(0, repReadData)) + (cvd*PAContext.dRiskCoef.getDBETAbv_M(1, repReadData))
						+ (aht*PAContext.dRiskCoef.getDBETAbv_M(2, repReadData)) + (fhdm2*PAContext.dRiskCoef.getDBETAbv_M(3, repReadData));
				
				sum = sum + (age1*bmi1*PAContext.dRiskCoef.getDAGE1INT_M(0, repReadData)) + (age1*bmi2*PAContext.dRiskCoef.getDAGE1INT_M(1, repReadData))
						+ (age1*fhdm2*PAContext.dRiskCoef.getDAGE1INT_M(2, repReadData));
						
				sum = sum + (age2*bmi1*PAContext.dRiskCoef.getDAGE2INT_M(0, repReadData)) + (age2*bmi2*PAContext.dRiskCoef.getDAGE2INT_M(1, repReadData))
						+ (age2*fhdm2*PAContext.dRiskCoef.getDAGE2INT_M(2, repReadData));
				
				dRisk = 1 - Math.pow(PAContext.dRiskCoef.getDSURV_M(0, repReadData), Math.exp(sum)); 
			}
			else { //FEMALE
				//Centre
				age1 = age1 - PAContext.dRiskCoef.getDXBAR_F(0, repReadData);
				age2 = age2 - PAContext.dRiskCoef.getDXBAR_F(1, repReadData);
				
				bmi1 = bmi1 - PAContext.dRiskCoef.getDXBAR_F(2, repReadData);
				bmi2 = bmi2 - PAContext.dRiskCoef.getDXBAR_F(3, repReadData);
				town = town - PAContext.dRiskCoef.getDXBAR_F(4, repReadData);
				
				//Sums
				sum = 0;
				
				sum = PAContext.dRiskCoef.getDBETAeth_F(eth, repReadData) + PAContext.dRiskCoef.getDBETAsmo_F(smo, repReadData);
				
				sum = sum + (age1*PAContext.dRiskCoef.getDBETAcv_F(0, repReadData)) + (age2*PAContext.dRiskCoef.getDBETAcv_F(1, repReadData)) 
						+ (bmi1*PAContext.dRiskCoef.getDBETAcv_F(2, repReadData)) + (bmi2*PAContext.dRiskCoef.getDBETAcv_F(3, repReadData))
						+ (town*PAContext.dRiskCoef.getDBETAcv_F(4, repReadData));
				
				sum = sum + (cort*PAContext.dRiskCoef.getDBETAbv_F(0, repReadData)) + (cvd*PAContext.dRiskCoef.getDBETAbv_F(1, repReadData))
						+ (aht*PAContext.dRiskCoef.getDBETAbv_F(2, repReadData)) + (fhdm2*PAContext.dRiskCoef.getDBETAbv_F(3, repReadData));
				
				sum = sum + (age1*bmi1*PAContext.dRiskCoef.getDAGE1INT_F(0, repReadData)) + (age1*bmi2*PAContext.dRiskCoef.getDAGE1INT_F(1, repReadData))
						+ (age1*fhdm2*PAContext.dRiskCoef.getDAGE1INT_F(2, repReadData));
						
				sum = sum + (age2*bmi1*PAContext.dRiskCoef.getDAGE2INT_F(0, repReadData)) + (age2*bmi2*PAContext.dRiskCoef.getDAGE2INT_F(1, repReadData))
						+ (age2*fhdm2*PAContext.dRiskCoef.getDAGE2INT_F(2, repReadData));
				
				dRisk = 1 - Math.pow(PAContext.dRiskCoef.getDSURV_F(0, repReadData), Math.exp(sum)); 
			}
			
				
			return dRisk;
		} 
		
		/// QCOVID algorithm
		public static double calculateCRisk(Agent b, int repReadData) {
			        
			        double score;
			
			        double ethrisk, renalcat;
		        	int  sex, b_type2, cvd;
		        	
					if (b.getGender().equalsIgnoreCase("M")){sex = 0;} else {sex = 1;}				
					double age=b.getAge();		
					double bmi= b.getBMI(); 
					if (bmi > 40){bmi = 40;} else if (bmi < 20) {bmi = 20;}								
					
					int b_ra_sle=b.getRheumatoidArthritis();
					int b_pulmhyper= b.getHypertension();			
					int b_type1=b.getHadT1D();
					int b_AF=b.getAtrialFibrillation();
					
					if(b.hasT2D()){b_type2 = 1;}
					else {b_type2=0;}
					if (b.hasChronicKidneyDisease()) {renalcat=PAContext.qCovidCoef.getCMults_M(0, repReadData);} // assuming CD5 without dialysis as an average
					else {renalcat=0;}
					if (b.hasCVD()) {cvd= 1;}
					else {cvd=0;} // CVD includes stroke, CHD
					
					double town=b.getDeprivationTown(); // townsend score - mean for England and Wales is 0
					
					if (sex == 0) {
						
					double surv=PAContext.qCovidCoef.getCSURV_M(0, repReadData);
					
					if(b.getEthnicity().equalsIgnoreCase("White")){ethrisk = PAContext.qCovidCoef.getCAeth_M(0, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("NR")){ethrisk = PAContext.qCovidCoef.getCAeth_M(1, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Indian")){ethrisk = PAContext.qCovidCoef.getCAeth_M(2, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Pakistani")){ethrisk = PAContext.qCovidCoef.getCAeth_M(3, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Bangladeshi")){ethrisk = PAContext.qCovidCoef.getCAeth_M(4, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Other_Asian")){ethrisk = PAContext.qCovidCoef.getCAeth_M(5, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Black_Carribean")){ethrisk = PAContext.qCovidCoef.getCAeth_M(6, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Black_African")){ethrisk = PAContext.qCovidCoef.getCAeth_M(7, repReadData);}
					else if (b.getEthnicity().equalsIgnoreCase("Chinese")){ethrisk = PAContext.qCovidCoef.getCAeth_M(8, repReadData);}
					else {ethrisk = PAContext.qCovidCoef.getCAeth_M(9, repReadData);}
											  				
						double dage = age;
						dage=dage/10;
						double agePow_1 = PAContext.qCovidCoef.getAGEBMIPOW_M(0, repReadData);
						double agePow_2 = PAContext.qCovidCoef.getAGEBMIPOW_M(1, repReadData);
						double age_1 = Math.pow(dage,agePow_1);
						double age_2 = Math.pow(dage,agePow_2);
											
						double dbmi = bmi;
						dbmi=dbmi/10;
						double bmiPow_1 = PAContext.qCovidCoef.getAGEBMIPOW_M(2, repReadData);
						double bmi_1 = Math.pow(dbmi,bmiPow_1);
						double bmi_2 = Math.log(dbmi);

						age_1 = age_1 - PAContext.qCovidCoef.getAGEBMIPOW_M(3, repReadData);
						age_2 = age_2 - PAContext.qCovidCoef.getAGEBMIPOW_M(4, repReadData);
						bmi_1 = bmi_1 - PAContext.qCovidCoef.getAGEBMIPOW_M(5, repReadData);
						bmi_2 = bmi_2 - PAContext.qCovidCoef.getAGEBMIPOW_M(6, repReadData);
			//			town = town - 0.436863929033279;

						double a=0;

						a += ethrisk;  // ethnicity - White, Indian, Pakistani, Bangladeshi, Other Asian, Caribbean, Black African, Chinese, other ethnic group
						a += age_1 * PAContext.qCovidCoef.getCMults_M(1, repReadData);
						a += age_2 * PAContext.qCovidCoef.getCMults_M(2, repReadData);
						a += bmi_1 * PAContext.qCovidCoef.getCMults_M(3, repReadData);
						a += bmi_2 * PAContext.qCovidCoef.getCMults_M(4, repReadData);
						a += town * PAContext.qCovidCoef.getCMults_M(5, repReadData);

						a += b_AF * PAContext.qCovidCoef.getCMults_M(6, repReadData);
						a += b_pulmhyper * PAContext.qCovidCoef.getCMults_M(7, repReadData);
						a += b_ra_sle * PAContext.qCovidCoef.getCMults_M(8, repReadData);
						a += b_type1 * PAContext.qCovidCoef.getCMults_M(9, repReadData);
						a += b_type2 * PAContext.qCovidCoef.getCMults_M(10, repReadData);
						a += cvd * PAContext.qCovidCoef.getCMults_M(11, repReadData);   // includes stroke, chd, congenheart and pvd - taken a mean of multipliers 
						
						a += age_1 * b_type2 * PAContext.qCovidCoef.getCMults_M(12, repReadData);
						a += age_2 * b_type2 * PAContext.qCovidCoef.getCMults_M(13, repReadData);
						
						score = PAContext.qCovidCoef.getCalib_M(0, repReadData)*(1 - Math.pow(surv, Math.exp(a))) ;
					}
				
					else {
						
						double surv=PAContext.qCovidCoef.getCSURV_F(0, repReadData);
						
						if(b.getEthnicity().equalsIgnoreCase("White")){ethrisk = PAContext.qCovidCoef.getCAeth_F(0, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("NR")){ethrisk = PAContext.qCovidCoef.getCAeth_F(1, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Indian")){ethrisk = PAContext.qCovidCoef.getCAeth_F(2, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Pakistani")){ethrisk = PAContext.qCovidCoef.getCAeth_F(3, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Bangladeshi")){ethrisk = PAContext.qCovidCoef.getCAeth_F(4, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Other_Asian")){ethrisk = PAContext.qCovidCoef.getCAeth_F(5, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Black_Carribean")){ethrisk = PAContext.qCovidCoef.getCAeth_F(6, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Black_African")){ethrisk = PAContext.qCovidCoef.getCAeth_F(7, repReadData);}
						else if (b.getEthnicity().equalsIgnoreCase("Chinese")){ethrisk = PAContext.qCovidCoef.getCAeth_F(8, repReadData);}
						else {ethrisk = PAContext.qCovidCoef.getCAeth_F(9, repReadData);}
												  				
							double dage = age;
							dage=dage/10;
							double agePow_1 = PAContext.qCovidCoef.getAGEBMIPOW_F(0, repReadData);
							double agePow_2 = PAContext.qCovidCoef.getAGEBMIPOW_F(1, repReadData);			
							double age_1 = Math.pow(dage,agePow_1);
							double age_2 = Math.pow(dage,agePow_2);
												
							double dbmi = bmi;
							dbmi=dbmi/10;
							double bmiPow_1 = PAContext.qCovidCoef.getAGEBMIPOW_F(2, repReadData);
							double bmi_1 = Math.pow(dbmi,bmiPow_1);
							double bmi_2 = Math.log(dbmi);

							age_1 = age_1 - PAContext.qCovidCoef.getAGEBMIPOW_F(3, repReadData);
							age_2 = age_2 - PAContext.qCovidCoef.getAGEBMIPOW_F(4, repReadData);
							bmi_1 = bmi_1 - PAContext.qCovidCoef.getAGEBMIPOW_F(5, repReadData);
							bmi_2 = bmi_2 - PAContext.qCovidCoef.getAGEBMIPOW_F(6, repReadData);
				//			town = town - 0.327639788389206;

							double a=0;

							a += ethrisk;  // ethnicity - White, Indian, Pakistani, Bangladeshi, Other Asian, Caribbean, Black African, Chinese, other ethnic group

							a += age_1 * PAContext.qCovidCoef.getCMults_F(1, repReadData);
							a += age_2 * PAContext.qCovidCoef.getCMults_F(2, repReadData);
							a += bmi_1 * PAContext.qCovidCoef.getCMults_F(3, repReadData);
							a += bmi_2 * PAContext.qCovidCoef.getCMults_F(4, repReadData);
							a += town * PAContext.qCovidCoef.getCMults_F(5, repReadData);

							a += b_AF * PAContext.qCovidCoef.getCMults_F(6, repReadData);
							a += b_pulmhyper * PAContext.qCovidCoef.getCMults_F(7, repReadData);
							a += b_ra_sle *  PAContext.qCovidCoef.getCMults_F(8, repReadData);
							a += b_type1 * PAContext.qCovidCoef.getCMults_F(9, repReadData);
							a += b_type2 * PAContext.qCovidCoef.getCMults_F(10, repReadData);
							a += cvd * PAContext.qCovidCoef.getCMults_F(11, repReadData); // includes stroke, chd, congenheart and pvd - taken a mean of multipliers 
							
							a += age_1 * b_type2 * PAContext.qCovidCoef.getCMults_F(12, repReadData);
							a += age_2 * b_type2 * PAContext.qCovidCoef.getCMults_F(13, repReadData);
							
							score = PAContext.qCovidCoef.getCalib_F(0, repReadData)*(1 - Math.pow(surv, Math.exp(a))) ;
					}
						return score;
				}
		
	
		public static double calculateTimeAdjustedProbability(Agent a, double probability, double timeCurrent, double timeNew){
			double rate, returnProb;	
			
			rate = (-1*(Math.log(1-probability)))/timeCurrent;
			returnProb = 1-Math.exp((-1*rate)*timeNew);
			return returnProb;			
		}
		
		public static double rrPAEventRisk(Agent a, My2DArray<Double> paOn){
			double rrRisk, rr = 0;
			double paStatus = a.getPAStatus();		
			int pa = 100; 
			
			if (paStatus >= 100*paOn.size()){rr = (paOn.get(paOn.size()-1).get(a.repReadData));}
			else{
				for(int i = 0; i < paOn.size(); i++){
					if (paStatus<pa){rr = paOn.get(i).get(a.repReadData);
						break;
					}
					pa+=100;
				}
			}
			
			rrRisk = rr;
			return rrRisk;
		}
		
		public static double rrPADprsProbability(Agent a){
			double rrDepProbability;
			double paStatus = a.getPAStatus();
		
			if(paStatus <= 69){rrDepProbability = PAContext.rrPAOnDepression.get(0).get(a.repReadData);}
			else if(paStatus>=70 && paStatus<=209){rrDepProbability = PAContext.rrPAOnDepression.get(1).get(a.repReadData);}
			else if(paStatus>=210 && paStatus<=419){rrDepProbability = PAContext.rrPAOnDepression.get(2).get(a.repReadData);}
			else if(paStatus>=420 && paStatus<=629){rrDepProbability = PAContext.rrPAOnDepression.get(3).get(a.repReadData);}
			else{rrDepProbability = PAContext.rrPAOnDepression.get(4).get(a.repReadData);}
			
			return rrDepProbability;
		}
	
		public static double rrPAmsiProbability(Agent a){
			double rrMSIProbability;
			double paStatus = a.getPAStatus();
			
			if(a.getGender().equalsIgnoreCase("M")){
				if(paStatus <= 59){rrMSIProbability = PAContext.rrPAOnMSI.get(0).get(a.repReadData);}
				else if(paStatus>=60 && paStatus<=300){rrMSIProbability = PAContext.rrPAOnMSI.get(1).get(a.repReadData);}
				else if(paStatus>=301 && paStatus<=600){rrMSIProbability = PAContext.rrPAOnMSI.get(2).get(a.repReadData);}
				else{rrMSIProbability = PAContext.rrPAOnMSI.get(3).get(a.repReadData);}
			}
			else{
				if(paStatus <= 59){rrMSIProbability = PAContext.rrPAOnMSI.get(4).get(a.repReadData);}
				else if(paStatus>=60 && paStatus<=300){rrMSIProbability = PAContext.rrPAOnMSI.get(5).get(a.repReadData);}
				else if(paStatus>=301 && paStatus<=600){rrMSIProbability = PAContext.rrPAOnMSI.get(6).get(a.repReadData);}
				else{rrMSIProbability = PAContext.rrPAOnMSI.get(7).get(a.repReadData);}
			}
			
			return rrMSIProbability;
		}
		
		
		public static double cvdStrokeProportion(Agent a){
			double cvdStrokeProportion;
			double age = a.getAge();
		/*
			int aa = 35; 
			
			for(int i = 0; i < PAContext.proportionStrokeCHD.size()/2; i++){
				if(age < aa){
					cvdStrokeProportion = PAContext.proportionStrokeCHD.get(i).get(a.repReadData);
					break;
				}
				aa+=10;
			}
			*/
			
				if(age < 35){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(0).get(a.repReadData);}
				else if(age >=35 && age <= 44){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(1).get(a.repReadData);}
				else if(age >= 45 && age <= 54){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(2).get(a.repReadData);}
				else if(age >= 55 && age <= 64){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(3).get(a.repReadData);}
				else if(age >= 65 && age <= 74){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(4).get(a.repReadData);}
				else if(age >= 75 && age <= 84){cvdStrokeProportion = PAContext.proportionStrokeCHD.get(5).get(a.repReadData);}
				else{cvdStrokeProportion = PAContext.proportionStrokeCHD.get(6).get(a.repReadData);}
					
			return cvdStrokeProportion;
		}
		
		public static double cvdCHDProportion(Agent a){
			double cvdCHDProportion;
			double age = a.getAge();
		
				if(age <= 35){cvdCHDProportion = PAContext.proportionStrokeCHD.get(7).get(a.repReadData);}
				else if(age >=35 && age <= 44){cvdCHDProportion = PAContext.proportionStrokeCHD.get(8).get(a.repReadData);}
				else if(age >= 45 && age <= 54){cvdCHDProportion = PAContext.proportionStrokeCHD.get(9).get(a.repReadData);}
				else if(age >= 55 && age <= 64){cvdCHDProportion = PAContext.proportionStrokeCHD.get(10).get(a.repReadData);}
				else if(age >= 65 && age <= 74){cvdCHDProportion = PAContext.proportionStrokeCHD.get(11).get(a.repReadData);}
				else if(age >= 75 && age <= 84){cvdCHDProportion = PAContext.proportionStrokeCHD.get(12).get(a.repReadData);}
				else{cvdCHDProportion = PAContext.proportionStrokeCHD.get(13).get(a.repReadData);}
					
			return cvdCHDProportion;
		}
		
		public static double fatalityCHD(Agent a){
			double fatalityCHD;
			double age = a.getAge();
			
			if(age < 30){fatalityCHD = PAContext.fatalityCHD.get(0).get(a.repReadData);}
			else if(age >=30 && age <= 54){fatalityCHD = PAContext.fatalityCHD.get(1).get(a.repReadData);}
			else if(age >= 55 && age <= 64){fatalityCHD = PAContext.fatalityCHD.get(2).get(a.repReadData);}
			else if(age >= 65 && age <= 74){fatalityCHD = PAContext.fatalityCHD.get(3).get(a.repReadData);}
			else if(age >= 75 && age <= 84){fatalityCHD = PAContext.fatalityCHD.get(4).get(a.repReadData);}
			else{fatalityCHD = PAContext.fatalityCHD.get(5).get(a.repReadData);}
					
			return fatalityCHD;
		}
		
		public static double fatalityStroke(Agent a){
			double fatalityStroke;
			double age = a.getAge();
			
			if(a.getGender().equalsIgnoreCase("M")){
				if(age<75){fatalityStroke = PAContext.fatalityStroke.get(0).get(a.repReadData);}
				else{fatalityStroke = PAContext.fatalityStroke.get(1).get(a.repReadData);}
			}
			else{
				if(age<75){fatalityStroke = PAContext.fatalityStroke.get(2).get(a.repReadData);}
				else{fatalityStroke = PAContext.fatalityStroke.get(3).get(a.repReadData);}
			}
					
			return fatalityStroke;
		}
		
		// not needed for CALMS
		
		//Method to calculate discount for a simulation time unit based on daily increment and daily discount rate
/*		public static double calculateDiscount(double dailyIncrement, double dailyDiscountRate){
			double discounted = 0;
			int dt = (int) Util.CONVERT_DAYS;
			discounted = (dailyIncrement * (1 - Math.pow(1+dailyDiscountRate, dt))) / dailyDiscountRate;
			return discounted;
		} */
		
		public static void updateAgeRelatedCharacteristics(Agent a){
			
			double bmi, hdl, sbp;
			double ageSq3Mnths = a.getAge()*(12/3);
			
			if(a.getGender().equalsIgnoreCase("M")){
				bmi = a.getBMI() + 0.124 - 0.00027*ageSq3Mnths;
				hdl = a.getHDLRatio() + 0.0207 - 0.000045*ageSq3Mnths;
				sbp = a.getSBloodPressure() + 0.179 - 0.00023*ageSq3Mnths;
				
			}
			else{
				bmi = a.getBMI() + 0.107 - 0.00023*ageSq3Mnths;
				hdl = a.getHDLRatio() + 0.0134 - 0.000027*ageSq3Mnths;
				sbp = a.getSBloodPressure() + 0.1 - 0.000027*ageSq3Mnths;
			}
			
			a.setBMI(bmi);
			a.setHDLRatio(hdl);
			a.setSBloodPressure(sbp);
		}
		
		
		public static void getEvent(ArrayList<OccuredEvent> historyList, Record rec, Agent a){
			OccuredEvent oe = new OccuredEvent(rec, a);
			historyList.add(oe);
			//System.out.println("in get event");
			
			a.setTotalNoOfEvents(a.getTotalNoOfEvents()+1);
			
			if (oe.getEventName().equalsIgnoreCase(Events.DEPRESSION.toString().trim())){
				a.setNoOfDprsEpisodes(a.getNoOfDprsEpisodes()+1);
			}
			else if (oe.getEventName().equalsIgnoreCase(Events.MSI.toString().trim())){
				a.setNoOfMSI(a.getNoOfMSI()+1);
			}
			else if (oe.getEventName().equalsIgnoreCase(Events.STROKE.toString().trim())){
				a.setNoOfStroke(a.getNoOfStroke()+1);
				a.setTotalNoOfCVD(a.getTotalNoOfCVD()+1);
			}
			else if (oe.getEventName().equalsIgnoreCase(Events.CHD.toString().trim())){
				a.setNoOfCHD(a.getNoOfCHD()+1);
				a.setTotalNoOfCVD(a.getTotalNoOfCVD()+1);
			}
			else if (oe.getEventName().equalsIgnoreCase(Events.T2D.toString().trim())){
				a.setNoOfT2D(a.getNoOfT2D()+1);
			}
		}
		
				
		public static void updateBaselinePAStatus (Agent a){
			double pa, meanAge, sdAge, ndAge, meanAgeSq, sdAgeSq, ndAgeSq;
			double ageSq3Mnths = a.getAge()*(12/3);
			
			if(a.getGender().equalsIgnoreCase("M")){
				if(a.getPAStatus() < PA_MODERATELY_THRESHOLD){
					meanAge = 0.002; sdAge = 1.248; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.0001; sdAgeSq = 0; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() + ndAge - ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
					
				}
				else if(a.getPAStatus() >= PA_MODERATELY_THRESHOLD && a.getPAStatus() < PA_VERY_THRESHOLD){
					meanAge = 0.122; sdAge = 0.015; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.0001; sdAgeSq = 0; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() - ndAge + ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
					
				}
				else {
					meanAge = 3.865; sdAge = 34.256; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.010; sdAgeSq = 0.121; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() + ndAge - ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
					if(pa>=3600){pa=3600;} // 8.5 hours per day -- max in database
					a.setPAStatus(pa);
				
				}
			}
			else{
				if(a.getPAStatus() < PA_MODERATELY_THRESHOLD){
					meanAge = 0.040; sdAge = 1.276; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.0002; sdAgeSq = 0; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() + ndAge - ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
				}
				else if(a.getPAStatus() >= PA_MODERATELY_THRESHOLD && a.getPAStatus() < PA_VERY_THRESHOLD){
					meanAge = 0.116; sdAge = 4.873; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.0001; sdAgeSq = 0; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() - ndAge + ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
				}
				else {
					meanAge = 4.328; sdAge = 30.490; ndAge = RandomHelper.createNormal(meanAge, sdAge).nextDouble();
					meanAgeSq = 0.012; sdAgeSq = 0.079; ndAgeSq = RandomHelper.createNormal(meanAgeSq, sdAgeSq).nextDouble();
					pa = a.getPAStatus() + ndAge - ndAgeSq*ageSq3Mnths;
					if(pa<0){pa=0;}
					a.setPAStatus(pa);
					if(pa>=3600){pa=3600;} // 8.5 hours per day -- max in database
					a.setPAStatus(pa);
				}
			}
		}
		
		public static void eventUpdates(Agent a){
			
			double t = getCurrentTime();
		
			if (a.getCHDEventsList().size()!=0){
			//	System.out.println("in CHD list");
				if(getMonths(t-a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getStartTime()) < a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcuteDuration()){
					//System.out.println("in CHD acute");
					a.updateTotalEventCost(a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcuteCost()*CONVERT_MONTHS);
					a.setPAStatus(a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcutePA());
					a.setInAcutePeriod(true);
				}
				else{
					//System.out.println("in CHD post acute");
					a.updateTotalEventCost(a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventPostAcuteCost()*CONVERT_MONTHS);
				}
				
				if(getMonths(t-a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getStartTime()) == a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcuteDuration()){
					a.getCHDEventsList().get(a.getCHDEventsList().size()-1).setPostPAStatus(a);
					//System.out.println("in CHD post acute PA");
					//System.out.println(a.getPAStatus());
					a.setInAcutePeriod(false);
				}
			
			}
			if (a.getStrokeEventsList().size()!=0){
				//System.out.println("in Stroke list");
				if(getMonths(t-a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getStartTime()) < a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventAcuteDuration()){
					//System.out.println("in Stroke acute");
					a.updateTotalEventCost(a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventAcuteCost()*CONVERT_MONTHS);
					a.setInAcutePeriod(true);
				}
				else{
					//System.out.println("in Stroke post acute");
					a.updateTotalEventCost(a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventPostAcuteCost()*CONVERT_MONTHS);
				}
				
				if(getMonths(t-a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getStartTime()) == a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventAcuteDuration()){
					a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).setPostPAStatus(a);
					//System.out.println("in Stroke post acute PA");
					//System.out.println(a.getPAStatus());
					a.setInAcutePeriod(false);
				}
			}
			if (a.getT2DEventsList().size()!=0){
				//System.out.println("in T2D list");
				if(getMonths(t-a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getStartTime()) < a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getEventAcuteDuration()){
					//System.out.println("in T2D acute");
					a.updateTotalEventCost(a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getEventAcuteCost()*CONVERT_MONTHS);
					a.setInAcutePeriod(true);
				}
				else{
					//System.out.println("in T2D post acute");
					a.updateTotalEventCost(a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getEventPostAcuteCost()*CONVERT_MONTHS);
				}
				
				if(getMonths(t-a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getStartTime()) == a.getT2DEventsList().get(a.getT2DEventsList().size()-1).getEventAcuteDuration()){
					a.getT2DEventsList().get(a.getT2DEventsList().size()-1).setPostPAStatus(a);
					//System.out.println("in T2D post acute PA");
					//System.out.println(a.getPAStatus());
					a.setInAcutePeriod(false);
				}
			}
			if (a.getMSIEventsList().size()!=0){
				//System.out.println("in MSI list");
				//System.out.println(a.getMSIEventsList().size());
				if(getMonths(t-a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getStartTime()) < a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventAcuteDuration()){
					//System.out.println("in MSI acute");
					a.updateTotalEventCost(a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventAcuteCost()*CONVERT_MONTHS);
					a.setInAcutePeriod(true);
				}
				else{
					//System.out.println("in MSI post acute");
					a.updateTotalEventCost(a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventPostAcuteCost()*CONVERT_MONTHS);
				}
				
				if(getMonths(t-a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getStartTime()) == a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventAcuteDuration()){
					a.getMSIEventsList().get(a.getMSIEventsList().size()-1).setPostPAStatus(a);
					//System.out.println("in MSI post acute PA");
					//System.out.println(a.getPAStatus());
					a.setInAcutePeriod(false);
				}
			}
			if (a.getDepressionEventsList().size()!=0){
				//System.out.println("in Depression list");
				if(getMonths(t-a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getStartTime()) < a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventAcuteDuration()){
				//	System.out.println("in Depression acute");
					a.updateTotalEventCost(a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventAcuteCost()*CONVERT_MONTHS);
					a.setInAcutePeriod(true);
				}
				else{
					//System.out.println("in Depression post acute");
					a.updateTotalEventCost(a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventPostAcuteCost()*CONVERT_MONTHS);
				}
				
				if(getMonths(t-a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getStartTime()) == a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventAcuteDuration()){
					a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).setPostPAStatus(a);
				//	System.out.println("in Depression post acute PA");
				//	System.out.println(a.getPAStatus());
					a.setInAcutePeriod(false);
				}
			}
			
		}
		
		//@ScheduledMethod(start=4, interval=4, priority=ScheduleParameters.LAST_PRIORITY-1)
		public static void recordAnnualPSAOutput(){
			
			AnnualPSAOutput year = new AnnualPSAOutput();
			Context context = RunState.getInstance().getMasterContext();
			for (Object obj : context) {
				if(obj instanceof Agent){
					
					year.getLifeYears().recordDT(((Agent) obj).getLifeYears());
					year.getPAStatus().recordDT(((Agent) obj).getPAStatus());
					year.getBMI().recordDT(((Agent) obj).getBMI());
					year.getHDL().recordDT(((Agent) obj).getHDLRatio());
					year.getSBP().recordDT(((Agent) obj).getSBloodPressure());
					year.getEvents().recordDT(((Agent) obj).getTotalNoOfEvents());
					year.getCVDEvents().recordDT(((Agent) obj).getTotalNoOfCVD()); 
					year.getMSIEvents().recordDT(((Agent) obj).getNoOfMSI());
					year.getDepressionEpisodes().recordDT(((Agent) obj).getNoOfDprsEpisodes());
					year.getStrokeEvents().recordDT(((Agent) obj).getNoOfStroke());
					year.getCHDEvents().recordDT(((Agent) obj).getNoOfCHD());
					year.getT2DEvents().recordDT(((Agent) obj).getNoOfT2D());
					year.getYearsActive().recordDT(((Agent) obj).getYearsActive());
					year.getEventCost().recordDT(((Agent) obj).getTotalEventCost());
				
					year.getTimesInfected().recordDT(((Agent) obj).getTimesInfected());
					year.getTimesHospitalised().recordDT(((Agent) obj).getTimesHospitalised());
					year.getTimesICU().recordDT(((Agent) obj).getTimesICU());
					year.getDiedOfCovid().recordDT(((Agent) obj).getDiedOfCovid());
					year.getTimesLongCovid().recordDT(((Agent) obj).getTimesLongCovid());
					year.getTotalHospitalCost().recordDT(((Agent) obj).getTotalHospitalCost());
					year.getTotalICUCost().recordDT(((Agent) obj).getTotalICUCost());
					year.getTotalLongCovidCost().recordDT(((Agent) obj).getTotalLongCovidCost());
					year.getTotalLockdownCost().recordDT(((Agent) obj).getTotallockdownCost());
					year.getTotalVaccinationCost().recordDT(((Agent) obj).getVaccineCost());
					year.getTotalHealthcareCostCovid().recordDT(((Agent) obj).getTotalHealthCostCovid());
					year.getTotalInterventionCostCovid().recordDT(((Agent) obj).getTotalInterventionCostCovid());
					year.getTimesVaccinated().recordDT(((Agent) obj).getVaccinated());
				//
				}
			}
			PAContext.psaAnnualStatsList.add(year);	
		}
		
}