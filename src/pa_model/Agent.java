package pa_model;

import java.util.ArrayList;
import java.util.Iterator;


import org.apache.commons.math3.distribution.GammaDistribution;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunState;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;

public class Agent {


	
	private int inModelID;
	//////////FROM data set read in files at initialisation////////////////
	private int id, had_CVD, had_T2D, smoking, familyCVDHistory, bpTreatment, rheumatoidArthritis, chronicKidneyDisease, atrialFibrillation, had_T1D, cort, familyT2DHistory, education, 
	had_depression, had_MSI, longLastingIllness, comorbidity;
	private double paStatus, age, bmi, hdlRatio, sBloodPressure, dBloodPressure, deprivationTown, sampleWeight, utilityImpeq5d, deprivationIMD, socioEconomicClass; 
		
	private String gender, ethnicity;
	
	////////////////////////////END///////////////
	
	//////////////////////////////////
	
	/////////////VARIABLES for individual output/////////////////
	private double ageAtEntry, ageAtSimEnd, ageAtDeath, lifeYears, paStatusAtEntry, paStatusAtSimEnd, paStatusAtDeath, 
	bmiAtEntry, bmiAtEnd, hdlRatioAtEntry, hdlRatioAtEnd, sbpAtEntry, sbpAtEnd;
	
	/////////////VARIABLES for updates/////////////////
	private int totalNoOfEvents, totalNoOfCVD, noOfMSI, noOfDprsEpisodes, noOfStroke, noOfCHD, noOfT2D; 
	private double paPreviousPeriod, dprsProbability, msiProbability;
	private double totalEventCost, totalEventCostDiscounted;
	private String causeOfDeath;
	
	private double yearsActive;
	
	public static int repReadData; // = repCounter -1; this is for the array map of events, interventions, probabilities, etc. data  
	
	//keep variables for update events and interventions * use in record id for index for the corresponding event 
	private double qRisk, dRisk, paAdjustedDiabetesRisk, paAdjustedStrokeRisk, paAdjustedCHDRisk, paAdjustedDprsProbability, paAdjustedMSIProbability;
	
	private ArrayList<OccuredEvent> chdEventsList = new ArrayList<OccuredEvent>();
	private ArrayList<OccuredEvent> strokeEventsList = new ArrayList<OccuredEvent>();
	private ArrayList<OccuredEvent> t2dEventsList = new ArrayList<OccuredEvent>();
	private ArrayList<OccuredEvent> msiEventsList = new ArrayList<OccuredEvent>();
	private ArrayList<OccuredEvent> depressionEventsList = new ArrayList<OccuredEvent>();
	
	/////////////////////END/////////////////////
	
	private boolean inAcutePeriod;
	public boolean isDead; 
	
	// Covid variables //
	
	private int ticks, threeMonthTimeStepAdjust, hypertension, exposurePeriod, infectiousPeriod, infectiousToHospital, startDateLockdown, endDateLockdown,
	lockdownScenario, periodicLockdownTimestep, startDateVaccine, endDateVaccine, booster,
	daysSinceExposure, daysSinceInfected, vaccineUptakeDecision, daysSinceVaccine, vaccineImmunityLag, minVaccineImmunityTime, myVaccinationImmunityTime,
	maxVaccineImmunityTime,	boosterTime, severity, hospitalised, icu, longTermEffects, daysOfLongTermEffects, myLongCovidTime, 
	minLongCovidTime, maxLongCovidTime,	minTimeToReInfection, maxTimeToReInfection, timeToReinfection, timeSinceLastInfection, diedOfCovid,
	covidDeathDiabetes, covidDeathCVD, covidDeathHypertension, covidDeathMale, covidDeathObese, 
    icuMorbidlyObese, icuObese, icuOverweight, icuNormalWeight, clinicallyVulnerable, vaccinateMe, beginLockdown;
	private double transmissionProb, nContacts, nContactsLockdown, nContactsLockdownValidation, lockdownCost, totalLockdownCost, lockdownAdherence, myLockdownCost,
	infectionProb, exposuresPerDay, myContacts,  vaccineUptakeProb, vaccineImmunity, vaccineCost, totalVaccineCost, vaccineRiskAdjust, 
	myVaccineCost, cRisk, dailyHospitalCost, totalHospitalCost, dailyICUCost, totalICUCost, myHospitalCost, myICUCost, probICU, ICUDeathProb, hospDeathProb, dailyDeathProb, 
	lengthOfStay, lengthOfStayICU, hosLoSmean, hosLoSsd, icuLoSmean, icuLoSsd, shapeHosp, scaleHosp, shapeICU, scaleICU, 
	gammaHosp, gammaICU, longTermEffectsProb, myLongCovidCost, longCovidCost, introducedInfected, introducedInfectionTimeStep, 
	totalLongCovidCost, propVaccinatedPerDay, totalHealthCostCovid, totalInterventionCostCovid, totalCovidCost, hospitalThreshold;

	public int susceptible, exposed, infected, recovered, vaccinated, timesInfected, timesHospitalised, timesICU, timesLongCovid, timesVaccinated;  
	private double exposure_a, exposure_b, exposure_c;	
    private int mild, severe, critical, beenInfected, beenHospitalised, countDead, nContactsTickCounter;
  
	
	Context context = RunState.getInstance().getMasterContext();
	
	
	
	public Agent(int inModelID){
		
		this.inModelID = inModelID;
		this.isDead = false;
		this.inAcutePeriod = false;
		
		this.yearsActive = 0.0;
		this.totalEventCost = 0.0;
		this.totalEventCostDiscounted = 0.0;
			
		this.totalNoOfEvents = 0;
		this.totalNoOfCVD = 0;
		this.noOfMSI = 0;
		this.noOfDprsEpisodes = 0;
		this.noOfStroke = 0;
		this.noOfCHD = 0;
		this.noOfT2D = 0;
			
		// initialise population for covid parameters //
		// set initial number of susceptible agents //
		if (PAContext.initialSusceptible < Math.random()) {
			this.susceptible = 0;
		}
		else 
			{this.susceptible = 1; }
		
		/// set initial number of exposed agents
		if (PAContext.initialExposed < Math.random()) {
			this.exposed = 0;
		}
		else 
			{this.exposed = 1;}
		
		if (this.exposed == 1){  
			this.susceptible = 0;
	    	this.timesInfected ++;
	    	this.timeToReinfection = 180 + (int)(Math.random() * ((360 - 180) + 1));
		}

	}
	
		@ScheduledMethod(start=1, interval=1, priority=5, shuffle=false)
		public void step(){
			
			if(!(PAContext.clock.getClockYears()==0.0)){
				//all living agents follow the process below 
				
				if(!this.isDead){
					
					// counter for adjusting age,death,and risks to 3 months 
					ticks ++;
					threeMonthTimeStepAdjust ++;
					
					this.clinicallyVulnerableCheck();
				
					if(this.age<25){
						this.qRisk = 0.0;
						this.dRisk = 0.0;
					}
					else{
						this.calculateTimeAdjustedRisk();
					}
					
					if(this.age<5){
						this.setDprsProbability(0);
					}
					else{
						this.updateDprsProbability();
					}
					
					
					if (threeMonthTimeStepAdjust == 90) {	
					this.updateMSIProbability();
					this.paAdjustedDiabetesRisk = Util.rrPAEventRisk(this, PAContext.rrPAOnDiabetes)*this.dRisk;
					this.paAdjustedDprsProbability = Util.rrPADprsProbability(this)*this.dprsProbability;
					this.paAdjustedMSIProbability = Util.rrPAmsiProbability(this)*this.msiProbability;
					this.paAdjustedStrokeRisk = this.qRisk*Util.cvdStrokeProportion(this)*Util.rrPAEventRisk(this, PAContext.rrPAOnStroke);
					this.paAdjustedCHDRisk = this.qRisk*Util.cvdCHDProportion(this)*Util.rrPAEventRisk(this, PAContext.rrPAOnCHD);
					
					this.eventCheck();
					}
					
					/*
					 * some events have fatality risk. if agents have not died from an event we check intervention suitability 
					 * and advance age & age related characteristics
					*/
					if(!this.isDead){

						this.paPreviousPeriod = this.paStatus;
						
						if(!inAcutePeriod){
							
							Util.updateBaselinePAStatus(this);	
						}
											
						// Covid methods //
						
						this.getCovidData();
						this.getCovidInterventionData();
						this.getCovidCostData();
						
						this.updateLockdownScenario();
						this.introducedInfections(); // keeps a small number of infections coming into the cohort 
						this.calculateInfectionProbability(); 
						this.infectAgents();
						this.calculateExposurePeriod(); // triangular distribution 
						this.checkVaccination();
						this.calculateCRisk();			
						this.updateSeverity();					
						if (this.severity == 2) {
							this.updateHospitalDuration();
							this.calculateHospitalCost(); 
						}
						if (this.severity == 3) {
							this.updateICUDuration();
							this.calculateICUCost(); 
						}	
						this.calculateTotalHospitalCost();
						this.checkCovidDeath();
						this.updateInfectionStatus();
						this.checkLongTermEffects();
						this.calculateTotalLongCovidCost();
						this.updateSusceptibility();
						this.calculateTotalCovidCost();
					
						// age and death occur every 90 days
					if (threeMonthTimeStepAdjust == 90) {	
						this.updateAge();
						this.checkDeath();
					}
					}
					
					Util.eventUpdates(this); // cost 
		
					// turn off individual output option for batch runs
					
		/*		if (PAContext.individualRecordOn) {
					IndividualTrackingOutput ind = new IndividualTrackingOutput(this);
					ind.writeOutIndividualTrack(this);
				} */
					
					if (threeMonthTimeStepAdjust > 90) {
					threeMonthTimeStepAdjust = 0;
					}
				}
			}
			
		}
		
		//checking if agent is clinically vulnerable //
		public void clinicallyVulnerableCheck() {
			if(this.hypertension == 1 || this.hasCVD() || this.had_CVD == 1|| this.hasT2D() || this.had_T2D == 1 ||
					this.hadT1D() || this.getBMI() >= 40) {
				this.clinicallyVulnerable = 1;
			}
		}
			
		public void calculateTimeAdjustedRisk(){
		
			this.qRisk = Util.calculateTimeAdjustedProbability(this, Util.calculateQRisk(this, repReadData), 10, Util.CONVERT_YEARS);
			if(!this.hasT2D() && !this.hadT1D()){
				this.dRisk = Util.calculateTimeAdjustedProbability(this, Util.calculateDRisk(this, repReadData), 10, Util.CONVERT_YEARS);
			}
		}
		
		public void updateDprsProbability(){
			if (this.getAge()>=5 && this.getAge()<=15){this.setDprsProbability(PAContext.probDepression.get(1).get(repReadData));}
			else if (this.getAge()>=16 && this.getAge()<=24){this.setDprsProbability(PAContext.probDepression.get(2).get(repReadData));}
			else if (this.getAge()>=25 && this.getAge()<=44){this.setDprsProbability(PAContext.probDepression.get(3).get(repReadData));}
			else if (this.getAge()>=45 && this.getAge()<=64){this.setDprsProbability(PAContext.probDepression.get(4).get(repReadData));}
			else if (this.getAge()>=65 && this.getAge()<=74){this.setDprsProbability(PAContext.probDepression.get(5).get(repReadData));}
			else if (this.getAge()>=75){this.setDprsProbability(PAContext.probDepression.get(6).get(repReadData));}
			
		}
		
		public void updateMSIProbability(){
			if (this.getAge()<18){this.setMSIProbability(PAContext.probMSI.get(0).get(repReadData));}
			else if (this.getAge()>=18 && this.getAge()<=44){this.setMSIProbability(PAContext.probMSI.get(1).get(repReadData));}
			else if (this.getAge()>=45 && this.getAge()<=64){this.setMSIProbability(PAContext.probMSI.get(2).get(repReadData));}
			else if (this.getAge()>=65 && this.getAge()<=74){this.setMSIProbability(PAContext.probMSI.get(3).get(repReadData));}
			else if (this.getAge()>=75){this.setMSIProbability(PAContext.probMSI.get(4).get(repReadData));}
			
		}
		
		public void eventCheck(){

			double temp = 0;
			double tempQ = PAContext.myGenerator.nextDouble();
			//double tempCVD = PAContext.myGenerator.nextDouble();
			for (Iterator i = PAContext.replicationsData.getDataList().iterator();i.hasNext();){ 
				Record rec = (Record) i.next();
				if(rec.getType().equalsIgnoreCase(DataType.EVENT.toString().trim())){
					if(!this.isDead && rec.getName().equalsIgnoreCase(Events.DEPRESSION.toString().trim())){
						temp = PAContext.myGenerator.nextDouble();
				
						if (temp < this.paAdjustedDprsProbability) {
							//get depression
							
							Util.getEvent(depressionEventsList, rec, this);
							
						}
						
					}
					
					if(!this.isDead && rec.getName().equalsIgnoreCase(Events.MSI.toString().trim())){
						temp = PAContext.myGenerator.nextDouble(); 
						
						if (temp < this.paAdjustedMSIProbability){
							rec.getInModelID();
							//get msi
					
							Util.getEvent(msiEventsList, rec, this);
							
							//do not check fatality
						}
						
					}
					
					if(!this.isDead && rec.getName().equalsIgnoreCase(Events.T2D.toString().trim())){
						if(!this.hasT2D() && !this.hadT1D()){
							temp = PAContext.myGenerator.nextDouble();
						
							if(temp<this.paAdjustedDiabetesRisk){
								rec.getInModelID();
								//get T2D
								
								Util.getEvent(t2dEventsList, rec, this); 
								
								//do not check fatality
							}
						}
					}
					
					if (!this.isDead && (rec.getName().equalsIgnoreCase(Events.STROKE.toString().trim())) || (rec.getName().equalsIgnoreCase(Events.CHD.toString().trim()))){
								
						if(tempQ<=this.paAdjustedStrokeRisk){
							
							if ((!this.isDead) && (rec.getName().equalsIgnoreCase(Events.STROKE.toString().trim()))){
								rec.getInModelID();
								//get stroke
								
								Util.getEvent(strokeEventsList, rec, this);
								
								double temp1 = PAContext.myGenerator.nextDouble();
								if (temp1 < Util.fatalityStroke(this)){
									this.setPAStatus(rec.getEventAcutePA(repReadData));
						//			this.die("Death from stroke");
								}
							}
						}
						else if (tempQ>this.paAdjustedStrokeRisk && tempQ<=(this.paAdjustedStrokeRisk+this.paAdjustedCHDRisk)){
							if ((!this.isDead) && (rec.getName().equalsIgnoreCase(Events.CHD.toString().trim()))){
								rec.getInModelID();
								//get CHD
								
								Util.getEvent(chdEventsList, rec, this);
								
								double temp1 = PAContext.myGenerator.nextDouble();
								if (temp1 < Util.fatalityCHD(this)){
									this.setPAStatus(rec.getEventAcutePA(repReadData));
						//			this.die("Death from CHD");
								}
							}
						}						
					}
					
				}
			}
			
		}
		
		// Covid methods //
		
		public void getCovidData() {
			setTransmissionProb(PAContext.covidCharacteristics.get(0).get(repReadData));
			setnContacts(PAContext.covidCharacteristics.get(1).get(repReadData));
			setIntroducedInfections(PAContext.covidCharacteristics.get(2).get(repReadData));
			setExposurePeriodMin(PAContext.covidCharacteristics.get(3).get(repReadData));
			setExposurePeriodMax(PAContext.covidCharacteristics.get(4).get(repReadData));
			setExposurePeriodMode(PAContext.covidCharacteristics.get(5).get(repReadData));
			setInfectiousPeriod(PAContext.covidCharacteristics.get(6).get(repReadData));
			setInfectiousToHospital(PAContext.covidCharacteristics.get(7).get(repReadData));
			setLoSHospitalMean(PAContext.covidCharacteristics.get(8).get(repReadData));
			setLoSHospitalSD(PAContext.covidCharacteristics.get(9).get(repReadData));
			setLoSIcuMean(PAContext.covidCharacteristics.get(10).get(repReadData));
			setLoSIcuSD(PAContext.covidCharacteristics.get(11).get(repReadData));
			setICUProb(PAContext.covidCharacteristics.get(12).get(repReadData));
			setHospDeathProb(PAContext.covidCharacteristics.get(13).get(repReadData));
			setICUDeathProb(PAContext.covidCharacteristics.get(14).get(repReadData));
			setTimeToReinfectionMin(PAContext.covidCharacteristics.get(15).get(repReadData));
			setTimeToReinfectionMax(PAContext.covidCharacteristics.get(16).get(repReadData));
			setLongTermEffectsProb(PAContext.covidCharacteristics.get(17).get(repReadData));
			setMinLongTermEffectsDuration(PAContext.covidCharacteristics.get(18).get(repReadData));
			setMaxLongTermEffectsDuration(PAContext.covidCharacteristics.get(19).get(repReadData));
			
			/// gamma distribution parameters for LOS 
			this.shapeHosp = Math.pow((hosLoSmean/hosLoSsd), 2);
			this.scaleHosp = Math.pow(hosLoSsd, 2) / hosLoSmean;
			this.shapeICU = Math.pow((icuLoSmean/icuLoSsd), 2);
			this.scaleICU =  Math.pow(icuLoSsd, 2) / icuLoSmean;
		}
		
		public void getCovidInterventionData() {
			setNContactsLockdown(PAContext.covidInterventions.get(0).get(repReadData));
			setLockdownAdherenceProb(PAContext.covidInterventions.get(1).get(repReadData));
			setVaccineUptakeProb(PAContext.covidInterventions.get(2).get(repReadData));
			setVaccineImmunity(PAContext.covidInterventions.get(3).get(repReadData));
			setVaccineImmunityLag(PAContext.covidInterventions.get(4).get(repReadData));
			setMaxVaccineImmunityTime(PAContext.covidInterventions.get(5).get(repReadData));
			setMinVaccineImmunityTime(PAContext.covidInterventions.get(6).get(repReadData));
			setLockdownScenario(PAContext.covidInterventions.get(7).get(repReadData));
			setStartDateLockdown(PAContext.covidInterventions.get(8).get(repReadData));
			setEndDateLockdown(PAContext.covidInterventions.get(9).get(repReadData));
			setStartDateVaccine(PAContext.covidInterventions.get(10).get(repReadData));
			setEndDateVaccine(PAContext.covidInterventions.get(11).get(repReadData));
		//	setIntroAgeMinCovidIntervention(PAContext.covidInterventions.get(12).get(repReadData)); // moved to interface
		//	setIntroAgeMaxCovidIntervention(PAContext.covidInterventions.get(13).get(repReadData)); // moved to interface
			setPropVaccinatedPerDay(PAContext.covidInterventions.get(14).get(repReadData));
			setBoosterTime(PAContext.covidInterventions.get(15).get(repReadData));
		}
		
		public void getCovidCostData() {
			setLockdownCostPerCapita(PAContext.covidCosts.get(0).get(repReadData));
			setVaccineCostPerCapita(PAContext.covidCosts.get(1).get(repReadData));
			setHospitalCost(PAContext.covidCosts.get(2).get(repReadData));
			setICUCost(PAContext.covidCosts.get(3).get(repReadData));
			setLongCovidCost(PAContext.covidCosts.get(4).get(repReadData));
		}
		
		public void updateLockdownScenario() {
			// no lockdown //
			if (PAContext.covidIntervention != 1 && PAContext.covidIntervention != 3) {
			   	   this.myContacts = nContacts;
				   this.myLockdownCost = 0;
				   this.totalLockdownCost = 0;
			    }
			
			// LOCKDOWN SCENARIOS NEED TO BE CHANGED VIA INPUT FILE: 'INTERVENTION DATA COVID'.
			// lockdownScenario 1 = single lockdown scenario 
			// lockdownScenario 2 = periodic lockdown
			// lockdownScenario 3 = validation scenario (need to change start and end date in csv file for this scenario)
			// lockdown scenario for 3 month lockdown and extended lockdown (controlled by changing the end date) //
			if ((PAContext.covidIntervention == 1 || PAContext.covidIntervention == 3) && lockdownScenario == 1) {
				if (this.getTicks() >= startDateLockdown && this.getTicks() <= endDateLockdown &&
				    this.getAge() >= PAContext.introAgeMinCovidIntervention && this.getAge() <= PAContext.introAgeMaxCovidIntervention) {
					
				    this.myContacts = this.nContactsLockdown * lockdownAdherence;
				    this.totalLockdownCost = this.totalLockdownCost + lockdownCost;
				   	this.myLockdownCost = lockdownCost;
				     }
				    else {
				   	   this.myContacts = this.nContacts;
					   this.myLockdownCost = 0;
				    }
				}
			
			// lockdown scenario for periodic scenario
			
			hospitalThreshold = 0.00027; 
			if (PAContext.proportionHospital >= hospitalThreshold) { 
                beginLockdown = 1;
				}
			
			
			if ((PAContext.covidIntervention == 1 || PAContext.covidIntervention == 3) && lockdownScenario == 2) {
				if (this.getTicks() >= startDateLockdown && this.getTicks() <= endDateLockdown &&
				    this.getAge() >= PAContext.introAgeMinCovidIntervention && this.getAge() <= PAContext.introAgeMaxCovidIntervention) {						
						
                     if (beginLockdown == 1 && periodicLockdownTimestep <= 90) {  
						
						periodicLockdownTimestep ++;
						
					    this.myContacts = this.nContactsLockdown * lockdownAdherence;
					    this.totalLockdownCost = this.totalLockdownCost + lockdownCost;
					   	this.myLockdownCost = lockdownCost;
					     }
					    else {
					   	   this.myContacts = this.nContacts;
						   this.myLockdownCost = 0;
					    }
					if (periodicLockdownTimestep > 90) {
						periodicLockdownTimestep = 0;
						beginLockdown = 0;
					  }		
					}
			    else {
				   	   this.myContacts = this.nContacts;
					   this.myLockdownCost = 0;
				}
			}
			
			// lockdown scenario for validation //
			if ((PAContext.covidIntervention == 1 || PAContext.covidIntervention == 3) && lockdownScenario == 3) {
				if (this.getTicks() >= startDateLockdown && this.getTicks() <= endDateLockdown &&
				    this.getAge() >= PAContext.introAgeMinCovidIntervention && this.getAge() <= PAContext.introAgeMaxCovidIntervention) {
					
        			// make array list with timestep to update nContacts during lockdown
					// first value in array needs to be the same date lockdown starts 
					int[] ticksArray = {53,60,67,74,80,110,117,124,131,135,144,156,159,
							166,176,181,190,198,204,210,216,224,228,236,243,250,256,
							268,271,278,285,291,297,309,312,319,328,333,338, 346, 354, 361};				
					int i;
					for (i = 0; i < ticksArray.length; i++) {
						if (ticksArray[i] == getTicks()) {
							this.nContactsTickCounter ++;
							break;
						}
					}
					// 0 added at the start of the array (which won't be read) as tickCounter starts at 1
					// i.e. 53 ticks should match with 2.94 in age4Array
					double[] age4Array = {0, 2.94,2.92,2.74,2.59,2.43,3.11,3.06,3.37,3.32,3.41,3.71,3.31,2.87,2.87,
							2.96,3.32,4.64,4.82,4.89,5.3,6.41,7.9,8.44,7.97,7.6,7.66,6.44,4.86,5.73,6.75,6.33,
							6.16,6.88,6.93,6.31,5.2,3.32,3.26,3.75, 5.26,5.21,4.35};
					double[] age4to17Array = {0, 2.94,2.92,2.74,2.59,2.43,3.23,3.4,3.49,3.63,3.68,3.77,4.07,4.01,3.8,
							2.96,3.54,4.07,4.75,4.56,4.58,9.63,15.11,14.57,12.85,12.23,10.45,9.34,10.45,9.34,6.97,
							6.33,10.06,11.33,10.58,10.08,11.2,11.08,7.08,3.61,3.11,3.31, 3.38,3.07,3.56};
					double[] age18to69Array = {0, 2.94,2.92,2.74,2.59,2.43,3.08,3.22,3.06,2.88,2.78,2.97,3.12,3.09,2.89,
							2.96,3.28,4.79,4.93,4.62,4.24,4.23,4.39,4.53,4.34,4.08,3.72,3.52,3.56,3.59,3.44,2.81,2.79,
							2.9,3.47,3.83,3.43,2.75,2.67,2.8, 2.6,2.54,2.4};
					double[] age70PlusArray = {0, 2,2.01,1.97,2.06,2.06,1.89,1.93,1.86,1.95,1.95,2.11,2.09,2.04,2.01,
							2.02,2.37,2.83,3.07,3.02,3.08,3.09,2.71,2.75,2.85,2.59,2.46,2.52,2.43,2.32,2.29,2.05,1.84,
							1.92,2.23,2.46,2.44,2.2,1.92,1.81,1.68,1.61,1.63};
					
					if (getAge() <= 4) {this.nContactsLockdownValidation = age4Array[this.nContactsTickCounter];}
					if (getAge() > 4 && getAge() <= 17){this.nContactsLockdownValidation = age4to17Array[this.nContactsTickCounter];}
					if (getAge() > 17 && getAge() <= 69){this.nContactsLockdownValidation = age18to69Array[this.nContactsTickCounter];}
					if (getAge() >= 70){this.nContactsLockdownValidation = age70PlusArray[this.nContactsTickCounter];}
														
				    this.myContacts = this.nContactsLockdownValidation * lockdownAdherence;
				    this.totalLockdownCost = this.totalLockdownCost + lockdownCost;
				   	this.myLockdownCost = lockdownCost;
				     }
				    else {
				   	   this.myContacts = this.nContacts;
					   this.myLockdownCost = 0;
				    }
				}
		}

		public void introducedInfections() {
			// every 30 days add in X infected individuals - this is required for reinfection to occur
			introducedInfectionTimeStep ++;
			if (introducedInfectionTimeStep == 30) {
				if (introducedInfected > Math.random()) {
					this.exposed = 1;
					this.susceptible = 0;
					this.recovered = 0;
					this.timeSinceLastInfection = 0;
					this.timesInfected ++;
					this.timeToReinfection = 360;////= minTimeToReInfection + (int)(Math.random() * ((maxTimeToReInfection - minTimeToReInfection) + 1));
				}
				introducedInfectionTimeStep = 0;
			}
		}

		public void calculateInfectionProbability() {	
			// Infection probability is calculated as:
			// Transmission Prob (probability of infection per exposure) * N exposures 
			// N exposures = contacts per day * proportion of infectious agents
			this.exposuresPerDay = PAContext.propInfectiousAgents * this.myContacts;

			if (this.susceptible == 1) 
			   {this.infectionProb = (transmissionProb * this.exposuresPerDay);}
		    	else {this.infectionProb = 0.0;}
		  }

		public void infectAgents() {

			if (this.infectionProb > Math.random()) {
				this.exposed = 1;
				this.susceptible = 0;
				this.recovered = 0;
				this.timeSinceLastInfection = 0;
				this.timesInfected ++;
				this.timeToReinfection = minTimeToReInfection + (int)(Math.random() * ((maxTimeToReInfection - minTimeToReInfection) + 1));
			}
			if (this.exposed == 1 || this.infected == 1) {
				this.daysSinceExposure ++;
			}		
			if (this.infected == 1) {        
	 		this.daysSinceInfected ++;
			}
		  }

		public void calculateExposurePeriod() {
			if (this.exposed == 1 && this.daysSinceExposure <= 1) {
				this.exposurePeriod = (int) Math.round(this.triangularDist_Exp());
			}
		}

		public double triangularDist_Exp() {
			// a = min, b = max, c = mode
			
			double F = (exposure_c-exposure_a)/(exposure_b-exposure_a);
			double rand = Math.random();
			if (rand < F) {
				return exposure_a + Math.sqrt(rand * (exposure_b - exposure_a) * (exposure_c - exposure_a));
			} 
			else {
				return exposure_b - Math.sqrt((1 - rand) * (exposure_b - exposure_a) * (exposure_b - exposure_c));
			}
		}

		public void checkVaccination() {
			//TO DO: STRATIFY VACCINATIONS BY RISK CATEGORY
			
		if (PAContext.covidIntervention == 2 || PAContext.covidIntervention == 3) {
			
			double unvaccinatedAgentsWithUptake = PAContext.unvaccinatedCount * vaccineUptakeProb;
			double propVaccinatedPerDay_adjusted = (PAContext.agentCount / unvaccinatedAgentsWithUptake) * propVaccinatedPerDay;
			// adjusted propVaccinatedPerDay to account only for unvaccinated agents who decide to uptake vaccination.

			if (PAContext.vaccineStratified == 1) {
				if (this.clinicallyVulnerable == 1) 
				{ this.vaccinateMe = 1; }
				else { this.vaccinateMe = 0; }
			}
			else {this.vaccinateMe = 1;}
			
				if (this.getTicks() >= startDateVaccine && this.getTicks() < endDateVaccine && this.getAge() >= PAContext.introAgeMinCovidIntervention 
				&& this.getAge() <= PAContext.introAgeMaxCovidIntervention && this.vaccinateMe == 1) {
				if (this.vaccinated == 0 && this.vaccineUptakeDecision < 1 &&  // code ensures they only decide once if they will take the vaccine.
					Math.random() < propVaccinatedPerDay_adjusted) { // spreads out vaccination administration over time
					if (this.vaccineUptakeProb > Math.random()) {
						this.vaccinated = 1;
						this.vaccineUptakeDecision ++;
						this.totalVaccineCost = this.totalVaccineCost + vaccineCost;
						this.myVaccineCost = vaccineCost;
						this.timesVaccinated ++;
						// set how long immunity lasts
						this.myVaccinationImmunityTime = minVaccineImmunityTime + (int)(Math.random() * ((maxVaccineImmunityTime - minVaccineImmunityTime) + 1));
					}
				else {
						this.vaccineUptakeDecision ++;
					}	
				  }
				
				// re-vaccinate if due booster (only agents who did not reject vaccine the first time)
			    // does not affect vaccination status but just adds a cost and adjusts immunity.
			    if (this.daysSinceVaccine > boosterTime) { 
				     this.daysSinceVaccine = 0;
				     this.totalVaccineCost = this.totalVaccineCost + vaccineCost;
				     this.myVaccineCost = vaccineCost;
				     this.booster = 1;
				     
			      }
				} 
				
				// add in vaccine immunity lag time before immunity kicks in.
				// if booster occurs before immunity the agents initial immunity runs out there is no additional time lag
				if ((this.daysSinceVaccine > vaccineImmunityLag) || 
						(this.booster == 1 && boosterTime < this.myVaccinationImmunityTime)) {
					this.vaccineRiskAdjust = vaccineImmunity;
				}
				else {this.vaccineRiskAdjust = 0;}
				
			    
			    if (this.vaccinated == 1) {
			    	this.daysSinceVaccine ++;
			    	// adjust risk if vaccine no longer effective
				    if (this.daysSinceVaccine > this.myVaccinationImmunityTime) {
					   this.vaccineRiskAdjust = 0;	
				     }
			      }
				if (this.daysSinceVaccine > 1) {
					this.myVaccineCost = 0; // ensures vaccine cost only charged on day of vaccination
				 }
		     }
		}
		
		public void calculateCRisk() {			
			if (this.exposed == 1 && this.daysSinceExposure <= 1) {
			this.cRisk = Util.calculateCRisk(this, repReadData) * (1 - this.vaccineRiskAdjust); 
	     	}
		}

		public void updateSeverity() {
			// calculate how many are mild (1), severe (2, hospitalised) or critical (3, ICU or dead)	
			// agents are assigned a severity and go to hospital/ICU once the infected period starts. 
			if (this.infected == 1 && this.severity == 0) {
				if (this.cRisk < Math.random()) {
				this.severity = 1;
			}
			else {
				this.severity = 2;		
			}
				
		    if (this.severity == 2) {
		    	if (probICU < Math.random()) {
		    		this.severity = 2;  		
		    	}
		    	else {
					 this.severity = 3;
					 if (this.getBMI() >= 40)
					 {this.icuMorbidlyObese = 1;}
					 if (this.getBMI() >= 30 && this.getBMI() < 40)
					 {this.icuObese = 1;}
					 if (this.getBMI() >= 25 && this.getBMI() < 30)
					 {this.icuOverweight = 1;}		
					 if (this.getBMI() < 25) 
					 {this.icuNormalWeight = 1;}
		    	}
			}
		  }
		}

		public void updateHospitalDuration() {
		// hospital duration based on gamma distribution //	
			if (this.severity == 2 && this.hospitalised == 0 && this.daysSinceInfected >= infectiousToHospital) {
			this.lengthOfStay = this.gammaDistributionHosp();
			this.hospitalised = 1; 
			this.beenHospitalised = 1;
			this.timesHospitalised ++;
			}
		}

		public double gammaDistributionHosp() {
			this.gammaHosp = new GammaDistribution(shapeHosp, scaleHosp).sample();
			return gammaHosp;
		}

		public void calculateHospitalCost() {		
			this.myHospitalCost = dailyHospitalCost;
			
		}

		public void updateICUDuration() {
			if (this.severity == 3 && this.icu == 0 && this.daysSinceInfected >= infectiousToHospital) {
			this.lengthOfStayICU = this.gammaDistributionICU();
			this.icu = 1;
			this.timesICU ++;
			}
		}

		public double gammaDistributionICU() {	
			this.gammaICU = new GammaDistribution(shapeICU, scaleICU).sample();
			return gammaICU;
		}

		public void calculateICUCost() {
			this.myICUCost = dailyICUCost;
		}
		
		public void calculateTotalHospitalCost() {
			// cumulative agent cost
			if (this.hospitalised == 1) {
				this.totalHospitalCost = this.totalHospitalCost + myHospitalCost;
			}
			if (this.icu == 1) {
				this.totalICUCost = this.totalICUCost + myICUCost;
			}
		}

		public void checkCovidDeath() {
			// covid deaths are calculated daily, whereas background deaths are every 3 months.
			// probability of death if hospitalised, a set % of cases in hospital/ICU die 
			// convert overall probability of dying to daily prob by considering hospital/ICU duration.
			// 1 - (1-x)^(1/y)
			if (this.icu == 1) {
			   this.dailyDeathProb = 1 - (Math.pow((double)1-this.ICUDeathProb, (double)1/this.lengthOfStayICU)); 
			}
			else {
				this.dailyDeathProb = 0;
		}	   
				if(this.dailyDeathProb > Math.random()) {
					
					// establish comorbidites of fatality
					if (this.hadT1D() || this.hasT2D())
					{this.covidDeathDiabetes = 1;}
					if (this.hasCVD())
					{this.covidDeathCVD = 1;}
					if (this.getHypertension() == 1)
					{this.covidDeathHypertension = 1;}
					if (this.getGender().equals("M"))
					{this.covidDeathMale = 1;}
					if (this.bmi >= 30)
					{covidDeathObese = 1; }
					
					this.die("Death from Covid");
					// reset variables
					this.infected = 0;
					this.exposed = 0;
					this.severity = 0;
					this.icu = 0;
				    this.myICUCost = 0;
				    this.recovered = 0;
				}
				
				if (this.hospitalised == 1) {
					   this.dailyDeathProb = 1 - (Math.pow((double)1-this.hospDeathProb, (double)1/this.lengthOfStay)); 
					}
					else {
						this.dailyDeathProb = 0;
				}	   
						if(this.dailyDeathProb > Math.random()) {
						
							if (this.hadT1D() || this.hasT2D())
							{this.covidDeathDiabetes = 1;}
							if (this.hasCVD())
							{this.covidDeathCVD = 1;}
							if (this.getHypertension() == 1)
							{this.covidDeathHypertension = 1;}
							if (this.getGender().equals("M"))
							{this.covidDeathMale = 1;}
							if (this.bmi >= 30)
								{covidDeathObese = 1; }

							this.die("Death from Covid");
							// reset variables
							this.infected = 0;
							this.exposed = 0;
							this.severity = 0;
							this.hospitalised = 0;
						    this.myHospitalCost = 0;
						    this.recovered = 0;
						}
			}

		public void updateInfectionStatus() {	
			// update infection status 
			if (this.daysSinceExposure > this.exposurePeriod) {
				this.exposed = 0;
				this.infected = 1;
				this.beenInfected = 1;
				this.daysSinceInfected ++;
			}
			// set recovery for mild infected agents, severe and icu agents are recovered when they leave hospital.
			if (this.severity == 1) {
				if (this.daysSinceExposure > this.exposurePeriod + this.infectiousPeriod) {
					this.infected = 0;
					this.recovered = 1;
					this.daysSinceExposure = 0;
					this.cRisk = 0;
					this.severity = 0;
			}
		  }
			if (this.hospitalised == 1) {
				// agents are recovered when they are discharged from hospital/ICU.
				if (this.daysSinceExposure > this.exposurePeriod + infectiousToHospital + this.lengthOfStay) {
					this.hospitalised = 0;
					this.myHospitalCost = 0;
					this.lengthOfStay = 0;
					this.recovered = 1;
					this.daysSinceExposure = 0;
					this.infected = 0;
					this.severity = 0;
					this.cRisk = 0;
				}
			}
			
			if (this.icu == 1) {
				if (this.daysSinceExposure > this.exposurePeriod + infectiousToHospital + this.lengthOfStayICU) {
					this.icu = 0;
					this.myICUCost = 0;
					this.lengthOfStayICU = 0;
					this.recovered = 1;
					this.daysSinceExposure = 0;
					this.infected = 0;
					this.severity = 0;
					this.cRisk = 0;
				}
			}
			if (recovered == 1) {
				this.timeSinceLastInfection ++;
			}
		}

		public void checkLongTermEffects() {
		// calculate long covid probability, duration and economic costs //
		// TO DO: If data is available, stratify long covid by risk.
			if (this.recovered == 1 && timeSinceLastInfection == 1) {
				if (this.longTermEffectsProb > Math.random()) {
					this.longTermEffects = 1;
					this.timesLongCovid ++;
					this.myLongCovidTime = minLongCovidTime + (int)(Math.random() * ((maxLongCovidTime - minLongCovidTime) + 1));
				}
			}
			
			if (this.longTermEffects == 1) {
				this.daysOfLongTermEffects ++;
				this.myLongCovidCost = longCovidCost;
			}
			else { this.myLongCovidCost = 0; }
			
			if (this.daysOfLongTermEffects > this.myLongCovidTime) {
				this.longTermEffects = 0;
				this.daysOfLongTermEffects = 0;
			}
		}
		
		public void calculateTotalLongCovidCost() {
			if (this.longTermEffects == 1) {
				this.totalLongCovidCost = this.totalLongCovidCost + longCovidCost;
			}
		}

		public void updateSusceptibility() {
			if (this.susceptible == 0 && this.timeSinceLastInfection >= this.timeToReinfection) {
				this.susceptible = 1;
			}
		}
		
		public void calculateTotalCovidCost() {
			this.totalHealthCostCovid = this.totalHospitalCost + this.totalICUCost + this.totalLongCovidCost;
			this.totalInterventionCostCovid = this.totalLockdownCost + this.totalVaccineCost;
			this.totalCovidCost = this.totalInterventionCostCovid + this.totalHealthCostCovid;
		}
		
		public void updateAge(){
			this.age += Util.CONVERT_YEARS;
			Util.updateAgeRelatedCharacteristics(this);
			this.lifeYears += Util.CONVERT_YEARS;
			this.updateYearsActive();
		} 
				
		public void updateYearsActive(){
			if (this.paStatus>=Util.PA_ACTIVE_THRESHOLD){
				this.yearsActive+=Util.CONVERT_YEARS;
			}
		}
		
		public void checkDeath(){
			double temp = PAContext.myGenerator.nextDouble();
			int ageRoundDown = (int) Math.floor(this.getAge());
			double rr = 0;
		
			if(ageRoundDown>100){this.die("Death over 100");}
			else if(ageRoundDown<=100){
				
					if(this.hasCHD()){ // CHD mortality
						
						if(this.getGender().equals("M")){
							rr = PAContext.mortality3monthsMaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(0).get(repReadData);
							if(temp<rr){this.die("CHD death from other causes");} // CHD death from other causes
						}
						else{
							rr = PAContext.mortality3monthsFemaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(0).get(repReadData);
							if(temp<rr){this.die("CHD death from other causes");} //CHD death from other causes
						}
					}
					else if(this.hadT1D() || this.hasT2D()){ // Diabetes mortality
						
						if(this.getGender().equals("M")){
							rr = PAContext.mortality3monthsMaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(1).get(repReadData);
							if(temp<rr){this.die("Diabetes death from other causes");} // Diabetes death from other causes
						}
						else{
							rr = PAContext.mortality3monthsFemaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(1).get(repReadData);
							if(temp<rr){this.die("Diabetes death from other causes");} //Diabetes death from other causes
						}
					}
					else if(this.hasDepression()){ // Depression mortality
						if(this.getGender().equals("M")){
							rr = PAContext.mortality3monthsMaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(2).get(repReadData);
							if(temp<rr){this.die("Depression death from other causes");} //Depression death from other causes
						}
						else{
							rr = PAContext.mortality3monthsFemaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(2).get(repReadData);
							if(temp<rr){this.die("Depression death from other causes");} //Depression death from other causes
						}
					}
					else if(this.hasMSI()){ // MSI mortality
						if(this.getGender().equals("M")){
							rr = PAContext.mortality3monthsMaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(3).get(repReadData);
							if(temp<rr){this.die("MSI death from other causes");} // MSI death from other causes							
						}
						else{
							rr = PAContext.mortality3monthsFemaleHealthyNonCVD[ageRoundDown]*PAContext.rrMortality.get(3).get(repReadData);
							if(temp<rr){this.die("MSI death from other causes");} //MSI death from other causes
						}
					}
					else{ // Healthy mortality
						if(this.getGender().equals("M")){
							rr = PAContext.mortality3monthsMaleHealthyNonCVD[ageRoundDown];
							if(temp<rr){this.die("Death from other causes");}
						}
						else{
							rr = PAContext.mortality3monthsFemaleHealthyNonCVD[ageRoundDown];
							if(temp<rr){this.die("Death from other causes");}
						}
					}
				
			}
	
			
		}

		
		public void updateTotalEventCost(double eventCost){
			this.totalEventCost += eventCost;
		}
		
		public void die(String causeOfDeath){
			this.isDead = true;
			this.ageAtDeath = this.age;
			this.paStatusAtDeath = this.getPAStatus();
			this.causeOfDeath = causeOfDeath;
			//
			
			PAContext.deadCounter++;
		
			if (PAContext.deadCounter>=PAContext.cohortSize){
				PAContext pa = new PAContext();
				PAContext.schedule.schedule(ScheduleParameters.createOneTime(Util.getCurrentTime(), ScheduleParameters.LAST_PRIORITY), pa, "end");
			}	
		}
	
	
		//////START seed data
		public int getID(){
			return this.id;
		}
		
		public void setID(int id){
			this.id = id;
		}
		
		public void setInModelID(int inModelID){
			this.inModelID = inModelID;
		}
		
		public int getInModelID(){
			return this.inModelID;
		}
		
		public double getPAStatus() {
			return this.paStatus;
		}
		
		public void setPAStatus(double paStatus){
			this.paStatus = paStatus;
		}
		
		public void setAge(double age){
			this.age = age; 
		}
		
		public String getGender(){
			return this.gender;
		}
		
		public void setGender(String gender){
			this.gender = gender;
		}
		
		//history CVD from health survey for England
		public int getHadCVD(){
			return this.had_CVD;
		}
		
		public void setHadCVD(int had_CVD){
			this.had_CVD = had_CVD;
		}
		
		//event during simulation
		public boolean hasCHD(){
			if(this.noOfCHD != 0){return true;}
			else{return false;}
		}
		
		public boolean hasStroke(){
			if(this.noOfStroke != 0){return true;}
			else{return false;}
		}
		
		// both history and simulation 
		public boolean hasCVD(){
			if (this.getHadCVD() == 1 || this.hasCHD() || this.hasStroke()) {return true;}
			else {return false;}
		}
		
		public int getHadT2D(){
			return this.had_T2D;
		}
		
		
		public void setHadT2D(int had_T2D){
			this.had_T2D = had_T2D;
		}
		
		public boolean hasT2D(){
			if(this.getHadT2D()==1 || this.noOfT2D != 0){return true;}
			else {return false;}
		}
		
		public int getHadDepression(){
			return this.had_depression;
		}
		
		public void setHadDepression(int had_depression){
			this.had_depression = had_depression;
		}
		
		public boolean hasDepression(){
			if(this.getHadDepression()==1 && this.noOfDprsEpisodes != 0){return true;}
			else {return false;}
		}
		
		public int getHadMSI(){
			return this.had_MSI;
		}
		
		public void setHadMSI(int had_MSI){
			this.had_MSI = had_MSI;
		}
		
		public boolean hasMSI(){
			if(this.getHadMSI()==1 && this.noOfMSI != 0){return true;}
			else {return false;}
		}
		
		public double getBMI(){
			return this.bmi;
		}
		
		public void setBMI(double bmi){
			this.bmi = bmi;
		}
		
		public double getHDLRatio(){
			return this.hdlRatio;
		}
		
		public void setHDLRatio(double hdlRatio){
			this.hdlRatio = hdlRatio;
		}
		
		public double getSBloodPressure(){
			return this.sBloodPressure;
		}
		
		public void setSBloodPressure(double sBloodPressure){
			this.sBloodPressure = sBloodPressure;
		}
		
		public double getDBloodPressure(){
			return this.dBloodPressure;
		}
		
		public void setDBloodPressure(double dBloodPressure){
			this.dBloodPressure = dBloodPressure;
		}		
		
		public int getSmoking(){
			return this.smoking;
		}
		
		public void setSmoking(int smoking){
			this.smoking = smoking;
		}
		
		public boolean isSmoker(){
			if(this.getSmoking()==0 || this.getSmoking()==1){return false;}
			else{return true;}
		}
		
		public String getEthnicity(){
			return this.ethnicity;
		}
		
		public void setEthnicity(String ethnicity){
			this.ethnicity = ethnicity;
		}
		
		public int getFamilyCVDHistory(){
			return this.familyCVDHistory;
		}
		
		public void setFamilyCVDHistory(int familyCVDHistory){
			this.familyCVDHistory = familyCVDHistory;
		}
		
		public boolean hasCVDFamilyHistory(){
			if(this.getFamilyCVDHistory()==1){return true;}
			else{return false;}
		}
		
		public int getEducation(){
			return this.education;
		}
		
		public void setEducation(int education){
			this.education = education;
		}
		
		public double getSampleWeight(){
			return this.sampleWeight;
		}
		
		public void setSampleWeight(double sampleWeight){
			this.sampleWeight = sampleWeight;
		}
		
		public double getDeprivationTown(){
			return this.deprivationTown;
		}
		
		public void setDeprivationTown(double deprivationTown){
			this.deprivationTown = deprivationTown;
		}

		public int getBpTreatment(){
			return this.bpTreatment;
		}
		
		public void setBpTreatment(int bpTreatment){
			this.bpTreatment = bpTreatment;
		}
		
		public boolean onBpTreatment(){
			if(this.getBpTreatment()==1){return true;}
			else{return false;}
		}
		
		public int getRheumatoidArthritis(){
			return this.rheumatoidArthritis;
		}
		
		public void setRheumatoidArthritis(int rheumatoidArthritis){
			this.rheumatoidArthritis = rheumatoidArthritis;
		}
		
		public boolean hasRheumatoidArthritis(){
			if(this.getRheumatoidArthritis()==1){return true;}
			else{return false;}
		}
		
		public int getChronicKidneyDisease(){
			return this.chronicKidneyDisease;
		}
		
		public void setCHronicKidneyDisease(int chronicKidneyDisease){
			this.chronicKidneyDisease = chronicKidneyDisease;
		}
		
		public boolean hasChronicKidneyDisease(){
			if(this.getChronicKidneyDisease()==1){return true;}
			else{return false;}
		}
		
		public int getAtrialFibrillation(){
			return this.atrialFibrillation;
		}
		
		public void setAtrialFibrillation(int atrialFibrillation){
			this.atrialFibrillation = atrialFibrillation;
		}
		
		public boolean hasAtrialFibrillation(){
			if(this.getAtrialFibrillation()==1){return true;}
			else{return false;}
		}
		
		public int getHadT1D(){
			return this.had_T1D;
		}
		
		public void setHadT1D(int had_T1D){
			this.had_T1D = had_T1D;
		}
		
		public boolean hadT1D(){
			if(this.getHadT1D()==1){return true;}
			else{return false;}
		}
		
		public int getCort(){
			return this.cort;
		}
		
		public void setCort(int cort){
			this.cort = cort;
		}
		
		public int getFamilyT2DHistory(){
			return this.familyT2DHistory;
		}
		
		public void setFamilyT2DHistory(int familyT2DHistory){
			this.familyT2DHistory = familyT2DHistory;
		}
		
		public boolean hasT2DFamilyHistory(){
			if(this.getFamilyT2DHistory()==1){return true;}
			else{return false;}
		}
		
		public double getUtilityIMPEQ5D(){
			return this.utilityImpeq5d;
		}
		
		public void setUtilityIMPEQ5D(double utilityImpeq5d){
			this.utilityImpeq5d = utilityImpeq5d;
		}
		
		public double getDeprivationIMD(){
			return this.deprivationIMD;
		}
		
		public void setDeprivationIMD(double deprivationIMD){
			this.deprivationIMD = deprivationIMD;
		}
		
		public double getSocioEconomicClass(){
			return this.socioEconomicClass;
		}
		
		public void setSocioEconomicClass(double sec){
			this.socioEconomicClass = sec;
		}
		
		public int getLongLastingIllness(){
			return this.longLastingIllness;
		}
		
		public void setLongLastingIllness(int lli){
			this.longLastingIllness = lli;
		}
		
		public int getComorbidity(){
			return this.comorbidity;
		}
		
		public void setComorbidity(int comorbidity){
			this.comorbidity = comorbidity;
		}
		
		
		//////END seed data
		
		public boolean getInAcutePeriod(){
			return this.inAcutePeriod;
		}
		
		public void setInAcutePeriod(boolean inAcutePeriod){
			this.inAcutePeriod = inAcutePeriod;
		}
		
		public boolean getIsDead(){
			return this.isDead;
		}
		
		public void setIsDead(boolean isDead){
			this.isDead = isDead;
		}
		
		public double getAgeAtEntry(){
			return this.ageAtEntry;
		}
		public void setAgeAtEntry(double ageAtEntry){
			this.ageAtEntry = ageAtEntry;
		}
		
		public double getAgeAtSimEnd(){
			return this.ageAtSimEnd;
		}
		public void setAgeAtSimEnd(double ageAtSimEnd){
			this.ageAtSimEnd = ageAtSimEnd;
		}
		
		public double getAgeAtDeath(){
			return this.ageAtDeath;
		}
		public void setAgeAtDeath(double ageAtDeath){
			this.ageAtDeath = ageAtDeath;
		}
		
		public double getLifeYears(){
			return this.lifeYears;
		}
		public void setLifeYears(double lifeYears){
			this.lifeYears = lifeYears;
		}
		
		public double getPAStatusAtEntry(){
			return this.paStatusAtEntry;
		}
		public void setPAStatusAtEntry(double paStatusAtEntry){
			this.paStatusAtEntry = paStatusAtEntry;
		}
		
		public double getPAStatusAtSimEnd(){
			return this.paStatusAtSimEnd;
		}
		public void setPAStatusAtSimEnd(double paStatusAtSimEnd){
			this.paStatusAtSimEnd = paStatusAtSimEnd;
		}
		
		public double getPAStatusAtDeath(){
			return this.paStatusAtDeath;
		}
		public void setPAStatusAtDeath(double paStatusAtDeath){
			this.paStatusAtDeath = paStatusAtDeath;
		}
		
		public double getBMIAtEntry(){
			return this.bmiAtEntry;
		}
		
		public void setBMIAtEntry(double bmiAtEntry){
			this.bmiAtEntry = bmiAtEntry;
		}
		public double getBMIAtEnd(){
			return this.bmiAtEnd;
		}
		
		public void setBMIAtEnd(double bmiAtEnd){
			this.bmiAtEnd = bmiAtEnd;
		}
		public double getHDLRatioAtEntry(){
			return this.hdlRatioAtEntry;
		}
		
		public void setHDLRatioAtEntry(double hdlRatioAtEntry){
			this.hdlRatioAtEntry = hdlRatioAtEntry;
		}
		public double getHDLRatioAtEnd(){
			return this.hdlRatioAtEnd;
		}
		
		public void setHDLRatioAtEnd(double hdlRatioAtEnd){
			this.hdlRatioAtEnd = hdlRatioAtEnd;
		}
		public double getSBPAtEntry(){
			return this.sbpAtEntry;
		}
		
		public void setSBPAtEntry(double sbpAtEntry){
			this.sbpAtEntry = sbpAtEntry;
		}
		public double getSBPAtEnd(){
			return this.sbpAtEnd;
		}
		
		public void setSBPAtEnd(double sbpAtEnd){
			this.sbpAtEnd = sbpAtEnd;
		}
		
		public double getYearsActive(){
			return this.yearsActive;
		}
		public void setYearsActive(double yearsActive){
			this.yearsActive = yearsActive;
		}
		
		public int getTotalNoOfEvents(){
			return this.totalNoOfEvents;
		}
		
		public void setTotalNoOfEvents(int totalNoOfEvents){
			this.totalNoOfEvents = totalNoOfEvents;
		}
		
		public int getTotalNoOfCVD(){
			return this.totalNoOfCVD;
		}
		
		public void setTotalNoOfCVD(int totalNoOfCVD){
			this.totalNoOfCVD = totalNoOfCVD;
		}
		
		public int getNoOfMSI(){
			return this.noOfMSI;
		}
		
		public void setNoOfMSI(int noOfMSI){
			this.noOfMSI = noOfMSI;
		}
		
		public int getNoOfDprsEpisodes(){
			return this.noOfDprsEpisodes;
		}
		
		public void setNoOfDprsEpisodes(int noOfDprsEpisodes){
			this.noOfDprsEpisodes = noOfDprsEpisodes;
		}
			
		public int getNoOfStroke(){
			return this.noOfStroke;
		}
		
		public void setNoOfStroke(int noOfStroke){
			this.noOfStroke = noOfStroke;
		}
		
		public int getNoOfCHD(){
			return this.noOfCHD;
		}
		
		public void setNoOfCHD(int noOfCHD){
			this.noOfCHD = noOfCHD;
		}
		
		public int getNoOfT2D(){
			return this.noOfT2D;
		}
		
		public void setNoOfT2D(int noOfT2D){
			this.noOfT2D = noOfT2D;
		}
		
		public double getTotalEventCost(){
			return this.totalEventCost;
		}
		
		public void setTotalEventCost(double totalEventCost){
			this.totalEventCost = totalEventCost;
		}
		
		public double getTotalEventCostDiscounted(){
			return this.totalEventCostDiscounted;
		}
		
		public void setTotalEventCostDiscounted(double totalEventCostDiscounted){
			this.totalEventCostDiscounted = totalEventCostDiscounted;
		}
		
		public double getPAPreviousPeriod(){
			return this.paPreviousPeriod;
		}
		public void setPAPreviousPeriod(double paPreviousPeriod){
			this.paPreviousPeriod = paPreviousPeriod;
		}
		
		public double getDprsProbability(){
			return this.dprsProbability;
		}
		public void setDprsProbability(double dprsProbability){
			this.dprsProbability = dprsProbability;
		}
		
		public double getMSIProbability(){
			return this.msiProbability;
		}
		public void setMSIProbability(double msiProbability){
			this.msiProbability = msiProbability;
		}	
		
		//CALMS
		
		public void setTransmissionProb(double transmissionProb){
			this.transmissionProb = transmissionProb;
		}
		public void setnContacts(double nContacts){
			this.nContacts = nContacts;
		}	
		public void setIntroducedInfections(double introducedInfections){
			this.introducedInfected = introducedInfections;
		}
		public void setExposurePeriodMin(double exposurePeriodMin){
			this.exposure_a = exposurePeriodMin;
		}
		public void setExposurePeriodMax(double exposurePeriodMax){
			this.exposure_b = exposurePeriodMax;
		}
		public void setExposurePeriodMode(double exposurePeriodMode){
			this.exposure_c = exposurePeriodMode;
		}
		public void setInfectiousPeriod(double infectiousPeriod){
			this.infectiousPeriod = (int)infectiousPeriod;
		}
		public void setInfectiousToHospital(double infectiousToHospital){
			this.infectiousToHospital = (int)infectiousToHospital;
		}
		public void setLoSHospitalMean(double losHospitalMean){
			this.hosLoSmean = losHospitalMean;
		}
		public void setLoSHospitalSD(double losHospitalSD){
			this.hosLoSsd = losHospitalSD;
		}
		public void setLoSIcuMean(double losICUMean){
			this.icuLoSmean = losICUMean;
		}
		public void setLoSIcuSD(double losICUSD){
			this.icuLoSsd = losICUSD;
		}
		public void setICUProb(double icuProb){
			this.probICU = icuProb;
		}
		public void setHospDeathProb(double hospDeathProb){
			this.hospDeathProb = hospDeathProb;
		}
		public void setICUDeathProb(double icuDeathProb){
			this.ICUDeathProb = icuDeathProb;
		}
		public void setTimeToReinfectionMin(double timeToReinfectionMin){
			this.minTimeToReInfection = (int) timeToReinfectionMin;
		}
		public void setTimeToReinfectionMax(double timeToReinfectionMax){
			this.maxTimeToReInfection = (int) timeToReinfectionMax;
		}
		public void setLongTermEffectsProb(double longTermEffectsProb){
			this.longTermEffectsProb = longTermEffectsProb;
		}
		public void setMinLongTermEffectsDuration(double longTermEffectsDurationMin){
			this.minLongCovidTime = (int) longTermEffectsDurationMin;
		}
		public void setMaxLongTermEffectsDuration(double longTermEffectsDurationMax){
			this.maxLongCovidTime = (int) longTermEffectsDurationMax;
		}	
		public void setNContactsLockdown(double nContactsLockdown) {
			this.nContactsLockdown = nContactsLockdown;
		}
		public void setLockdownAdherenceProb(double lockdownAdherenceProb) {
			this.lockdownAdherence = lockdownAdherenceProb;
		}
		public void setVaccineUptakeProb(double vaccineUptakeProb) {
			this.vaccineUptakeProb = vaccineUptakeProb;
		}
		public void setVaccineImmunity(double vaccineImmunity) {
			this.vaccineImmunity = vaccineImmunity;
		}
		public void setVaccineImmunityLag(double vaccineImmunityLag) {
			this.vaccineImmunityLag = (int) vaccineImmunityLag;
		}
		public void setMinVaccineImmunityTime(double vaccineImmunityTimeMin) {
			this.minVaccineImmunityTime = (int) vaccineImmunityTimeMin;
		}
		public void setMaxVaccineImmunityTime(double vaccineImmunityTimeMax) {
			this.maxVaccineImmunityTime = (int) vaccineImmunityTimeMax;
		}
		public void setLockdownScenario(double lockdownScenario) {
			this.lockdownScenario = (int) lockdownScenario;
		}
		public void setStartDateLockdown(double startDateLockdown) {
			this.startDateLockdown = (int) startDateLockdown;
		}
		public void setEndDateLockdown(double endDateLockdown) {
			this.endDateLockdown = (int) endDateLockdown;
		}
		public void setStartDateVaccine(double startDateVaccine) {
			this.startDateVaccine = (int) startDateVaccine;
		}
		public void setEndDateVaccine(double endDateVaccine) {
			this.endDateVaccine = (int) endDateVaccine;
		}
		public void setPropVaccinatedPerDay(double propVaccinatedPerDay) {
			this.propVaccinatedPerDay = propVaccinatedPerDay;
		}
		public void setBoosterTime(double boosterTime) {
			this.boosterTime = (int) boosterTime;
		}	
		public void setLockdownCostPerCapita(double lockdownCostPerCapita) {
			this.lockdownCost = lockdownCostPerCapita;
		}
		public void setVaccineCostPerCapita(double vaccineCostPerCapita) {
			this.vaccineCost = vaccineCostPerCapita;
		}
		public void setHospitalCost(double hospitalCost) {
			this.dailyHospitalCost = hospitalCost;
		}
		public void setICUCost(double icuCost) {
			this.dailyICUCost = icuCost;
		}
		public void setLongCovidCost(double longCovidCost) {
			this.longCovidCost = longCovidCost;
		}			
		public void setInfected(int infected) {
			this.infected = infected;
		}
		public void setExposed(int exposed) {
			this.exposed = exposed;
		}
		public void setSusceptible(int susceptible) {
			this.susceptible = susceptible;
		}
		public void setRecovered(int recovered) {
			this.recovered = recovered;
		}
		public void setLongTermEffects(int longTermEffects) {
			this.longTermEffects = longTermEffects;
		}
		public void setDaysSinceExposure (int daysSinceExposure) {
			this.daysSinceExposure = daysSinceExposure;
		}
		public void setDaysSinceInfected (int daysSinceInfected) {
			this.daysSinceInfected = daysSinceInfected;
		}
		public void setTimeSinceLastInfection (int timeSinceLastInfection) {
			this.timeSinceLastInfection = timeSinceLastInfection;
		}
		public void setSeverity (int severity) {
			this.severity = severity;
		}
		public void setCRisk(int cRisk) {
			this.cRisk = cRisk;
		}
		public void setHospitalised (int hospitalised) {
			this.hospitalised = hospitalised;
		}
		public void setTimesHospitalised (int timesHospitalised) {
			this.timesHospitalised = timesHospitalised;
		}
		public void setTimesLongCovid (int timesLongCovid) {
			this.timesLongCovid = timesLongCovid;
		}
		public void setMyHospitalCost (double myHospitalCost) {
			this.myHospitalCost = myHospitalCost;
		}
		public void setTotalHospitalCost (double totalHospitalCost) {
			this.totalHospitalCost = totalHospitalCost;
		}
		public void setICU (int ICU) {
			this.icu = ICU;
		}
		public void setTimesICU (int timesICU) {
			this.timesICU = timesICU;
		}
		public void setMyICUCost (double myICUCost) {
			this.myICUCost = myICUCost;
		}		
		public void setTotalICUCost (double totalICUCost) {
			this.totalICUCost = totalICUCost;
		}	
		public void setDiedOfCovid (int diedOfCovid) {
			this.diedOfCovid = diedOfCovid;
		}
		public void setCountDead (int countDead) {
			this.countDead = countDead;
		}
		public void setCovidDeathDiabetes (int covidDeathDiabetes) {
			this.covidDeathDiabetes = covidDeathDiabetes;
		}
		public void setCovidDeathCVD (int covidDeathCVD) {
			this.covidDeathCVD = covidDeathCVD;
		}
		public void setCovidDeathHypertension (int covidDeathHypertension) {
			this.covidDeathHypertension = covidDeathHypertension;
		}
		public void setCovidDeathMale (int covidDeathMale) {
			this.covidDeathMale = covidDeathMale;
		}
		public void setCovidDeathObese (int covidDeathObese) {
			this.covidDeathObese = covidDeathObese;
		}
		public void seticuMorbidlyobese (int icuMorbidlyObese) {
			this.icuMorbidlyObese = icuMorbidlyObese;
		}	
		public void setIcuObese (int icuObese) {
			this.icuObese = icuObese;
		}	
		public void setIcuOverweight (int icuOverweight) {
			this.icuOverweight = icuOverweight;
		}	
		public void setIcuNormalWeight (int icuNormalWeight) {
			this.icuNormalWeight = icuNormalWeight;
		}
		
		public void setMyLockdownCost (double myLockdownCost) {
			this.myLockdownCost = myLockdownCost;
		}
		public void setTotalLockdownCost (double totalLockdownCost) {
			this.totalLockdownCost = totalLockdownCost;
		}
		public void setTimesInfected (int timesInfected) {
			this.timesInfected = timesInfected;
		}
		public void setVaccineUptakeDecision (int vaccineUptakeDecision) {
			this.vaccineUptakeDecision = vaccineUptakeDecision;
		}
		public void setMyVaccineCost (double myVaccineCost) {
			this.myVaccineCost = myVaccineCost;
		}
		public void setTotalVaccineCost (double totalVaccineCost) {
			this.totalVaccineCost = totalVaccineCost;
		}
		public void setTimesVaccinated (int timesVaccinated) {
			this.timesVaccinated = timesVaccinated;
		}
		public void setDaysOfLongTermEffects (int daysOfLongTermEffects) {
			this.daysOfLongTermEffects = daysOfLongTermEffects;
		}
		public void setMyLongCovidCost (double myLongCovidCost) {
			this.myLongCovidCost = myLongCovidCost;
		}
		public void setTotalLongCovidCost (double totalLongCovidCost) {
			this.totalLongCovidCost = totalLongCovidCost;
		}
		public void setTicks (int ticks) {
			this.ticks = ticks;
		}
		public void setNContactsTickCounter (int nContactsTickCounter) {
			this.nContactsTickCounter = nContactsTickCounter;
		}
		public void setPeriodicLockdownTimestep (int setPeriodicLockdownTimestep) {
			this.periodicLockdownTimestep = setPeriodicLockdownTimestep;
		}
		public void setVaccinated (int setVaccinated) {
			this.vaccinated = setVaccinated;
		}
		public void setVaccineRiskAdjust (double setVaccineRiskAdjust ) {
			this.vaccineRiskAdjust = setVaccineRiskAdjust;
		}
		public void setThreeMonthTimeStepAdjust (int setThreeMonthTimeStepAdjust) {
			this.threeMonthTimeStepAdjust = setThreeMonthTimeStepAdjust;
		}
		public void setBeginLockdown (int setBeginLockdown) {
			this.beginLockdown = setBeginLockdown;
		}
		public void setClinicallyVulnerable (int setClinicallyVulnerable) {
			this.clinicallyVulnerable = setClinicallyVulnerable;
		}
		
		public String getCauseOfDeath(){
			return this.causeOfDeath;
		}
		
		public void setCauseOfDeath(String causeOfDeath){
			this.causeOfDeath = causeOfDeath;
		}

		public void clearCHDEventsList(){
			this.chdEventsList.clear();
		}
		
		public void clearStrokeEventsList(){
			this.strokeEventsList.clear();
		}
		
		public void clearT2DEventsList(){
			this.t2dEventsList.clear();
		}
		
		public void clearMSIEventsList(){
			this.msiEventsList.clear();
		}
		
		public void clearDepressionEventsList(){
			this.depressionEventsList.clear();
		}
		
		public ArrayList<OccuredEvent> getCHDEventsList(){
			return this.chdEventsList;
		}
		
		public ArrayList<OccuredEvent> getStrokeEventsList(){
			return this.strokeEventsList;
		}
		
		public ArrayList<OccuredEvent> getT2DEventsList(){
			return this.t2dEventsList;
		}

		public ArrayList<OccuredEvent> getMSIEventsList(){
			return this.msiEventsList;
		}

		public ArrayList<OccuredEvent> getDepressionEventsList(){
			return this.depressionEventsList;
		}
		
//		COVID OUTPUTS
		public double getInfectionProb() {	
			return this.infectionProb;
		}

		public double getExposuresPerDay() {
		return this.exposuresPerDay;
		}
		
		public double getTransmissionProb() {
			return this.transmissionProb;
		}

		public int getSusceptibleAgents() {
			return this.susceptible;
		}

		public int getExposedAgents() {
			return this.exposed;
		}

		public int getinfectedAgents() {
			return this.infected;
		}

		public int getRecoveredAgents() {
			return this.recovered;
		}

		public int getdaysSinceExposure() {
			return this.daysSinceExposure;
		}

		public double getRisk() {
			return this.cRisk;
		}

		public int getSeverity() {
			return this.severity;	
		}

		public int countMild() {
			if (this.severity == 0) {
				this.mild = 0;
			}
			if (this.severity == 1) {
				this.mild = 1;
			}
			return this.mild;
		}

		public int countSevere() {
			if (this.severity == 0) {
				this.severe = 0;
			}
			if (this.severity == 2) {
				this.severe = 1;
			}
			return this.severe;
		}

		public int countCritical() {
			if (this.severity == 0) {
				this.critical = 0;
			}
			if (this.severity == 3) {
				this.critical = 1;
			}
			return this.critical;
		}

		public int getHospitalised() {
			return this.hospitalised;
		}
		
		public int getTimesHospitalised() {
			return this.timesHospitalised;
		}

		public int totalHospitalised() {
			return this.beenHospitalised;
		}

		public double getLengthOfStay() {
			return this.lengthOfStay;
		}

		public int getICU() {
			return this.icu;
		}
		
		public int getTimesICU() {
			return this.timesICU;
		}

		public double getLengthOfStayICU() {
			return this.lengthOfStayICU;
		}

		public double getHospitalCost() {
			return this.myHospitalCost;	
		}
		public double getTotalHospitalCost() {
			return this.totalHospitalCost;	
		}

		public double getICUCost() {
			return this.myICUCost;	
		}
		
		public double getTotalICUCost() {
			return this.totalICUCost;	
		}

		public double getDeathProb() {
			return this.dailyDeathProb;
		}

		public double getVaccineCost() {
			return this.myVaccineCost;
		}
		public double getTotalVaccineCost() {
			return this.totalVaccineCost;
		}

		public double getlockdownCost() {
			return this.myLockdownCost;
		}
		
		public double getTotallockdownCost() {
			return this.totalLockdownCost;
		}

		public double getTotalInfected() {
			return this.beenInfected;
		}

		public double getAge() {
			return this.age;
		}

		public int getVaccinated() {
			return this.vaccinated;
		}

		public double getvaccineRiskAdjust() {
		return this.vaccineRiskAdjust;
		}

		public int getLongTermEffects() {
		return this.longTermEffects;
	    }

		public double getLongCovidCost() {
		return this.myLongCovidCost;
		}

		public int getTimeToLastInfection() {
		return this.timeSinceLastInfection;
		}

		public int getInfectionPeriod() {
		return this.infectiousPeriod;
		}
		
		public int getDiedOfCovid() {
			if (this.causeOfDeath == "Death from Covid") { 
				diedOfCovid = 1; }
			else { diedOfCovid = 0; }
			return this.diedOfCovid;
			}
		
		public int countDead() {
			if (this.isDead == true) {
				this.countDead = 1;
			}
			else {this.countDead = 0;}
			return this.countDead;
		}
		
		public double gethosLOSMean() {
		return this.hosLoSmean;
		}	
		
		public int getTimesInfected() {
			return this.timesInfected;
		}
		public int getTimesLongCovid() {
			return this.timesLongCovid;
		}
		public double getTotalLongCovidCost() {
			return this.totalLongCovidCost;
		}
		public int getTimesVaccinated() {
			return this.timesVaccinated;
		}
		public int getHypertension() {
			if (this.sBloodPressure >= 140 || this.dBloodPressure >= 90 ) 
				{this.hypertension = 1;}
			else {this.hypertension = 0;}
			return this.hypertension;
		}
		public int getTicks() {
			return this.ticks;
		}
		public double getNContacts() {
			return this.myContacts;
		}
		public int getnContactsTickCounter() {
			return this.nContactsTickCounter;
		}
		public double getExposurePeriod() {
			return this.exposurePeriod;
		}
		public double getTotalHealthCostCovid() {
			return this.totalHealthCostCovid;
		}
		public double getTotalInterventionCostCovid() {
			return this.totalInterventionCostCovid;
		}
		public double getTotalCovidCost() {
			return this.totalCovidCost;
		}
		public int getCovidDeathDiabetes() {
			return this.covidDeathDiabetes;
		}
		public int getCovidDeathCVD() {
			return this.covidDeathCVD;
		}
		public int getCovidDeathHypertension() {
			return this.covidDeathHypertension;
		}
		public int getCovidDeathMale() {
			return this.covidDeathMale;
		}
		public int getCovidDeathObese() {
			return this.covidDeathObese;
		}
		public int getICUMorbidlyObese() {
			return this.icuMorbidlyObese;
		}
		public int getICUObese() {
			return this.icuObese;
		}
		public int getICUOverweight() {
			return this.icuOverweight;
		}
		public int getICUNormalWeight() {
			return this.icuNormalWeight;
		}
}

