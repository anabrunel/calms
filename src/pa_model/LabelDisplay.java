package pa_model;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.Border;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunState;
import repast.simphony.render.RenderListener;
import repast.simphony.render.RendererListenerSupport;
import repast.simphony.visualization.DisplayEditorLifecycle;
import repast.simphony.visualization.DisplayListener;
import repast.simphony.visualization.IDisplay;
import repast.simphony.visualization.Layout;
import repast.simphony.visualization.ProbeListener;


public class LabelDisplay implements IDisplay {
	
		private JPanel panel = new JPanel();
		private JLabel labelCost = new JLabel();
		private JLabel labelPAStatus = new JLabel();
		private RendererListenerSupport support = new RendererListenerSupport();
		private double count, c1;
		private double infectionCount, i1;
		
	  public LabelDisplay() {
		  this.count = 0;
		  this.infectionCount = 0;
		 
		  }
	  
	  /**
	   * Gets a panel that contains the actual gui for visualization.
	   *
	   * @return a panel that contains the actual gui for visualization.
	   */
	  
	  public JPanel getPanel() {
		  /*
		  
		  Border pad = BorderFactory.createEmptyBorder(3, 3, 3, 3);
		  Border line = BorderFactory.createLineBorder(Color.black, 3);
		  Border b = BorderFactory.createCompoundBorder(line, pad);
		  labelCost.setBorder(b);
		
		  
	    labelCost.setForeground(Color.RED);
		labelCost.setFont(labelCost.getFont().deriveFont(18f));
	   */
		  
		  labelCost.setLocation(0, 0);
		  
		  panel.add(getMyLabel(labelCost));
	   	panel.add(getMyLabel(labelPAStatus));
		  
		
		//panel.add(labelCost);
		
	    return panel;
	  }
	  
	  public JLabel getMyLabel(JLabel j){

		  Border pad = BorderFactory.createEmptyBorder(10, 10, 10, 10);
		  Border line = BorderFactory.createLineBorder(Color.DARK_GRAY, 3);
		  Border b = BorderFactory.createCompoundBorder(line, pad);
		  
		  j.setOpaque(true);
		  
		  j.setBorder(b);
		  
		  
		  
	    j.setForeground(Color.RED);
		j.setFont(j.getFont().deriveFont(18f));
		j.setBackground(Color.WHITE);  
		
		
		  return j;
	  }
	  
	  
	  /**
	   * Sets the Layout for the display to use.
	   *
	   * @param layout the layout to use
	   */
	  public void setLayout(Layout layout) {
	  }
	  
	  /**
	   * Sets the frequency of the layout.
	   *
	   * @param frequency the frequency of the layout
	   * @param interval  the interval if the frequency is AT_INTERVAL. The interval is in terms
	   *                  of number of calls to update()
	   */
	  public void setLayoutFrequency(LayoutFrequency frequency, int interval) {
	  }

	@Override
	public void render() {
		
		// TODO Auto-generated method stub
	/*	labelCost.setText("Total Cost: " + count);
		labelPAStatus.setText("PA Status: " + Util.formatNumber(paCount)); */
		
		labelCost.setText("Total Cost: " + count);
		labelPAStatus.setText("Total Infections: " + Util.formatNumber(infectionCount));
	    support.fireRenderFinished(this);
	}

	
	 public void addRenderListener(RenderListener listener) {
		    support.addListener(listener);
		  }
	 
	@Override
	public void setPause(boolean pause) {
		// TODO Auto-generated method stub
		
	}

	 /**
	   * Updates the state of the display to reflect whatever it is that it is displaying.
	   */
	
	@Override
	public void update() {
		// TODO Auto-generated method stub
		Context context = RunState.getInstance().getMasterContext();
		c1=0;
		i1=0;
		for (Object obj : context) {
			if(obj instanceof Agent){
			//CALMS
				c1 += ((Agent) obj).getTotalCovidCost();
				i1 += ((Agent) obj).getTimesInfected();
				
		/*	c1 += ((Agent) obj).getTotalCost();
			p1 += ((Agent) obj).getPAStatus(); */
			}
		}
		count = c1;///PAContext.cohortSize;
		
		//count = PAContext.psaStatsList.get(13).getAverage();
		
		infectionCount = i1; ///PAContext.cohortSize;
		
		
		PAContext.psaStatsList.get(13);
		
		//count++;
		//paCount+=0.01;
	}

	 /**
	   * Initializes the display. Called once before the display is made visible.
	   */
	
	@Override
	public void init() {
		update();
		
	}



	@Override
	public void addDisplayListener(DisplayListener listener) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void registerToolBar(JToolBar bar) {
		// TODO Auto-generated method stub
		
	}

	 /**
	   * Destroys the display, allowing it to free any resources it may have acquired.
	   */
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	/**
	   * Notifies this IDisplay that its associated gui widget has been iconified.
	   */
	
	@Override
	public void iconified() {
		// TODO Auto-generated method stub
		
	}

	/**
	   * Notifies this IDisplay that its associated gui widget has been deIconified.
	   */
	
	@Override
	public void deIconified() {
		// TODO Auto-generated method stub
		
	}

	 /**
	   * Notifies this IDisplay that its associated gui widget has been closed.
	   */
	
	@Override
	public void closed() {
		// TODO Auto-generated method stub
		
	}

	  /**
	   * Adds a probe listener to listen for probe events produced by this IDisplay.
	   *
	   * @param listener the listener to add
	   */
	
	@Override
	public void addProbeListener(ProbeListener listener) {
		// TODO Auto-generated method stub
		
	}

	  /**
	   * Gets the layout the display uses
	   *
	   * @return a layout object
	   */
	
	@Override
	public Layout getLayout() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DisplayEditorLifecycle createEditor(JPanel panel) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resetHomeView() {
		// TODO Auto-generated method stub
		
	}

}
