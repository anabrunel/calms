package pa_model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ReplicationData {

	private Map<String, Map<String, ArrayList<String>>> replication; 
		
	
	public ReplicationData(Map replication){
	
		//this.replication = new HashMap();
		
		if(replication == null){
			this.replication = new HashMap();
			} else {
				this.replication = new HashMap(replication);
			}
		
		
	}
	
	public Set getAllKeys() {
		
		return this.replication.keySet();
	}
	
	public ArrayList getReplicationsArray(String type, String arrayName){
		
		return (ArrayList) getReplicationsArrayMap(type).get(arrayName);		
			
	}
	
	public int getReplicationsArraySize(String type, String arrayName){
		int ret = 0;
		ArrayList a = (ArrayList) getReplicationsArrayMap(type).get(arrayName);		
		ret = a.size();	
		return ret;
	}
	
	
	public Map getReplicationsArrayMap(String type){
		
		return replication.get(type);
			
	}
	
	
	public Map getReplicationsMap(){
			return replication;
	}

}
