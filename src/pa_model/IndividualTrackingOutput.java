package pa_model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IndividualTrackingOutput {
	
	private String s1, c1, d1, m1 = null; 
	private int s2, c2, d2, m2 = -1;
    private double s3, s4, s5, s6, s7, c3, c4, c5, c6, c7, d3, d4, d5, d6, d7, m3, m4, m5, m6, m7 = -0.1;
	
	public IndividualTrackingOutput(Agent a){
		  if(!a.getStrokeEventsList().isEmpty()){
			  s1 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventName();
			  s2 = a.getStrokeEventsList().size();
			  s3 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getStartTime();
			  s4 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getAgeATStart();
			  s5 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getPAATStart();
			  s6 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventAcuteDuration();
			  s7 = a.getStrokeEventsList().get(a.getStrokeEventsList().size()-1).getEventAcutePA();
		  }
		  if(!a.getCHDEventsList().isEmpty()){
			  c1= a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventName();
			  c2 = a.getCHDEventsList().size();
			  c3 = a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getStartTime();
			  c4 = a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getAgeATStart();
			  c5 = a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getPAATStart();
			  c6 = a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcuteDuration();
			  c7 = a.getCHDEventsList().get(a.getCHDEventsList().size()-1).getEventAcutePA();
		  }
		  if(!a.getDepressionEventsList().isEmpty()){
			  d1 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventName();
			  d2 = a.getDepressionEventsList().size();
			  d3 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getStartTime();
			  d4 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getAgeATStart();
			  d5 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getPAATStart();
			  d6 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventAcuteDuration();
			  d7 = a.getDepressionEventsList().get(a.getDepressionEventsList().size()-1).getEventAcutePA();
		  }
		  if(!a.getMSIEventsList().isEmpty()){
			  m1 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventName();
			  m2 = a.getMSIEventsList().size();
			  m3 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getStartTime();
			  m4 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getAgeATStart();
			  m5 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getPAATStart();
			  m6 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventAcuteDuration();
			  m7 = a.getMSIEventsList().get(a.getMSIEventsList().size()-1).getEventAcutePA();
		  }
		
	}
	
	public void writeOutIndividualTrack(Agent a){
		
		
		try{
		
			String outfile = Util.currentOutDirectory.getPath() + OutputFiles.INDIVIDUAL.toString().trim()+"TRACK.csv";
			//String outfile = OutputFiles.INDIVIDUAL.toString().trim();
			FileWriter fw = new FileWriter(outfile, true);
			  BufferedWriter out = new BufferedWriter(fw);
			  out.write(Util.getCurrentTime()+","+a.getInModelID()+","+a.getID()+","
					  +Util.formatNumber(a.getAge())+","
					  +Util.formatNumber(a.getPAStatus())+","  
					  +Util.formatNumber(a.getPAPreviousPeriod())+","
					  +a.getInAcutePeriod()+","
				      +s1+","+s2+","+s3+","+s4+","+s5+","+s6+","+s7+","
					  +c1+","+c2+","+c3+","+c4+","+c5+","+c6+","+c7+","
					  +d1+","+d2+","+d3+","+d4+","+d5+","+d6+","+d7+","
			          +m1+","+m2+","+m3+","+m4+","+m5+","+m6+","+m7+","
			  
				       +a.getSusceptibleAgents()+","
					   +a.getExposedAgents()+","
		               +a.getinfectedAgents()+","
				       +a.getRecoveredAgents()+","
				       +a.getInfectionProb()+","
			           +a.getTimesInfected()+","
					   +a.getSeverity()+","	
					   +a.getHospitalised()+","
					   +a.getICU()+","
					   +Util.formatNumber(a.getHospitalCost())+","
					   +Util.formatNumber(a.getICUCost())+","
					   +Util.formatNumber(a.getRisk())+","
					   +Util.formatNumber(a.getLengthOfStay())+","
			           +Util.formatNumber(a.getLengthOfStayICU())+"\r\n");
						
			  //Close the output stream
			  out.close();
			
		  }catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			  }
	}
	
	
	
}
