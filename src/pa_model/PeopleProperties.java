package pa_model;

public enum PeopleProperties {
	
	ID,	AGE, GENDER, PA_STATUS, HAD_CVD, HAD_T2D, BMI,	HDL_RATIO, S_BLOOD_PRESSURE, D_BLOOD_PRESSURE, SMOKING, ETHNICITY, FAMILY_CVD_HISTORY, DEPRIVATION_TOWN, BP_TREATMENT, 
	RHEUMATOID_ARTHRITIS, CHRONIC_KIDNEY_DISEASE, ATRIAL_FIBRILLATION, HAD_T1D, CORT, FAMILY_T2D_HISTORY, EDUCATION, SAMPLE_WEIGHT, UTILITY_IMPEQ5D, DEPRIVATION_IMD, HAD_DEPRESSION, HAD_MSI, SEC, LLI, COMORBIDITY;

		
	public String toString(){
		switch(this){
		case ID: return "Id";
		case AGE: return "Age";
		case GENDER: return "Gender";
		case PA_STATUS: return "PA_status";
		case HAD_CVD: return "Had_Stroke";
		case HAD_T2D: return "Had_T2D";
		case BMI: return "BMI";
		case HDL_RATIO: return "HDL_Ratio";
		case S_BLOOD_PRESSURE: return "sbp";
		case D_BLOOD_PRESSURE: return "dbp";
		case SMOKING: return "Smoking";
		case ETHNICITY: return "Ethnicity";
		case FAMILY_CVD_HISTORY: return "Family_CVD_history";
		case DEPRIVATION_TOWN: return "Deprivation_Town";
		case BP_TREATMENT: return "BP_treatment";
		case RHEUMATOID_ARTHRITIS: return "Rheumatoid_Arthritis";
		case CHRONIC_KIDNEY_DISEASE: return "Chronic_kidney_disease";
		case ATRIAL_FIBRILLATION: return "Atrial_Fibrillation";
		case HAD_T1D: return "Had_T1D";
		case CORT: return "Cort";
		case FAMILY_T2D_HISTORY: return "Family_T2D_history";
		case EDUCATION: return "Education";
		case SAMPLE_WEIGHT: return "Sample_Weight";
		case UTILITY_IMPEQ5D: return "Utility_impeq5D";
		case DEPRIVATION_IMD: return "Deprivation_imd";
		case HAD_DEPRESSION: return "Had_Depression";
		case HAD_MSI: return "Had_MSI";
		case SEC: return "SEC";
		case LLI: return "LLI";
		case COMORBIDITY: return "Comorbidity";
		default: return " ";
		}
	}


	/*
	 * SEC: SocioEconomic Class 
	 * 
	 * 0 Not classified
	 * 1 I-professional
	 * 2 II-Managerial technical
	 * 3.1
	 * 3.2
	 * 4 IV-Semi-skilled manual
	 * 5 V-Unskilled manual
	 * 
	 * LLI: Long Lasting Illness
	 *   
	 * 1 Limiting long lasting illness
	 * 2 Non-limiting long lasting illness
	 * 3 No long lasting illness
	 *   
	 * Comorbidity
	 * 
	 *   0 if diabetes==0&depression==0&cvd==0&musco==0
	 *   1 if diabetes==1&depression==1&cvd==1&musco==1
	 *   2 if diabetes==0&depression==1&cvd==1&musco==1
	 *   3 if diabetes==1&depression==0&cvd==1&musco==1
	 *   4 if diabetes==1&depression==1&cvd==0&musco==1
	 *   5 if diabetes==1&depression==1&cvd==1&musco==0
	 *   6 if diabetes==0&depression==0&cvd==1&musco==1
	 *   7 if diabetes==1&depression==0&cvd==0&musco==1
	 *   8 if diabetes==0&depression==1&cvd==1&musco==0
	 *   9 if diabetes==1&depression==1&cvd==0&musco==0
	 *   10 if diabetes==1&depression==0&cvd==1&musco==0
	 *   11 if diabetes==0&depression==1&cvd==0&musco==1
	 *   12 if diabetes==1&depression==0&cvd==0&musco==0
	 *   13 if diabetes==0&depression==1&cvd==0&musco==0
	 *   14 if diabetes==0&depression==0&cvd==1&musco==0
	 *   15 if diabetes==0&depression==0&cvd==0&musco==1
	 *   
	 * */
	
	
}
