package pa_model;

public class Record {
	
	private int inModelID;
	private RecordProperties prop;
	
	private String type;
	private String name;
	private ReplicationData repl;
	
	public Record(int inModelID, String type, RecordProperties prop){
		this.inModelID = inModelID;
		this.prop = prop;
		
	}
		
	public Record(int inModelID, String type, String name, ReplicationData repl){
		this.inModelID = inModelID;
		this.type = type;
		this.name = name;
		this.repl = repl;
		
	}
	
	public int getInModelID(){
		return this.inModelID;
	}
	
	public String getType (){
		return this.type;
	}
	
	public String getName (){
		return this.name;
	}
	
	public RecordProperties getProp(){
		return this.prop;
	} 

	public ReplicationData getReplicationData(){
		return this.repl;
	}
	
	//////Methods to return the values from the array in record in the correct type
	
	////Events
	public String getEventName(int repReadData){
		String s = this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.NAME.toString()).get(repReadData).toString();
		return s;
	}

	public int getEventAcuteDurationInMonths(int repReadData){
		int i = Integer.parseInt(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.ACUTE_DURATION_MONTHS.toString()).get(repReadData).toString());
		return i;
	}
	
	public double getEventAcuteCostPerMonth(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.ACUTE_COST_MONTH.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventAcuteUtilityMultiplier(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.ACUTE_UTILITY_MULTIPLIER.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventAcutePA(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.ACUTE_PA.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcuteCostPerMonth(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_COST_MONTH.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcuteUtilityMultiplier(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_UTILITY_MULTIPLIER.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAInactiveMmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_INACTIVE_M_MEAN.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAInactiveMsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_INACTIVE_M_SD.toString()).get(repReadData).toString());
		return d;
	}
		
	public double getEventPostAcutePAModeratelyActiveMmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_M_MEAN.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAModeratelyActiveMsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_M_SD.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAVeryActiveMmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_M_MEAN.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAVeryActiveMsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_M_SD.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAInactiveFmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_INACTIVE_F_MEAN.toString()).get(repReadData).toString());
		return d;
	}	
	
	public double getEventPostAcutePAInactiveFsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_INACTIVE_F_SD.toString()).get(repReadData).toString());
		return d;
	}
		
	public double getEventPostAcutePAModeratelyActiveFmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_F_MEAN.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAModeratelyActiveFsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_MODERATELY_ACTIVE_F_SD.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAVeryActiveFmean(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_F_MEAN.toString()).get(repReadData).toString());
		return d;
	}
	
	public double getEventPostAcutePAVeryActiveFsd(int repReadData){
		double d = Double.parseDouble(this.getReplicationData().getReplicationsArray(DataType.EVENT.toString(), EventsProperties.POST_ACUTE_PA_VERY_ACTIVE_F_SD.toString()).get(repReadData).toString());
		return d;
	}
		
	
}
