package pa_model;

public class AnnualPSAOutput {
	
	private double year;
	private int psa, cohortSize;
	private Stats lifeYears, paStatus, bmi, hdl, sbp, events, cvdEvents, msiEvents, depressionEpisodes, strokeEvents, chdEvents, t2dEvents,
		    yearsActive, eventCost, 
            // covid parameters
			dieOfCovid, timesLongCovid, timesInfected, timesHospitalised, timesICU, totalHospitalCost, totalICUCost, totalLongCovidCost, 
			totalLockdownCost, totalVaccinationCost, timesVaccinated, totalHealthcareCostCovid, totalInterventionCostCovid, totalCovidCost;
	
	public AnnualPSAOutput(){
		//this.year = Util.getCurrentTime()*Util.CONVERT_YEARS;
		this.year = PAContext.clock.getClockYears();
		this.psa = PAContext.repCounter;
		this.cohortSize = PAContext.cohortSize;
		this.lifeYears = new Stats();
		this.paStatus = new Stats();
		this.bmi = new Stats();
		this.hdl = new Stats();
		this.sbp = new Stats();
		this.events = new Stats();
		this.cvdEvents = new Stats();
		this.msiEvents = new Stats();
		this.depressionEpisodes = new Stats();
		this.strokeEvents = new Stats();
		this.chdEvents = new Stats();
		this.t2dEvents = new Stats();
		this.yearsActive = new Stats();
		this.eventCost = new Stats();

		this.timesInfected = new Stats();
		this.timesHospitalised = new Stats();
		this.timesICU = new Stats();
		this.dieOfCovid = new Stats();
		this.timesLongCovid = new Stats();
		this.totalHospitalCost = new Stats();
		this.totalICUCost = new Stats();
		this.totalLongCovidCost = new Stats();
		this.totalLockdownCost = new Stats();
		this.totalVaccinationCost = new Stats();
		this.timesVaccinated = new Stats();
		this.totalHealthcareCostCovid = new Stats();
		this.totalInterventionCostCovid = new Stats();
		this.totalCovidCost = new Stats();
	}
	
	
	
	public void initialiseAnnualPSAStats(){
		this.lifeYears.initialise();
		this.paStatus.initialise();
		this.bmi.initialise();
		this.hdl.initialise();
		this.sbp.initialise();
		this.events.initialise();
		this.cvdEvents.initialise();
		this.msiEvents.initialise();
		this.depressionEpisodes.initialise();
		this.strokeEvents.initialise();
		this.chdEvents.initialise();
		this.t2dEvents.initialise();
		this.yearsActive.initialise();
		this.eventCost.initialise();
		
		this.timesInfected.initialise();
		this.timesHospitalised.initialise();
		this.timesICU.initialise();
		this.dieOfCovid.initialise();
		this.timesLongCovid.initialise();
		this.totalHospitalCost.initialise();
		this.totalICUCost.initialise();
		this.totalLongCovidCost.initialise();
		this.totalLockdownCost.initialise();
		this.totalVaccinationCost.initialise();
		this.timesVaccinated.initialise();
		this.totalHealthcareCostCovid.initialise();
		this.totalInterventionCostCovid.initialise();
		this.totalCovidCost.initialise();
	}

		public void setYear(double year){
			this.year = year;
		} 
		public double getYear(){
			return this.year;
		}
		public void getPSA(int psa){
			this.psa = psa;
		}
		public int getPSA(){
			return this.psa;
		}
		public void setCohortSize(int cohortSize){
			this.cohortSize = cohortSize;
		}
		public int getCohortSize(){
			return this.cohortSize;
		}
		public Stats getLifeYears(){
			return this.lifeYears;
		}
		public Stats getPAStatus(){
			return this.paStatus;
		}
		
		public Stats getBMI(){
			return this.bmi;
		}
		public Stats getHDL(){
			return this.hdl;
		}
		public Stats getSBP(){
			return this.sbp;
		}
		
		public Stats getEvents(){
			return this.events;
		}
		public Stats getCVDEvents(){
			return this.cvdEvents;
		}
		public Stats getMSIEvents(){
			return this.msiEvents;
		}
		public Stats getDepressionEpisodes(){
			return this.depressionEpisodes;
		}
		public Stats getStrokeEvents(){
			return this.strokeEvents;
		}
		public Stats getCHDEvents(){
			return this.chdEvents;
		}
		public Stats getT2DEvents(){
			return this.t2dEvents;
		}
		public Stats getYearsActive(){
			return this.yearsActive;
		}
		public Stats getEventCost(){
			return this.eventCost;
		}
		
		public Stats getTimesInfected(){
			return this.timesInfected;
		}
		public Stats getTimesHospitalised(){
			return this.timesHospitalised;
		}
		public Stats getTimesICU(){
			return this.timesICU;
		}
		public Stats getDiedOfCovid(){
			return this.dieOfCovid;
		}
		public Stats getTimesLongCovid(){
			return this.timesLongCovid;
		}
		public Stats getTotalHospitalCost(){
			return this.totalHospitalCost;
		}
		public Stats getTotalICUCost(){
			return this.totalICUCost;
		}
		public Stats getTotalLongCovidCost(){
			return this.totalLongCovidCost;
		}
		public Stats getTotalLockdownCost(){
			return this.totalLockdownCost;
		}
		public Stats getTotalVaccinationCost(){
			return this.totalVaccinationCost;
		}
		public Stats getTimesVaccinated(){
			return this.timesVaccinated;
		}
		public Stats getTotalHealthcareCostCovid(){
			return this.totalHealthcareCostCovid;
		}
		public Stats getTotalInterventionCostCovid(){
			return this.totalInterventionCostCovid;
		}
		public Stats getTotalCovidCost(){
			return this.totalCovidCost;
		}		
}
