package pa_model;

public enum EventsProperties {
	
	NAME, ACUTE_DURATION_MONTHS, ACUTE_COST_MONTH, ACUTE_UTILITY_MULTIPLIER, ACUTE_PA, POST_ACUTE_COST_MONTH, POST_ACUTE_UTILITY_MULTIPLIER, 
	POST_ACUTE_PA_INACTIVE_M_MEAN, POST_ACUTE_PA_INACTIVE_M_SD, POST_ACUTE_PA_MODERATELY_ACTIVE_M_MEAN, POST_ACUTE_PA_MODERATELY_ACTIVE_M_SD, POST_ACUTE_PA_VERY_ACTIVE_M_MEAN, POST_ACUTE_PA_VERY_ACTIVE_M_SD,
	POST_ACUTE_PA_INACTIVE_F_MEAN, POST_ACUTE_PA_INACTIVE_F_SD, POST_ACUTE_PA_MODERATELY_ACTIVE_F_MEAN, POST_ACUTE_PA_MODERATELY_ACTIVE_F_SD, POST_ACUTE_PA_VERY_ACTIVE_F_MEAN, POST_ACUTE_PA_VERY_ACTIVE_F_SD;
	
		public String toString(){
		switch(this){
		case NAME: return "Event_Name";
		case ACUTE_DURATION_MONTHS: return "Acute_Duration_Months";
		case ACUTE_COST_MONTH: return "Acute_Cost_month";
		case ACUTE_UTILITY_MULTIPLIER: return "Acute_Utility_Multiplier";
		case ACUTE_PA: return "Acute_PA";
		case POST_ACUTE_COST_MONTH: return "Post_Acute_Cost_month";
		case POST_ACUTE_UTILITY_MULTIPLIER: return "Post_Acute_Utiilty_Multiplier";
		case POST_ACUTE_PA_INACTIVE_M_MEAN: return "Post_Acute_PA_Inactive_Mmean";
		case POST_ACUTE_PA_INACTIVE_M_SD: return "Post_Acute_PA_Inactive_Msd";
		case POST_ACUTE_PA_MODERATELY_ACTIVE_M_MEAN: return "Post_Acute_PA_Moderately_Active_Mmean";
		case POST_ACUTE_PA_MODERATELY_ACTIVE_M_SD: return "Post_Acute_PA_Moderately_Active_Msd";
		case POST_ACUTE_PA_VERY_ACTIVE_M_MEAN: return "Post_Acute_PA_Very_Active_Mmean";
		case POST_ACUTE_PA_VERY_ACTIVE_M_SD: return "Post_Acute_PA_Very_Active_Msd";
		case POST_ACUTE_PA_INACTIVE_F_MEAN: return "Post_Acute_PA_Inactive_Fmean";
		case POST_ACUTE_PA_INACTIVE_F_SD: return "Post_Acute_PA_Inactive_Fsd";
		case POST_ACUTE_PA_MODERATELY_ACTIVE_F_MEAN: return "Post_Acute_PA_Moderately_Active_Fmean";
		case POST_ACUTE_PA_MODERATELY_ACTIVE_F_SD: return "Post_Acute_PA_Moderately_Active_Fsd";
		case POST_ACUTE_PA_VERY_ACTIVE_F_MEAN: return "Post_Acute_PA_Very_Active_Fmean";
		case POST_ACUTE_PA_VERY_ACTIVE_F_SD: return "Post_Acute_PA_Very_Active_Fsd";
		
				
		default: return " ";
		}
	}
}
