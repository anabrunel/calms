# CoronAvirus Lifelong Modelling and Simulation (CALMS)

CALMS is an agent-based simulation that considers individual characteristics as well as comorbidities in calculating the risk of infection and severe disease. It predicts the impact of Covid-19 on individual health-related quality of life and its economic impact on the healthcare system. 

CALMS is built in JAVA using [Repast Simphony](https://repast.github.io/).

## CALMS: How to run

To run CALMS:

1. Install [Repast Simphony](https://repast.github.io/download.html).
 
2. Create a Repast Simphony Project.
   
3. Import the model code into the project.

4. Run it as any Repast Project.

For full Repast documentation go [here](https://repast.github.io/docs.html).
