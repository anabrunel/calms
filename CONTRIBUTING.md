# Contributors
The development of CALMS was:
- led by Dr Anastasia Anagnostou
- carried out by Dr Katie Mintram 

#### Supporting team

The following team contributed to model conceptualisation:
- Dr Nana Anokye
- Professor Simon J.E. Taylor

# Acknowledgements

***This work is partially supported by the H2020 [STAMINA Project](https://cordis.europa.eu/project/id/883441)***




